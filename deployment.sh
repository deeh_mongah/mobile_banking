#!/bin/bash

#Custom Java Service Wrapper - not like a real daemon

#Declare re-usable constants
[[ -n "$DEBUG" ]] && set -x

#Turn on color prompt: for ubuntu servers
force_color_prompt=yes

# Initialize stop wait time if not provided by the config file
#[[ -z "$STOP_WAIT_TIME" ]] && STOP_WAIT_TIME="{{stopWaitTime:60}}"
STOP_WAIT_TIME=60

JAR_FILE="kluster-sacco-middleware-0.0.1-SNAPSHOT.jar"
GIT_URL="https://mwawughanga@bitbucket.org/binaryltdteam/kluster-middleware.git"

# ANSI Colors
echoRed() { echo $'\e[091m'"$1"$'\e[0m'; }
echoGreen() { echo $'\e[092m'"$1"$'\e[0m'; }
echoYellow() { echo $'\e[093m'"$1"$'\e[0m'; }

#Version control
update(){
    echo -e ''
    echoGreen '[INFO]----------------------------------------------------------------------------'
    echoGreen '[INFO] Updating repository content'
    echoGreen '[INFO]----------------------------------------------------------------------------'
    rm -rf application
    mkdir application
    git clone ${GIT_URL} application/

    # Package jar file and restart application
    build && restart

    return 0;
}

#Compile a new jar file for deployment
build(){
    echo -e ''
    echoGreen '[INFO]----------------------------------------------------------------------------'
    echoGreen '[INFO] Clean and package the jar'
    echoGreen '[INFO]----------------------------------------------------------------------------'
    cd application
    # QA Server
    mv src/main/resources/application-qa.yml src/main/resources/application.yml
    # Live Server
    # mv src/main/resources/application-live.yml src/main/resources/application.yml
    mvn clean package

    echoGreen '[INFO]----------------------------------------------------------------------------'
    echoGreen '[INFO] Copy file to working directory'
    echoGreen '[INFO]----------------------------------------------------------------------------'
    cp target/${JAR_FILE} ../

    echoGreen '[INFO]----------------------------------------------------------------------------'
    echoGreen '[INFO] Clean up the target'
    echoGreen '[INFO]----------------------------------------------------------------------------'
    rm -rf application/target/
    return 0;
}

#Check the application status
check_status(){
    systemctl status kluster-gateway
    # In any other case, return 0
    return 0
}

restart(){
  echoYellow '[WARN]----------------------------------------------------------------------------'
  systemctl restart kluster-gateway
  echoYellow '[WARN]----------------------------------------------------------------------------'
  return 0
}

start(){
  echoYellow '[WARN]----------------------------------------------------------------------------'
  systemctl start kluster-gateway
  echoYellow '[WARN]----------------------------------------------------------------------------'
  return 0
}

stop() {
   systemctl stop kluster-gateway
   return 0
}

#Expose commands to handle the app
case "$1" in
status)
    check_status
    ;;
start)
    start; exit $?;;
stop)
    stop
    ;;

update)
    update
    ;;

restart|reload)
    stop && start
    ;;
*)
echo "Usage: $0 {status|start|stop|restart}"
    exit 1
esac

exit 0