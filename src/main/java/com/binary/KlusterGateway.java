package com.binary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class KlusterGateway {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(KlusterGateway.class);
        app.addListeners(new ApplicationPidFileWriter());
        app.run( args );
    }

}
