package com.binary.core.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@Component
public class HttpService {

    final private Logger logger = LoggerFactory.getLogger(HttpService.class);
    @Autowired private Environment env;

    /**
     * @param baseURL
     * @param uri
     * @param request
     * @return
     * @throws Exception
     */
    public ResponseEntity<String> postRequest(String baseURL, String uri, Object request) throws Exception {
        //Fetch Host
        /*Pack the URL for this request*/
        UriComponents uriComponents =
                fromHttpUrl(baseURL)
                        .path(uri)
                        .build()
                        .encode();

        ObjectMapper mapper = new ObjectMapper();
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<?> httpEntity = new HttpEntity<>(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request), httpHeaders);

        System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request));
        logger.info("SERVICE REQUEST : {} {} {}", uriComponents.toString(), httpEntity.getHeaders(), httpEntity.getBody());
        ResponseEntity<String> responseEntity = restTemplate.exchange(uriComponents.toString(), HttpMethod.POST, httpEntity, String.class);
        logger.info("SERVICE RESPONSE : {} {}", responseEntity.getHeaders(), responseEntity.getBody());
        return responseEntity;
    }
}
