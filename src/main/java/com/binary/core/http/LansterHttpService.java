package com.binary.core.http;

import com.binary.web.apiauthentication.services.AuthorizationService;
import com.binary.web.apiauthentication.vm.Authorization;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;

import java.util.Map;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@Component
public class LansterHttpService {

    @Autowired private RestTemplate restTemplate;
    @Autowired private ObjectMapper objectMapper;
    @Autowired private AuthorizationService authorizationService;

    @Value("${lanster.api.base-url}")
    private String baseUrl;

    public ResponseEntity<String> postRequest( String uri, JsonNode request) throws Exception{
        // Fetch Auth Object: Pass an instance of this class to avoid cyclic dependency issues
        Map<String, Object> userAuthorization = authorizationService.findUserAuthorization( this );
        if (!userAuthorization.get("status").equals("00")) {

            JsonNode node = objectMapper.createObjectNode();
            ((ObjectNode) node)
                .put("message", String.valueOf(userAuthorization.get("message")))
                .put("status", Integer.valueOf(String.valueOf(userAuthorization.get("status"))));

            return ResponseEntity.unprocessableEntity().body( node.toString() );
        }
        Authorization authorization = new Authorization()
                .setSessionKey(String.valueOf(userAuthorization.get("sessionKey")))
                .setAccountID(String.valueOf(userAuthorization.get("accountID")))
                .setAccountType(Integer.valueOf(String.valueOf(userAuthorization.get("accountType"))))
                .setPass(String.valueOf(userAuthorization.get("pass")))
                .setUser(String.valueOf(userAuthorization.get("user")));

        JsonNode authNode = objectMapper.convertValue( authorization, JsonNode.class);
        ((ObjectNode) request)
                .put("auth", authNode );

        UriComponents uriComponents =
                fromHttpUrl( baseUrl )
                        .path(uri)
                        .build()
                        .encode();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<?> httpEntity = new HttpEntity<>(request.toString(), httpHeaders);

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                uriComponents.toString(),
                HttpMethod.POST,
                httpEntity,
                String.class
        );

        return responseEntity;
    }

    /**
     * @param uri
     * @param request
     * @return
     * @throws Exception
     */
    public ResponseEntity<String> postWithoutAuth(String uri, Object request) throws Exception {
        //Fetch Host
        /*Pack the URL for this request*/
        UriComponents uriComponents =
                fromHttpUrl( baseUrl )
                        .path( uri )
                        .build()
                        .encode();

        String jsonPayload = objectMapper.writeValueAsString( request );
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<?> httpEntity = new HttpEntity<>(jsonPayload, httpHeaders);
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                uriComponents.toString(),
                HttpMethod.POST,
                httpEntity,
                String.class
        );

        return responseEntity;
    }
}
