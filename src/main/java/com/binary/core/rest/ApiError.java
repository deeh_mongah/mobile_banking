package com.binary.core.rest;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class ApiError {

    private String status;

    //Holds the operation call httpStatus
    private HttpStatus httpStatus;

    //The date-time instance of when the error happened.
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp = LocalDateTime.now();

    //User-friendly message about the error.
    private String message;

    //System message describing the error in more detail.
    private String debugMessage;

    //Holds an array of sub-errors that happened
//    private List<ApiSubError> subErrors;

    public ApiError(HttpStatus httpStatus) {
        this.setHttpStatus( httpStatus );
    }

    public ApiError(HttpStatus httpStatus, Throwable ex) {
        this.message = "Unexpected error";
        this.debugMessage = ex.getLocalizedMessage();
        this.setHttpStatus( httpStatus );
    }

    public ApiError(HttpStatus httpStatus, String message, Throwable ex) {
        this.message = message;
        this.debugMessage = ex.getLocalizedMessage();
        this.setHttpStatus( httpStatus );
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
        this.status = String.valueOf( httpStatus.value() );
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDebugMessage() {
        return debugMessage;
    }

    public void setDebugMessage(String debugMessage) {
        this.debugMessage = debugMessage;
    }
}
