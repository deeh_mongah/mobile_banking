package com.binary.core.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.math.BigInteger;
import java.security.SecureRandom;

public class HelperFunctions {

    private static SecureRandom _random = new SecureRandom();

    /**
     * Generate email token
     *
     * @return email token code
     */
    public static String generateEmailToken() {
        return new BigInteger(130, _random).toString(32);
    }

    /**
     * Generate OTP
     *
     * @param len length of token
     * @return random numeric code
     */
    public static String generatePhoneToken(int len) {
        SecureRandom sr = new SecureRandom();
        String result = (sr.nextInt(9) + 1) + "";
        for (int i = 0; i < len - 2; i++) result += sr.nextInt(10);
        result += (sr.nextInt(9) + 1);
        return result;
    }

    /**
     * Hash User Password
     *
     * @param password
     * @return
     */
    public static String hashPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /**
     * Check if a plain text password matches its hashed equivalent
     *
     * @param rawpPassword   presented password
     * @param hashedPassword saved password
     * @return boolean
     */
    public static boolean checkPasswords(String rawpPassword, String hashedPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawpPassword, hashedPassword);
    }

    public static String receiptNo(){
        RRNGenerator generatesRandomRRN = RRNGenerator.getInstance("SS");
        String code = generatesRandomRRN.getRRN().toLowerCase();
        return code;
    }


}
