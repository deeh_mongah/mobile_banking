package com.binary.core.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.*;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;

import static com.google.common.collect.Lists.newArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/**"))
                .build()
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, newArrayList(
                        new ResponseMessageBuilder().code(500).message("Internal Server Error").build(),
                        new ResponseMessageBuilder().code(403).message("Restricted access to the specified resource").build(),
                        new ResponseMessageBuilder().code(401).message("Invalid input").build()
                ))
                .securitySchemes(Arrays.asList(securityScheme()))
                .securityContexts(Arrays.asList(securityContext()))
                ;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Kluster Gateway")
                .description("Banking Channel system")
                .license("")
                .licenseUrl("")
                .termsOfServiceUrl("")
                .version("1.0.0")
                .contact(new Contact("Anthony Mwawughanga", "", "amwawughanga@binary.co.ke"))
                .build();
    }

    @Bean
    public SecurityConfiguration swaggerSecurityConfiguration() {
        return new SecurityConfiguration(
                "client",
                "secret",
                "",
                "",
                "",
                ApiKeyVehicle.HEADER,
                "", ",");
    }

    private SecurityScheme securityScheme() {
        GrantType grantType = new ResourceOwnerPasswordCredentialsGrant("/oauth/token");
        SecurityScheme oauth = new OAuthBuilder().name("oauth2schema")
                .grantTypes(Arrays.asList(grantType))
                .build();
        return oauth;
    }

    private SecurityContext securityContext() {
        AuthorizationScope[] scopes = new AuthorizationScope[0];
        return SecurityContext.builder()
                .securityReferences(
                        Collections.singletonList(new SecurityReference("oauth2schema", scopes))
                )
//                .forPaths(PathSelectors.ant("/auth/**"))
                .build();
    }
}
