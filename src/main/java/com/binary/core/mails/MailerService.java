package com.binary.core.mails;

import com.sendgrid.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

/**
 * This interface defines the basic utilities that will be used to send an email
 * to a respective client
 *
 * @author Anthony
 * @category
 * @package octa
 * @since Oct 13, 2016
 */
@Transactional(readOnly = true)
@Service("sendGridMailService")
public class MailerService implements MailerServiceInterface {

    // The object used to send the email content
    private static SendGrid sendGrid;

    @Value("${sendgrid.key}")
    private String apiKey;

//    @Autowired
//    private AppAuditLogRepository appAuditLogRepo;

    /**
     * Get the object that will be used to send email via the SendGrid API
     *
     * @return Email
     */
    @Override
    public MailOptions sendGridConfig() {
        return new MailOptions();
    }

    /**
     * Send the mail message.
     *
     * @param email
     * @return Boolean
     */
    @Override
    public boolean sendMail(MailOptions email) {
        try {

            sendGrid = new SendGrid(apiKey);
            Mail mail = email.init();
            Request request = new Request();
            request.method = Method.POST;
            request.endpoint = "mail/send";
            request.body = mail.build();

            //Retrieve response
            Response response = sendGrid.api(request);

            if (200 != response.statusCode) {
//                appAuditLogRepo.mailError(response.body);
            }

            return true;
        } catch (IOException ex) {
//            appAuditLogRepo.mailError(ex.getLocalizedMessage());
//            Console.printStackTrace(ex);
            return false;
        }
    }


}
