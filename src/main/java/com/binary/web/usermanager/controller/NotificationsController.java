package com.binary.web.usermanager.controller;

import com.binary.web.usermanager.NotificationsServiceInterface;
import com.binary.web.usermanager.vm.FlagNotificationRequest;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/notifications")
public class NotificationsController {

    @Autowired private NotificationsServiceInterface notificationsService;

    /**
     * Fetch notifications
     *
     * @return ResponseEntity<?>
     */
    @ApiOperation( value="Fetch notifications.")
    @ApiResponses( value={
            @ApiResponse(code = 200, message="Request processed successful."),
            @ApiResponse(code = 500, message="Internal server error.")
    })
    @GetMapping
    public ResponseEntity<?> fetchNotifications(){
        return ResponseEntity.ok().body( notificationsService.fetchNotifications( ) );
    }

    /**
     * Fetch notifications
     *
     * @param request
     * @return ResponseEntity<?>
     */
    @ApiOperation( value="Flag notifications as read.")
    @ApiResponses( value={
            @ApiResponse(code = 200, message="Request processed successful."),
            @ApiResponse(code = 500, message="Internal server error.")
    })
    @PostMapping("/flag")
    public ResponseEntity<?> flagAsRead(@RequestBody FlagNotificationRequest request) throws Exception{
        return ResponseEntity.ok().body( notificationsService.flagAsRead( request ) );
    }


}
