package com.binary.web.usermanager.controller;

import com.binary.core.sms.SmsOptions;
import com.binary.core.sms.SmsServiceInterface;
import com.binary.web.usermanager.services.UserServiceInterface;
import com.binary.web.usermanager.vm.ChangePasswordRequest;
import com.binary.web.usermanager.vm.ProfileUpdateRequest;
import com.binary.web.usermanager.vm.SubmitCardApplication;
import com.google.common.base.Preconditions;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/users")
public class UsersController {

    @Autowired private SmsServiceInterface smsService;
    @Autowired private UserServiceInterface userServiceInterface;

    @ApiOperation(value = "Fetch member profile", notes = "Fetch member profile information")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request processed successfully"),
            @ApiResponse(code = 400, message = "Bad request. The request was unacceptable"),
            @ApiResponse(code = 401, message = "Invalid data provided"),
            @ApiResponse(code = 402, message = "Request Failed. The parameters were valid but the request failed."),
            @ApiResponse(code = 500, message = "Internal server error.")
    })
    @GetMapping("/fetch-profile")
    public ResponseEntity<?> fetchUserProfile() {
        // profile
        Map<String, Object> result = userServiceInterface.fetchMemberProfile();
        if (!result.get("status").equals("00")) {
            return ResponseEntity.status(402).body(result);
        }
        return ResponseEntity.ok().body(result);
    }

    /**
     * Change current password
     *
     * @param request
     * @return ResponseEntity<?>
     */
    @ApiOperation( value="Update current password", notes="Allows a user to change their passwords.")
    @ApiResponses( value={
            @ApiResponse(code = 201, message="Request processed successfuly."),
            @ApiResponse(code = 404, message="Invalid email address."),
            @ApiResponse(code = 409, message="Current password and the supplied equivalent do not match."),
            @ApiResponse(code = 500, message="Internal server error.")
    })
    @PostMapping( value="/change-password" )
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordRequest request){
        Preconditions.checkNotNull( request );
        Map<String, Object> map = userServiceInterface.changePassword( request );
        return ResponseEntity.ok().body( map );
    }

    /**
     * When updating profile details
     *
     * @param request
     * @return Json Response
     */
    @ApiOperation( value="Update profile details")
    @ApiResponses( value={
            @ApiResponse(code = 200, message="Request processed successfuly."),
            @ApiResponse(code = 400, message="Invalid email address."),
            @ApiResponse(code = 500, message="Internal server error.")
    })
    @PostMapping("/update-profile")
    public ResponseEntity<?> updateProfile(@RequestBody ProfileUpdateRequest request){
        Map<String, Object> map = userServiceInterface.updateProfile( request );
        if( "00".equals( (String) map.get("status")) ){

            //When to activate email
            if(  (Boolean) map.get("activateEmail") ){
//                String names = String.format("%s %s", request.getFirstName(), request.getSurname());
//                String emailToken = (String)map.get("emailToken");
//                boolean send = mailService.sendMail( new MailOptions()
//                        .setTemplateId("87fce1f8-64fe-4dbe-8332-b67c5fc478f3")
//                        .setTo( request.getNewEmailAddress(), names)
//                        .addAttribute("_firstName", names)
//                        .addAttribute("_baseUrl", env.getProperty("portal.endpoint")+"/email-stt/"+ emailToken )
//                );

                map.remove("activateEmail");
                map.remove("emailToken");
            }

            //When to activate phone number
            if(  (Boolean) map.get("activatePhone") ){
                StringBuilder message = new StringBuilder();
                message.append("Your Kluster authorization code is ")
                        .append( map.get("mobileToken"));
                smsService.sendSMS( new SmsOptions()
                        .setMessage( message.toString() )
                        .setMobileNo( request.getMobileNo() )
                );

                map.remove("activatePhone");
                map.remove("mobileToken");
            }
        }

        return ResponseEntity.ok().body( map );
    }

    /**
     * Submit requests against your ATM cards
     *
     * @param request
     * @return ResponseEntity<?>
     * @throws Exception
     */
    @PostMapping( value="/submit-card-application" )
    public ResponseEntity<?> submitCardApplication(@RequestBody SubmitCardApplication request) throws Exception{
        Preconditions.checkNotNull( request );
        return ResponseEntity.ok().body(
                userServiceInterface.submitCardApplication( request )
        );
    }
}
