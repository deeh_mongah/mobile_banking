package com.binary.web.usermanager.controller;

import com.binary.core.rpm.ResponseModel;
import com.binary.web.usermanager.services.UserService;
import com.binary.web.usermanager.services.UserServiceInterface;
import com.binary.web.usermanager.vm.*;
import com.binary.web.usermanager.vm.request.ExistingMemberVerification;
import com.google.common.base.Preconditions;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    @Autowired private UserServiceInterface userServiceInterface;

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    /**
     * Fetch SACCO branches
     *
     * @return ResponseEntity
     */
    @ApiOperation(value = "Fetch SACCO branches")
    @GetMapping("/fetch-branches")
    public ResponseEntity fetchBranches() throws Exception{
        return ResponseEntity.ok().body(userServiceInterface.fetchBranches() );
    }


    /**
     * Register a non-sacco member
     *
     * @param request
     * @return ResponseEntity
     */
    @ApiOperation(value = "Allow non-sacco members to sign up on the platform")
    @PostMapping("/non-member-registration")
    public ResponseEntity registerNonSaccoMember(@RequestBody @Valid NonSaccoMemberRequest request) throws Exception{
        return ResponseEntity.ok().body(userServiceInterface.registerNonSaccoMember(request));
    }



    /**
     * Register a non-sacco member
     *
     * @param applicationReference
     * @param file
     * @param imageType
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Allow non-sacco members to sign up on the platform")
    @PostMapping("/non-member-registration/{reference}/images")
    public ResponseEntity registerNonSaccoMemberImages(@PathVariable("reference") String applicationReference,
                                                       @RequestParam("file") MultipartFile file,
                                                       @RequestParam("fileType") ImageTypes imageType) throws Exception{
        return ResponseEntity.ok().body(
                userServiceInterface.uploadMemberImages(applicationReference, file, imageType)
        );
    }

    /**
     * Register User
     * Send account validation to email
     * Send OTP
     * Persist record
     *
     * @param request Customer registration request
     * @return Registration confirmation result
     */
    @ApiOperation(value = "Registration confirmation request", notes = "Saves user records")
    @PostMapping("/create-account")
    public ResponseEntity<?> createAccount(@RequestBody @Valid CreateAccountRequest request) throws Exception{
        return ResponseEntity.ok().body(userServiceInterface.createAccount(request));
    }


    /**
     *
     * @param request Account closure request
     * @return Closure confirmation result
     */
    @ApiOperation(value = "Account Closure confirmation request")
    @PostMapping("/{accountNo}/close-account")
    public ResponseEntity<?> closeAccount(@PathVariable("accountNo") String accountNo, @RequestBody @Valid CloseAccount request) throws Exception{
        return ResponseEntity.ok().body(userServiceInterface.closeAccount(accountNo, request));
    }


    /**
     * Validate existing Sacco members.
     *
     * @param request Sacco member no
     * @return validation status
     */
    @ApiOperation(value = "Validate member details")
    @PostMapping("/validate-member")
    public ResponseEntity<ResponseModel> validatePreExistingMembers(@RequestBody ExistingMemberVerification request) throws Exception{
        Preconditions.checkNotNull( request );
        ResponseModel response = userServiceInterface.validateMember( request );
        logger.info("Request [{0}] Response [{}]", request, response);
        return ResponseEntity.ok().body( response );
    }

    @ApiOperation(value = "Resend notification token")
    @PostMapping("/resend-token")
    public ResponseEntity<?> resendUserToken(@RequestBody ResendVerificationToken userEmail) {
        // Find the user
        Map<String, Object> result = userServiceInterface.resendVerificationToken(userEmail);
        if (!result.get("status").equals("00")) {
            return ResponseEntity.status(402).body(result);
        }
        return ResponseEntity.ok().body(result);
    }

    /**
     * Validate users registered phone No.
     *
     * @param phoneValidationRequest request object
     * @return validation status
     */
    @ApiOperation(value = "Phone No. verification via OTP", notes = "Validate token sent to user phone")
    @PostMapping("/validate-phone")
    public ResponseEntity<?> phoneValidation(@RequestBody PhoneValidationRequest phoneValidationRequest) {
        Map<String, Object> result = userServiceInterface.registrationPhoneNoValidation(phoneValidationRequest);
        if (!result.get("status").equals("00")) {
            return ResponseEntity.status(402).body(result);
        }
        return ResponseEntity.ok().body(result);
    }

    /**
     * Forgot password
     *
     * @param request Customer registration request
     * @return Registration confirmation result
     */
    @ApiOperation(value = "Reset password", notes = "Allow users to reset their passwords")
    @PostMapping("/reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody ResetPasswordRequest request) {
        return ResponseEntity.ok().body( userServiceInterface.resetPassword( request ) );
    }

    @ApiOperation(value = "Verify password reset token")
    @PostMapping("/reset-password/verify-token")
    public ResponseEntity<?> verifyResetToken(@RequestBody @Valid VerifyPasswordResetToken request) {
        return ResponseEntity.ok().body( userServiceInterface.verifyPasswordResetToken( request ) );
    }

    @ApiOperation(value = "Resend password reset token")
    @PostMapping("/reset-password/resend-token")
    public ResponseEntity<?> resendResetToken(@RequestBody ResetPasswordRequest request) {
        return ResponseEntity.ok().body( userServiceInterface.resetPassword( request ) );
    }

    @ApiOperation(value = "Reset password handlers")
    @PostMapping("/reset-password/update-password")
    public ResponseEntity<?> updatePasswordInResetFlow(@RequestBody @Valid ResetPasswordCredentials request) {
        return ResponseEntity.ok().body( userServiceInterface.resetPasswordCredentials( request ) );
    }
}
