package com.binary.web.usermanager.vm;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ResetPasswordRequest {

    @NotNull
    @NotBlank
    @NotEmpty
    @Email
    private String email;

    public String getEmail() {
        return email;
    }

    public ResetPasswordRequest setEmail(String email) {
        this.email = email;
        return this;
    }
}
