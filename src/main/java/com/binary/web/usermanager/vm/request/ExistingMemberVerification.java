package com.binary.web.usermanager.vm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@ApiModel(value = "Existing Member No Verification", description = "Verify a member no exist")
public class ExistingMemberVerification {

    @NotNull
    @NotEmpty
    @ApiModelProperty(value = "member phone no")
    private String phoneNumber;

    @NotNull
    @NotEmpty
    @Email
    @ApiModelProperty(value = "member email")
    private String email;

    @NotNull
    @NotEmpty
    @ApiModelProperty(value = "member no")
    private String memberNo;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public ExistingMemberVerification setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public ExistingMemberVerification setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public ExistingMemberVerification setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    @Override
    public String toString() {
        return "ExistingMemberVerification{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", memberNo='" + memberNo + '\'' +
                '}';
    }
}
