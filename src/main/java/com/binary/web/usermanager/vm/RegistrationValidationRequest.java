package com.binary.web.usermanager.vm;

import javax.validation.constraints.NotNull;

public class RegistrationValidationRequest {
    @NotNull
    private String email;
    @NotNull
    private String firstName;
    @NotNull
    private String nationalId;
    @NotNull
    private String phone;

    public String getEmail() {
        return email;
    }

    public RegistrationValidationRequest setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public RegistrationValidationRequest setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getNationalId() {
        return nationalId;
    }

    public RegistrationValidationRequest setNationalId(String nationalId) {
        this.nationalId = nationalId;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public RegistrationValidationRequest setPhone(String phone) {
        this.phone = phone;
        return this;
    }
}
