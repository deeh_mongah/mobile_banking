package com.binary.web.usermanager.vm;

public class OnlineActivationResponse {
    private int status;
    private String message;
    private String memberNo;
    private String activationPIN;

    public int getStatus() {
        return status;
    }

    public OnlineActivationResponse setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public OnlineActivationResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public OnlineActivationResponse setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public String getActivationPIN() {
        return activationPIN;
    }

    public OnlineActivationResponse setActivationPIN(String activationPIN) {
        this.activationPIN = activationPIN;
        return this;
    }
}
