package com.binary.web.usermanager.vm;

public enum ImageTypes {

    signature{
        @Override
        public String toString(){
            return "signature";
        }
    },
    memberPicture{
        @Override
        public String toString(){
            return "memberPicture";
        }
    },
    identificationP1{
        @Override
        public String toString(){
            return "identificationP1";
        }
    },
    identificationP2{
        @Override
        public String toString(){
            return "identificationP2";
        }
    },
}
