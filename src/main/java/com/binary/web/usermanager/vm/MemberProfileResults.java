package com.binary.web.usermanager.vm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MemberProfileResults {

    private String message;
    private int status;
    private String memberNo;
    private String fullNames;
    private String firstName;
    private String otherNames;
    private String surnName;
    private String payrollNo;
    private DefaultAccount defaultAccount;
    private String nationalID;
    private String memberPicture;
    private String branchCode;

    private String phoneNo;
    private String cellPhone;
    private String employerCode;
    private String emailAddress;

    private int totalShares;
    private int availableShares;
    private BigDecimal committedShares;
    private List<NextOfKinVm> nextOfKin = new ArrayList<>();
    private List<AtmCards> atmCards = new ArrayList<>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getFullNames() {
        return fullNames;
    }

    public void setFullNames(String fullNames) {
        this.fullNames = fullNames;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getSurnName() {
        return surnName;
    }

    public void setSurnName(String surnName) {
        this.surnName = surnName;
    }

    public String getPayrollNo() {
        return payrollNo;
    }

    public void setPayrollNo(String payrollNo) {
        this.payrollNo = payrollNo;
    }

    public DefaultAccount getDefaultAccount() {
        return defaultAccount;
    }

    public void setDefaultAccount(DefaultAccount defaultAccount) {
        this.defaultAccount = defaultAccount;
    }

    public String getNationalID() {
        return nationalID;
    }

    public void setNationalID(String nationalID) {
        this.nationalID = nationalID;
    }

    public String getMemberPicture() {
        return memberPicture;
    }

    public void setMemberPicture(String memberPicture) {
        this.memberPicture = memberPicture;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getEmployerCode() {
        return employerCode;
    }

    public void setEmployerCode(String employerCode) {
        this.employerCode = employerCode;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getTotalShares() {
        return totalShares;
    }

    public void setTotalShares(int totalShares) {
        this.totalShares = totalShares;
    }

    public int getAvailableShares() {
        return availableShares;
    }

    public void setAvailableShares(int availableShares) {
        this.availableShares = availableShares;
    }

    public BigDecimal getCommittedShares() {
        return committedShares;
    }

    public void setCommittedShares(BigDecimal committedShares) {
        this.committedShares = committedShares;
    }

    public List<NextOfKinVm> getNextOfKin() {
        return nextOfKin;
    }

    public void setNextOfKin(List<NextOfKinVm> nextOfKin) {
        this.nextOfKin = nextOfKin;
    }

    public static class DefaultAccount {
        private String accountName;
        private String classification;
        private String accountType;
        private String status;
        private String accountNo;
        private String acceptsDeposit;
        private String acceptsWithdrawal;
        private BigDecimal availableBalance= BigDecimal.ZERO;
        private BigDecimal actualBalance = BigDecimal.ZERO;
        private BigDecimal loanAmount = BigDecimal.ZERO;
        private String endDate;

        public String getAccountName() {
            return accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }

        public String getClassification() {
            return classification;
        }

        public void setClassification(String classification) {
            this.classification = classification;
        }

        public String getAccountType() {
            return accountType;
        }

        public void setAccountType(String accountType) {
            this.accountType = accountType;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAccountNo() {
            return accountNo;
        }

        public void setAccountNo(String accountNo) {
            this.accountNo = accountNo;
        }

        public String getAcceptsDeposit() {
            return acceptsDeposit;
        }

        public void setAcceptsDeposit(String acceptsDeposit) {
            this.acceptsDeposit = acceptsDeposit;
        }

        public String getAcceptsWithdrawal() {
            return acceptsWithdrawal;
        }

        public void setAcceptsWithdrawal(String acceptsWithdrawal) {
            this.acceptsWithdrawal = acceptsWithdrawal;
        }

        public BigDecimal getAvailableBalance() {
            return availableBalance;
        }

        public void setAvailableBalance(BigDecimal availableBalance) {
            this.availableBalance = availableBalance;
        }

        public BigDecimal getActualBalance() {
            return actualBalance;
        }

        public void setActualBalance(BigDecimal actualBalance) {
            this.actualBalance = actualBalance;
        }

        public BigDecimal getLoanAmount() {
            return loanAmount;
        }

        public void setLoanAmount(BigDecimal loanAmount) {
            this.loanAmount = loanAmount;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }
    }

    public static class NextOfKinVm{
        private String title;
        private String fullNames;
        private String gender;
        private String phoneNo;
        private String address1;
        private String address2;
        private String relationship;
        private String email;
        private String dob;
        private String idnumber;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getFullNames() {
            return fullNames;
        }

        public void setFullNames(String fullNames) {
            this.fullNames = fullNames;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getRelationship() {
            return relationship;
        }

        public void setRelationship(String relationship) {
            this.relationship = relationship;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getIdnumber() {
            return idnumber;
        }

        public void setIdnumber(String idnumber) {
            this.idnumber = idnumber;
        }
    }

    public List<AtmCards> getAtmCards() {
        return atmCards;
    }

    public void setAtmCards(List<AtmCards> atmCards) {
        this.atmCards = atmCards;
    }

    public static class AtmCards{
        private String cardNumber;
        private String expiryDate;
        private String linkedAccountNo;
        private String status;
        private Network network;

        public String getCardNumber() {
            return cardNumber;
        }

        public void setCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
        }

        public String getExpiryDate() {
            return expiryDate;
        }

        public void setExpiryDate(String expiryDate) {
            this.expiryDate = expiryDate;
        }

        public String getLinkedAccountNo() {
            return linkedAccountNo;
        }

        public void setLinkedAccountNo(String linkedAccountNo) {
            this.linkedAccountNo = linkedAccountNo;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Network getNetwork() {
            return network;
        }

        public void setNetwork(Network network) {
            this.network = network;
        }

        public static class Network{
            private String code;
            private String name;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }


}
