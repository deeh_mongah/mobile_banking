package com.binary.web.usermanager.vm;

import java.util.Date;

public class NotificationsVm {

    private Long id;
    private String message;
    private Date createdOn;

    public NotificationsVm(Long id, String message, Date createdOn) {
        this.id = id;
        this.message = message;
        this.createdOn = createdOn;
    }

    public Long getId() { return id; }
    public NotificationsVm setId(Long id) {
        this.id = id;
        return this;
    }

    public String getMessage() { return message; }
    public NotificationsVm setMessage(String message) {
        this.message = message;
        return this;
    }

    public Date getCreatedOn() {  return createdOn; }
    public NotificationsVm setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
        return this;
    }
}
