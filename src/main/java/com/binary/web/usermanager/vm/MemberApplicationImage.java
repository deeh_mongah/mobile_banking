package com.binary.web.usermanager.vm;

public class MemberApplicationImage {

    private ApplicationImage applicationImage;
    private String applicationGUID;


    public MemberApplicationImage(String name, byte[] image, String applicationGUID) {
        this.applicationImage = new ApplicationImage( name, image);
        this.applicationGUID = applicationGUID;
    }


    public ApplicationImage getApplicationImage() {
        return applicationImage;
    }

    public void setApplicationImage(ApplicationImage applicationImage) {
        this.applicationImage = applicationImage;
    }

    public String getApplicationGUID() {
        return applicationGUID;
    }

    public void setApplicationGUID(String applicationGUID) {
        this.applicationGUID = applicationGUID;
    }

    public static class ApplicationImage{
        private String name;
        private byte[] image;

        public ApplicationImage(){}

        public ApplicationImage(String name, byte[] image) {
            this.name = name;
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public byte[] getImage() {
            return image;
        }

        public void setImage(byte[] image) {
            this.image = image;
        }
    }
}
