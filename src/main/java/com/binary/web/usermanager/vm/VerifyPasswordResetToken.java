package com.binary.web.usermanager.vm;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class VerifyPasswordResetToken {

    @NotBlank
    @NotNull
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
