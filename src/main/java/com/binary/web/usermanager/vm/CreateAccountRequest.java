package com.binary.web.usermanager.vm;

import javax.validation.constraints.NotNull;

public class CreateAccountRequest {

    @NotNull
    private String email;

    @NotNull
    private String firstName;

    @NotNull
    private String nationalId;

    @NotNull
    private String phone;

    @NotNull
    private String password;

    private String middleName;

    @NotNull
    private String surname;

    private String memberNo;

    public String getEmail() {
        return email;
    }

    public CreateAccountRequest setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public CreateAccountRequest setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getNationalId() {
        return nationalId;
    }

    public CreateAccountRequest setNationalId(String nationalId) {
        this.nationalId = nationalId;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public CreateAccountRequest setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public CreateAccountRequest setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }

    public CreateAccountRequest setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public CreateAccountRequest setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }
}
