package com.binary.web.usermanager.vm;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class NonSaccoMemberRequest {

    @NotNull
    private String firstName;

    @NotNull
    private String middleName;

    @NotNull
    private String lastName;

    private String referrerMemberNo = "";
    private String address1 = "";
    private String address2 = "";
    private String alternatePhoneNo = "";
    private String city = "";

    @NotNull
    private String primaryPhoneNo;

    @NotNull
    private String dateOfBirth;

    private String employerCode;

    @NotNull
    private String identificationNumber;

    private String passportNumber = "";

    @NotNull
    private String emailAddress = "";

    private String pinNumber = "";
    private String gender = "";
    private String branchCode = "";

    private List<NextOfKin> nextOfKins = new ArrayList<>();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getReferrerMemberNo() {
        return referrerMemberNo;
    }

    public void setReferrerMemberNo(String referrerMemberNo) {
        this.referrerMemberNo = referrerMemberNo;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAlternatePhoneNo() {
        return alternatePhoneNo;
    }

    public void setAlternatePhoneNo(String alternatePhoneNo) {
        this.alternatePhoneNo = alternatePhoneNo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPrimaryPhoneNo() {
        return primaryPhoneNo;
    }

    public void setPrimaryPhoneNo(String primaryPhoneNo) {
        this.primaryPhoneNo = primaryPhoneNo;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmployerCode() {
        return employerCode;
    }

    public void setEmployerCode(String employerCode) {
        this.employerCode = employerCode;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPinNumber() {
        return pinNumber;
    }

    public void setPinNumber(String pinNumber) {
        this.pinNumber = pinNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public List<NextOfKin> getNextOfKins() {
        return nextOfKins;
    }

    public void setNextOfKins(List<NextOfKin> nextOfKins) {
        this.nextOfKins = nextOfKins;
    }

    public static class NextOfKin{
        private String fullName;
        private String permanentAddress;
        private float benefitPercentage;
        private String contactNumber;
        private String gender;
        private String relationshipToNextOfKinCode;


        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getPermanentAddress() {
            return permanentAddress;
        }

        public void setPermanentAddress(String permanentAddress) {
            this.permanentAddress = permanentAddress;
        }

        public float getBenefitPercentage() {
            return benefitPercentage;
        }

        public void setBenefitPercentage(float benefitPercentage) {
            this.benefitPercentage = benefitPercentage;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getRelationshipToNextOfKinCode() {
            return relationshipToNextOfKinCode;
        }

        public void setRelationshipToNextOfKinCode(String relationshipToNextOfKinCode) {
            this.relationshipToNextOfKinCode = relationshipToNextOfKinCode;
        }
    }



    public enum Gender{
        MALE{
            @Override
            public String toString(){
                return "M";
            }
        },
        FEMALE{
            @Override
            public String toString(){
                return "F";
            }
        }
    }

    public enum Relationship{
        FATHER{
            @Override
            public String toString(){
                return "FATHER";
            }
            public String code(){
                return "1";
            }
        },
        MOTHER{
            @Override
            public String toString(){
                return "MOTHER";
            }
            public String code(){
                return "2";
            }
        },
        BROTHER{
            @Override
            public String toString(){
                return "BROTHER";
            }
            public String code(){
                return "3";
            }
        },
        SISTER{
            @Override
            public String toString(){
                return "SISTER";
            }
            public String code(){
                return "4";
            }
        },
        SON{
            @Override
            public String toString(){
                return "SON";
            }
            public String code(){
                return "5";
            }
        },
        DAUGHTER{
            @Override
            public String toString(){
                return "DAUGHTER";
            }
            public String code(){
                return "6";
            }
        },
        HUSBAND{
            @Override
            public String toString(){
                return "HUSBAND";
            }
            public String code(){
                return "7";
            }
        },
        WIFE{
            @Override
            public String toString(){
                return "WIFE";
            }
            public String code(){
                return "8";
            }
        },
        OTHERS{
            @Override
            public String toString(){
                return "OTHERS";
            }
            public String code(){
                return "9";
            }
        },
    }
}
