package com.binary.web.usermanager.vm;

public class PhoneValidationRequest {
    private String token;
    private String userId;

    public String getToken() {
        return token;
    }

    public PhoneValidationRequest setToken(String token) {
        this.token = token;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public PhoneValidationRequest setUserId(String userId) {
        this.userId = userId;
        return this;
    }
}
