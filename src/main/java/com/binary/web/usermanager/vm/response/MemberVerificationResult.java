package com.binary.web.usermanager.vm.response;

public class MemberVerificationResult {

    private String firstName;
    private String memberNo;
    private String otherNames;
    private String passport;
    private String surname;
    private String message;
    private String phoneNo;
    private String email;
    private String status;


    public String getFirstName() {
        return firstName;
    }

    public MemberVerificationResult setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public MemberVerificationResult setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public MemberVerificationResult setOtherNames(String otherNames) {
        this.otherNames = otherNames;
        return this;
    }

    public String getPassport() {
        return passport;
    }

    public MemberVerificationResult setPassport(String passport) {
        this.passport = passport;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public MemberVerificationResult setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public MemberVerificationResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public MemberVerificationResult setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public MemberVerificationResult setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public MemberVerificationResult setStatus(String status) {
        this.status = status;
        return this;
    }
}
