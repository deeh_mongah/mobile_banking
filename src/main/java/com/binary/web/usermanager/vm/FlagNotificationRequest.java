package com.binary.web.usermanager.vm;

import java.util.ArrayList;
import java.util.List;

public class FlagNotificationRequest {

    private List<Long> notificationIds = new ArrayList<>();

    public List<Long> getNotificationIds() {
        return notificationIds;
    }

    public void setNotificationIds(List<Long> notificationIds) {
        this.notificationIds = notificationIds;
    }
}
