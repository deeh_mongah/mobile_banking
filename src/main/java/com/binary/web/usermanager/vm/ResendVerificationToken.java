package com.binary.web.usermanager.vm;

public class ResendVerificationToken {
    private String email;

    public String getEmail() {
        return email;
    }

    public ResendVerificationToken setEmail(String email) {
        this.email = email;
        return this;
    }
}
