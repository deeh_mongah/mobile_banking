package com.binary.web.usermanager.vm.lanster;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MemberProfileRequest {

    @JsonProperty("nationalID")
    private String nationalId = "";
    private String memberNo = "";
    private String cellPhone = "";
    private String email = "";

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public MemberProfileRequest setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
