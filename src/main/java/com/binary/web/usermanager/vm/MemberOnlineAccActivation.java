package com.binary.web.usermanager.vm;

import com.binary.web.apiauthentication.vm.Authorization;

public class MemberOnlineAccActivation {
    private String nationalID;
    private String memberNo;
    private String cellPhone;
    private String email;
    private Authorization auth;

    public String getMemberNo() {
        return memberNo;
    }

    public MemberOnlineAccActivation setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public MemberOnlineAccActivation setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public MemberOnlineAccActivation setEmail(String email) {
        this.email = email;
        return this;
    }

    public Authorization getAuth() {
        return auth;
    }

    public MemberOnlineAccActivation setAuth(Authorization auth) {
        this.auth = auth;
        return this;
    }

    public String getNationalID() {
        return nationalID;
    }

    public MemberOnlineAccActivation setNationalID(String nationalID) {
        this.nationalID = nationalID;
        return this;
    }
}
