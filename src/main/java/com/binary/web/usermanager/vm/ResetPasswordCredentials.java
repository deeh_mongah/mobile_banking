package com.binary.web.usermanager.vm;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ResetPasswordCredentials {

    @NotNull
    @NotBlank
    @NotEmpty
    private String token;

    @NotNull
    @NotBlank
    @NotEmpty
    @Size(min = 7)
    private String password;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
