package com.binary.web.usermanager.vm.lanster;

import com.binary.web.apiauthentication.vm.Authorization;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Member No Verification", description = "Verify a member no exist")
public class MemberNoVerificationRequest {

    @ApiModelProperty(value = "member national Id")
    private String nationalID = "";

    @ApiModelProperty(value = "member phone no", hidden = true)
    private String cellPhone = "";

    @ApiModelProperty(value = "member email")
    private String email = "";

    @ApiModelProperty(value = "member no")
    private String memberNo = "";

    @ApiModelProperty(hidden = true)
    private Authorization auth;

    public String getMemberNo() {
        return memberNo;
    }

    public MemberNoVerificationRequest setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public Authorization getAuth() {
        return auth;
    }

    public MemberNoVerificationRequest setAuth(Authorization auth) {
        this.auth = auth;
        return this;
    }

    public String getNationalID() {
        return nationalID;
    }

    public MemberNoVerificationRequest setNationalID(String nationalID) {
        this.nationalID = nationalID;
        return this;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public MemberNoVerificationRequest setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public MemberNoVerificationRequest setEmail(String email) {
        this.email = email;
        return this;
    }
}
