package com.binary.web.usermanager;

import com.binary.web.usermanager.vm.FlagNotificationRequest;

import java.util.Map;

public interface NotificationsServiceInterface {

    /**
     * Fetch notifications
     *
     * @return Map<String, Object>
     */
    public Map<String, Object> fetchNotifications();

    /**
     * Flag notifications
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> flagAsRead(FlagNotificationRequest request);
}
