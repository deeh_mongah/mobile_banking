package com.binary.web.usermanager.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "user_types")
public class UserTypes implements Serializable {

    public static final String SYSTEM_ADMIN = "system-admin";
    public static final String MOBILE_USER = "mobile-user";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Size(max = 100)
    @Column(name = "code")
    private String code;

    @Size(max = 100)
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "userTypeLink", fetch = FetchType.LAZY)
    private Set<Users> usersSet;

    public Long getId() {
        return id;
    }

    public UserTypes setId(Long id) {
        this.id = id;
        return this;
    }

    public String getCode() {
        return code;
    }

    public UserTypes setCode(String code) {
        this.code = code;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserTypes setName(String name) {
        this.name = name;
        return this;
    }

    public Set<Users> getUsersSet() {
        return usersSet;
    }

    public UserTypes setUsersSet(Set<Users> usersSet) {
        this.usersSet = usersSet;
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserTypes)) return false;
        UserTypes other = (UserTypes) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "User Types [ id=" + id + " ]";
    }
}
