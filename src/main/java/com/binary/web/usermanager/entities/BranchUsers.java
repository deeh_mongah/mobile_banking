package com.binary.web.usermanager.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Administrator on 2/5/2019.
 */

@Entity
@Table(name = "branches_users")
public class BranchUsers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "branch_no")
    private Long branchNo;

    @JoinColumn(name = "id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Users users;

    @JoinColumn(name = "branch_no", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Branches branchesLink;

    public BranchUsers() { }

    public Long getId() {  return id; }
    public BranchUsers setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getBranchNo() {
        return branchNo;
    }

    public BranchUsers setBranchNo(Long branchNo) {
        this.branchNo = branchNo;
        return this;
    }

    public BranchUsers setUsers(Users users) {
        this.users = users;
        return this;
    }

    @JsonIgnore
    public Branches getBranchesLink() {
        return branchesLink;
    }

    public void setBranchesLink(Branches branchesLink) {
        this.branchesLink = branchesLink;
    }

    @JsonIgnore
    public Users getUsers() {  return users; }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BranchUsers)) {
            return false;
        }
        BranchUsers other = (BranchUsers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InstitutionUsers[ id=" + id + " ]";
    }
}
