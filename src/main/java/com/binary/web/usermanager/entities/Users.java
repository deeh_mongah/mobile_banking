package com.binary.web.usermanager.entities;

import com.binary.core.utils.AppConstants;
import com.binary.web.usermanager.vm.CreateAccountRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "users")
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Size(max = 100)
    @Column(name = "FIRST_NAME", length = 100)
    private String firstName;

    @Size(max = 100)
    @Column(name = "MIDDLE_NAME", length = 100)
    private String middleName;

    @Size(max = 100)
    @Column(name = "SURNAME", length = 100)
    private String surname;

    @Size(max = 250)
    @Column(name = "EMAIL", length = 250)
    private String email;

    @Size(max = 20)
    @Column(name = "PHONE", length = 20)
    private String phone;

    @Size(max = 100)
    @Column(name = "PASSPORT", length = 100)
    private String passport;

    @Size(max = 100)
    @Column(name = "PASSWORD", length = 100)
    private String password;

    @Lob
    @Size(max = 2147483647)
    @Column(name = "PHOTO_URL", length = 2147483647)
    private String photoUrl;

    @Size(max = 200)
    @Column(name = "PHOTO_KEY", length = 200)
    private String photoKey;

    @Column(name = "EXPIRY")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiry;

    @Column(name = "ENABLED")
    private Boolean enabled = false;

    @Column(name = "NONLOCKED")
    private Boolean nonlocked = true ;

    @Column(name = "CREATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date( System.currentTimeMillis() );

    @Column(name = "UPDATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn = new Date( System.currentTimeMillis() );

    @Column(name = "DELETED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedOn;

    @Column(name = "CREATED_BY")
    private Long createdBy;

    @Column(name = "UPDATED_BY")
    private Long updatedBy;

    @Size(max = 200)
    @Column(name = "MOBILE_TOKEN", length = 200)
    private String mobileToken;

    @Size(max = 200)
    @Column(name = "EMAIL_TOKEN", length = 200)
    private String emailToken;

    @Size(max = 600)
    @Column(name = "FCM_TOKEN", length = 600)
    private String fcmToken;

    @Size(max = 200)
    @Column(name = "RESET_KEY", length = 200)
    private String resetKey;

    @Column(name = "MOBILE_VERIFIED")
    private boolean mobileVerified = false;

    @Column(name = "EMAIL_VERIFIED")
    private boolean emailVerified = false;

    @Size(max = 20)
    @Column(name = "FLAG", length = 20)
    private String flag;

    @Lob
    @Size(max = 2147483647)
    @Column(name = "EDIT_DATA", length = 2147483647)
    private String editData;

    @Size(max = 200)
    @Column(name = "REASON_DESCRIPTION", length = 200)
    private String reasonDescription;

    @Column(name = "LAST_TIME_PASSWORD_UPDATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastTimePasswordUpdated;

    @Column(name = "PASSWORD_NEVER_EXPIRES")
    private Boolean passwordNeverExpires;

    @Column(name = "RESET_REQ_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date resetReqDate;

    @Size(max = 200)
    @Column(name = "ACTIVATION_KEY", length = 200)
    private String activationKey;

    @Column( name = "REASON_CODE_NO")
    private Long reasonCodeNo;

    @Column(name = "user_type_no")
    private Long userTypeNo;


    @Size(max = 20)
    @Column(name = "MOBILE_USER_STATUS", length = 20)
    private String mobileUserStatus;

    @Column( name = "member_id", length = 100)
    private String memberNo;

    @Transient
    private String fullNames;

    @JoinColumn(name = "user_type_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private UserTypes userTypeLink;

    public Users(){}

    public Users(CreateAccountRequest request){
        this.firstName = request.getFirstName();
        this.middleName = request.getMiddleName();
        this.surname = request.getSurname();
        this.email = request.getEmail();
        this.passport = request.getNationalId();
        this.phone = request.getPhone();
        this.flag = AppConstants.STATUS_ACTIVERECORD;
        this.emailVerified = true;
        this.mobileVerified = false;
        this.enabled = true;
        this.nonlocked = true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() { return phone; }
    public Users setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getPassword() { return password; }
    public Users setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoKey() {
        return photoKey;
    }

    public void setPhotoKey(String photoKey) {
        this.photoKey = photoKey;
    }

    public Date getExpiry() {
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    public Boolean getEnabled() {  return enabled; }
    public Users setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Boolean getNonlocked() { return nonlocked; }
    public Users setNonlocked(Boolean nonlocked) {
        this.nonlocked = nonlocked;
        return this;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() { return updatedOn; }
    public Users setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getMobileToken() {
        return mobileToken;
    }

    public void setMobileToken(String mobileToken) {
        this.mobileToken = mobileToken;
    }

    public String getEmailToken() {  return emailToken;}
    public Users setEmailToken(String emailToken) {
        this.emailToken = emailToken;
        return this;
    }

    public String getFcmToken() {  return fcmToken;}
    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getResetKey() { return resetKey; }
    public Users setResetKey(String resetKey) {
        this.resetKey = resetKey;
        return this;
    }

    public boolean isMobileVerified() {  return mobileVerified; }
    public Users setMobileVerified(boolean mobileVerified) {
        this.mobileVerified = mobileVerified;
        return this;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getEditData() {
        return editData;
    }

    public void setEditData(String editData) {
        this.editData = editData;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public Users setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
        return this;
    }

    public Date getLastTimePasswordUpdated() {
        return lastTimePasswordUpdated;
    }

    public void setLastTimePasswordUpdated(Date lastTimePasswordUpdated) {
        this.lastTimePasswordUpdated = lastTimePasswordUpdated;
    }

    public Boolean getPasswordNeverExpires() {
        return passwordNeverExpires;
    }

    public void setPasswordNeverExpires(Boolean passwordNeverExpires) {
        this.passwordNeverExpires = passwordNeverExpires;
    }

    public Date getResetReqDate() {  return resetReqDate; }
    public Users setResetReqDate(Date resetReqDate) {
        this.resetReqDate = resetReqDate;
        return this;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public Long getReasonCodeNo() {
        return reasonCodeNo;
    }

    public void setReasonCodeNo(Long reasonCodeNo) {
        this.reasonCodeNo = reasonCodeNo;
    }

    public Long getUserTypeNo() { return userTypeNo; }
    public Users setUserTypeNo(Long userTypeNo) {
        this.userTypeNo = userTypeNo;
        return this;
    }

    public String getMemberNo() { return memberNo; }
    public Users setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public String getFullNames() {
        this.fullNames = this.firstName + " " + this.middleName + " " + this.surname;
        return fullNames;
    }

    public void setFullNames(String fullNames) {
        this.fullNames = fullNames;
    }


    public String getMobileUserStatus() {
        return mobileUserStatus;
    }

    public Users setMobileUserStatus(String mobileUserStatus) {
        this.mobileUserStatus = mobileUserStatus;
        return this;
    }

    @JsonIgnore public UserTypes getUserTypeLink() {  return userTypeLink; }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserTypes)) return false;
        Users other = (Users) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "Users [ id=" + id + " ]";
    }
}
