package com.binary.web.usermanager.repository;

import com.binary.web.usermanager.entities.BranchUsers;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2/12/2019.
 */
@Repository
public interface BranchesUsersRepository extends CrudRepository<BranchUsers, Long> {

}
