package com.binary.web.usermanager.repository;

import com.binary.web.usermanager.entities.UserTypes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTypeRepository extends CrudRepository<UserTypes, Long> {
    UserTypes findByCode(String code);


}
