package com.binary.web.usermanager.repository;

import com.binary.web.usermanager.entities.Notifications;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationsRepository extends CrudRepository<Notifications, Long> {

    /**
     * Fetch records given client and read flag state
     *
     * @param clientNo
     * @param read
     * @return List<Notifications>
     */
    public List<Notifications> findByClientNoAndReadOrderByIdDesc(Long clientNo, boolean read);

    /**
     * Fetch records given a set of PKS
     *
     * @param ids
     * @return List<Notifications>
     */
    public List<Notifications> findByIdIn(List<Long> ids);

}
