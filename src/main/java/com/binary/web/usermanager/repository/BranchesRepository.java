package com.binary.web.usermanager.repository;

import com.binary.web.usermanager.entities.Branches;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface BranchesRepository extends CrudRepository<Branches, Long> {

    /**
     * Fetch record by code
     *
     * @param code
     * @return Optional<Branches>
     */
    Optional<Branches> findByCode(String code);


    ArrayList<Branches> findAllByFlag(String flag);

}
