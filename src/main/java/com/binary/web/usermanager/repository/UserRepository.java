package com.binary.web.usermanager.repository;

import com.binary.web.usermanager.entities.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * User Management Repository
 * Created by neshoj on 2018-05-25.
 */
@Repository
public interface UserRepository extends CrudRepository<Users, Long> {

    Optional<Users> findByMemberNo(String memberNo);

    Optional<Users> findByEmail(String email);

    /**
     * Fetch a record given passport number
     * @param passport
     * @return Optional<Users>
     */
    Optional<Users> findByPassport(String passport);

    /**
     * Find all users with a given passport
     *
     * @param passport
     * @return List<Users>
     */
    List<Users> findAllByPassport(String passport);


    Optional<Users> findByPhone(String phone);

    List<Users> findAllByPhoneIn(List<String> phoneNumbers);

    Optional<Users> findByPhoneIn( List<String> phoneNumbers );

    Optional<Users> findByPhoneInAndUserTypeNo( List<String> phoneNumbers, Long userTypeNo );

    /**
     * Find a record by a set of values
     *
     * @param passport
     * @param memberNo
     * @param email
     * @return Optional<Users>
     */
    Optional<Users> findByPassportOrMemberNoOrEmail( String passport, String memberNo, String email );

    Optional<Users> findByPassportOrMemberNoOrEmailAndUserTypeNo( String passport, String memberNo, String email, Long userTypeNo );

    /**
     * Fetch a record given a reset key
     *
     * @param key
     * @return Optional<Users>
     */
    public Optional<Users> findByResetKey( String key);
}
