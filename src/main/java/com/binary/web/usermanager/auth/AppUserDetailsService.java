package com.binary.web.usermanager.auth;

import com.binary.core.sms.SmsService;
import com.binary.web.usermanager.entities.UserTypes;
import com.binary.web.usermanager.entities.Users;
import com.binary.web.usermanager.repository.UserRepository;
import com.binary.web.usermanager.repository.UserTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.binary.core.utils.AppConstants.STATUS_ACTIVERECORD;

@Service
public class AppUserDetailsService implements UserDetailsService {

    @Autowired private UserRepository userRepository;
    @Autowired private SmsService smsService;
    @Autowired private UserTypeRepository userTypeRepository;

    @Override
    public UserDetails loadUserByUsername(String username ) throws UsernameNotFoundException {

        //Restrict login to mobile users only
        UserTypes userType = userTypeRepository.findByCode( UserTypes.MOBILE_USER );
        Long userTypeNo = userType.getId();

        Optional<Users> oUser;

        //Allows login to use email, member ID, ID no, or phone
        boolean validPhoneNumber = smsService.validatePhoneNumber(username, "Kenya");
        if( validPhoneNumber ) {
            String intMobileFormat = smsService.getInternationalPhoneNumber(username, "Kenya");
            String nationalMobileFormat = smsService.getNationalFormat(username, "Kenya");
            String mobileNoWithNoLeadingZero = nationalMobileFormat.replaceFirst("^0+(?!$)", "");

            List<String> phoneNumbers = new ArrayList<>();
            phoneNumbers.add(intMobileFormat);
            phoneNumbers.add(nationalMobileFormat);
            phoneNumbers.add(nationalMobileFormat.replace(" ", ""));
            phoneNumbers.add(username);
            phoneNumbers.add(mobileNoWithNoLeadingZero);
            oUser = userRepository.findByPhoneInAndUserTypeNo( phoneNumbers, userTypeNo );
        }
        else{
            oUser = userRepository.findByPassportOrMemberNoOrEmailAndUserTypeNo( username, username, username, userTypeNo );
        }

        // If the user was not found, throw an exception
        if (!oUser.isPresent()) throw new UsernameNotFoundException("The username '" + username + "' was not found");


        Users entity = oUser.get();

        if(entity.getMobileUserStatus().equals("Disabled") || entity.getMobileUserStatus().equals("Suspended"))
            throw new UsernameNotFoundException("The account '" + username + "' is disabled/suspended");

        boolean activeState = entity.getFlag().equals( STATUS_ACTIVERECORD );
        return new User(
                entity.getEmail(),
                entity.getPassword(),
                activeState,
                true,
                true,
                true,
                Collections.emptySet()
        );
    }
}
