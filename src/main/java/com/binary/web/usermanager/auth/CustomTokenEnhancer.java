package com.binary.web.usermanager.auth;

import com.binary.core.http.LansterHttpService;
import com.binary.core.sms.SmsService;
import com.binary.web.usermanager.entities.BranchUsers;
import com.binary.web.usermanager.entities.Branches;
import com.binary.web.usermanager.entities.Users;
import com.binary.web.usermanager.repository.BranchesRepository;
import com.binary.web.usermanager.repository.BranchesUsersRepository;
import com.binary.web.usermanager.repository.UserRepository;
import com.binary.web.usermanager.vm.MemberProfileResults;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class CustomTokenEnhancer implements TokenEnhancer {

    @Autowired private UserRepository userRepository;
    @Autowired private LansterHttpService lansterHttpService;
    @Autowired private ObjectMapper objectMapper;
    @Autowired private Environment env;

    @Autowired private SmsService smsService;

    @Autowired private BranchesRepository branchesRepository;
    @Autowired private BranchesUsersRepository branchesUsersRepository;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication){
        Map<String, Object> map = new HashMap<>();
        String username = authentication.getName();

        Optional<Users> oUser;

        //Allows login to use email, member ID, ID no, or phone
        String mobile = username;
        boolean validPhoneNumber = smsService.validatePhoneNumber(mobile, "Kenya");
        if( validPhoneNumber ) {
            String intMobileFormat = smsService.getInternationalPhoneNumber(mobile, "Kenya");
            String mobileNoWithNoLeadingZero = (mobile).replaceFirst("^0+(?!$)", "");
            List<String> phoneNumbers = new ArrayList<>();
            phoneNumbers.add(intMobileFormat);
            phoneNumbers.add(mobile);
            phoneNumbers.add(mobileNoWithNoLeadingZero);
            oUser = userRepository.findByPhoneIn(phoneNumbers);
        }
        else{
            oUser = userRepository.findByPassportOrMemberNoOrEmail( username, username, username );
        }

        Users user = oUser.get();

        //Package profile details
        BigDecimal committedShares = BigDecimal.ZERO;
        int totalShares = 0;
        MemberProfileResults.DefaultAccount defaultAccount = null;
        List<MemberProfileResults.NextOfKinVm> nextOfKin = new ArrayList<>();
        String photoUrl = "";

        map.put("emailVerified", user.isEmailVerified());
        map.put("phoneVerified", user.isMobileVerified());

        map.put("firstName", user.getFirstName());
        map.put("middleName", user.getMiddleName() );
        map.put("surname", user.getSurname() );
        map.put("passport", user.getPassport() );
        map.put("memberNo", user.getMemberNo());

        map.put("email", user.getEmail() );
        map.put("mobileNo", user.getPhone() );

        //Fetch member shares
        //Package request
        Map<String, Object> memberVerification = new HashMap<>();
        memberVerification.put("nationalID", "");
        memberVerification.put("memberNo", user.getMemberNo());
        memberVerification.put("cellPhone", "");
        memberVerification.put("email", "");

        try {
            String uri = env.getProperty("lanster.api.get-member-profile");
            JsonNode node = objectMapper.convertValue(memberVerification, JsonNode.class);
            ResponseEntity<String> responseEntity = lansterHttpService.postRequest( uri, node);

            if( responseEntity.getStatusCode().is2xxSuccessful() ){
                MemberProfileResults profile =  objectMapper.readValue(responseEntity.getBody(), MemberProfileResults.class);
                committedShares = profile.getCommittedShares();
                totalShares = profile.getTotalShares();
                defaultAccount = profile.getDefaultAccount();
                nextOfKin = profile.getNextOfKin();
                photoUrl = profile.getMemberPicture();

                map.put("atmCards", profile.getAtmCards() );

                // Update branch code
                updateBranchField( user, profile.getBranchCode() );
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        map.put("committedShares", committedShares);
        map.put("totalShares", totalShares);
        map.put("defaultAccount", defaultAccount);
        map.put("nextOfKin", nextOfKin);
        map.put("photoUrl", "" );


        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation( map );
        return accessToken;
    }

    private void updateBranchField(Users user, String branchCode){
        Optional<BranchUsers> oBranchUser = branchesUsersRepository.findById( user.getId() );

        // When this record has not been updated
        if( !oBranchUser.isPresent() ){

            // Retrieve the branch
            Optional<Branches> oBranch = branchesRepository.findByCode( branchCode );
            if( oBranch.isPresent()  ){
                Branches branch = oBranch.get();
                BranchUsers branchUser = new BranchUsers()
                        .setId( user.getId() )
                        .setBranchNo( branch.getId() );

                // Save record
                branchesUsersRepository.save( branchUser );
            }
        }
    }
}
