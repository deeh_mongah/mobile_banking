package com.binary.web.usermanager.services;

import com.binary.core.http.LansterHttpService;
import com.binary.core.mails.MailerService;
import com.binary.core.rpm.ResponseModel;
import com.binary.core.sms.SmsOptions;
import com.binary.core.sms.SmsServiceInterface;
import com.binary.core.utils.AppConstants;
import com.binary.core.utils.HelperFunctions;
import com.binary.web.config.entities.AuditTrail;
import com.binary.web.config.repository.AuditTrailRepository;
import com.binary.web.usermanager.auth.SecurityUtils;
import com.binary.web.usermanager.entities.BranchUsers;
import com.binary.web.usermanager.entities.Branches;
import com.binary.web.usermanager.entities.UserTypes;
import com.binary.web.usermanager.entities.Users;
import com.binary.web.usermanager.repository.BranchesRepository;
import com.binary.web.usermanager.repository.BranchesUsersRepository;
import com.binary.web.usermanager.repository.UserRepository;
import com.binary.web.usermanager.repository.UserTypeRepository;
import com.binary.web.usermanager.vm.*;
import com.binary.web.usermanager.vm.lanster.MemberProfileRequest;
import com.binary.web.usermanager.vm.request.ExistingMemberVerification;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.*;

import static com.binary.core.utils.AppConstants.STATUS_ACTIVERECORD;

@Transactional
@Service("userService")
public class UserService implements UserServiceInterface {

    @Autowired private Environment env;
    @Autowired private UserRepository userRepository;
    @Autowired private SmsServiceInterface smsServiceInterface;
    @Autowired private MailerService mailerService;
    @Autowired private UserTypeRepository userTypeRepository;

    @Autowired private AuditTrailRepository auditTrailRepository;
    @Autowired private BranchesRepository branchesRepository;
    @Autowired private BranchesUsersRepository branchesUsersRepository;

    @Autowired private ObjectMapper objectMapper;
    @Autowired private LansterHttpService lansterHttpService;


    @Value("${lanster.api.get-member-profile}")
    private String memberProfileUri;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);


    /**
     * Check if a user has signed up on our platform
     * @param passportNo
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> isUserRegisteredLocally(String passportNo) {
        Map<String, Object> result = new HashMap<>();
        Optional<Users> oUser = userRepository.findByPassport( passportNo );

        // Validate Results
        if ( !oUser.isPresent() ) {
            result.put("status", "01");
            result.put("message", "Record not found");
            return result;
        }

        // Process
        result.put("status", "00");
        result.put("message", "User record exist");
        result.put("data", oUser.get() );
        return result;
    }

    /**
     * Handle final leg of registration
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> createAccount(CreateAccountRequest request) throws Exception {

        Map<String, Object> result = new HashMap<>();

        // Check if user with the details already exists
        Optional<Users> oUser = userRepository.findByEmail(request.getEmail());
        if ( oUser.isPresent()) {
            result.put("status", "01");
            result.put("message", "Email already registered with another account");
            return result;
        }

        oUser = userRepository.findByMemberNo(request.getMemberNo());
        if ( oUser.isPresent()) {
            result.put("status", "01");
            result.put("message", "Membership no already registered with another account");
            return result;
        }

        oUser = userRepository.findByPassport(request.getNationalId());
        if ( oUser.isPresent()) {
            result.put("status", "01");
            result.put("message", "Passport/National Id already registered with another account");
            return result;
        }

        oUser = userRepository.findByPhone(request.getPhone());
        if ( oUser.isPresent()) {
            result.put("status", "01");
            result.put("message", "Phone No already registered with another account");
            return result;
        }

        // Validate the member number once more
        //Package request
        Map<String, Object> memberVerification = new HashMap<>();
        memberVerification.put("nationalID", "");
        memberVerification.put("memberNo", request.getMemberNo() );
        memberVerification.put("cellPhone", "");
        memberVerification.put("email", "");

        String uri = env.getProperty("lanster.api.get-member-profile");
        JsonNode node = objectMapper.convertValue(memberVerification, JsonNode.class);
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest( uri, node);

        JsonNode jsonNode = objectMapper.readValue(responseEntity.getBody(), JsonNode.class);
        int status = jsonNode.get("status").asInt();
        if( 0 != status ){
            result.put("status", "01");
            result.put("message", jsonNode.get("message"));
            return result;
        }

        // Check for null
        String phoneToken = HelperFunctions.generatePhoneToken(4 );
        String emailToken = HelperFunctions.generateEmailToken();
        String password = HelperFunctions.hashPassword(request.getPassword());

        UserTypes userType = userTypeRepository.findByCode( UserTypes.MOBILE_USER );
        Long userTypeId = userType.getId();

        Users newUser = new Users( request );
        newUser
                .setMobileUserStatus("Active")
                .setPassword( password )
                .setUserTypeNo( userTypeId )
                .setEmailToken( emailToken )
                .setMobileToken( phoneToken )
                ;

        // Existing users
        if (!request.getMemberNo().isEmpty()) {
            // Activate member
            MemberOnlineAccActivation activationReq = new MemberOnlineAccActivation()
                    .setMemberNo(request.getMemberNo())
                    .setEmail("")
                    .setCellPhone("")
                    .setNationalID(request.getNationalId());

            OnlineActivationResponse onlineActivationResponse = this.memberOnlineAccountActivation( activationReq );
            phoneToken = (onlineActivationResponse.getStatus() == 0) ? onlineActivationResponse.getActivationPIN() : phoneToken;
            newUser
                    .setMemberNo(request.getMemberNo())
                    .setMobileToken( phoneToken );
        }

        //Save the user
        newUser = userRepository.save( newUser );

        // Fetch branch data
        // Retrieve the branch
        Optional<Branches> oBranch = branchesRepository.findByCode( jsonNode.get("branchCode").asText() );
        if( oBranch.isPresent()  ){
            Branches branch = oBranch.get();
            BranchUsers branchUser = new BranchUsers()
                    .setId( newUser.getId() )
                    .setBranchNo( branch.getId() );

            // Save record
            branchesUsersRepository.save( branchUser );
        }

        // Send notification data
//        sendEmail(request.getSurname(), request.getEmail(), emailToken );
        sendSMS(request.getPhone(), phoneToken);

        result.put("status", "00");
        result.put("message", "Account created successfully");
        result.put("token", phoneToken);
        result.put("userId", newUser.getId() );
        return result;
    }

    @Override
    public Map<String, Object> registrationPhoneNoValidation(PhoneValidationRequest request) {
        // Fetch user details
        Optional<Users> user = this.userRepository.findById(Long.valueOf(request.getUserId()));

        if (!user.isPresent()) {
            Map<String, Object> result = new HashMap<>();
            result.put("status", "01");
            result.put("message", "User record not found.");
            return result;
        }

        // validate token
        Map<String, Object> result = new HashMap<>();
        Users users = user.get();
        if (!users.getMobileToken().equals(request.getToken())) {
            result.put("status", "01");
            result.put("message", "Invalid token received");
            return result;
        }
        // Update user records
        users
                .setUpdatedOn( new Date( System.currentTimeMillis() ))
                .setEnabled( true )
                .setMobileVerified( true )
                .setNonlocked( true )
                .setFlag(STATUS_ACTIVERECORD);
        userRepository.save(users);
        result.put("status", "00");
        result.put("message", "Request processed successfully");
        return result;
    }

    /**
     * Validate member details
     *
     * @param request
     * @return ResponseModel
     * @throws Exception
     */
    @Override
    public ResponseModel validateMember(ExistingMemberVerification request) throws Exception{
        ResponseModel responseModel = null;
        String memberNo = request.getMemberNo();
        MemberProfileRequest profileRequest = new MemberProfileRequest()
                .setMemberNo( memberNo );

        boolean isValidMobileNo = smsServiceInterface.validatePhoneNumber( request.getPhoneNumber(), "Kenya");
        if( !isValidMobileNo ){
            return new ResponseModel(
                    "01",
                    "Invalid mobile number."
            );
        }

        JsonNode jsonNode = objectMapper.convertValue(profileRequest, JsonNode.class );

        // Place request
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest( memberProfileUri, jsonNode );

        jsonNode = objectMapper.readTree( responseEntity.getBody() );
        int statusCode = jsonNode.get("status").asInt();
        String message = jsonNode.get("message").asText();
        boolean hasError = false;

        //Handle response
        if( responseEntity.getStatusCode().is2xxSuccessful() ){

            //When a success response was obtained
            if( 0 == statusCode ){

                //TODO: Should we rather obtained the request from the JsonNode instance?
                MemberProfileResults profileNode = objectMapper.readValue(responseEntity.getBody(), MemberProfileResults.class);
                //Match phone number with the CBS phone number
                String cbsPhone = smsServiceInterface.getInternationalPhoneNumber( profileNode.getPhoneNo(), "Kenya");
                String inPhoneNumber = smsServiceInterface.getInternationalPhoneNumber( request.getPhoneNumber(), "Kenya");

                boolean phoneMatches = inPhoneNumber.equals( cbsPhone );
                boolean emailMatches = request.getEmail().equals( profileNode.getEmailAddress() );

                if(  phoneMatches && emailMatches ){
                    ((ObjectNode) jsonNode).remove("status");
                    ((ObjectNode) jsonNode).remove("message");
                    responseModel = new ResponseModel(
                            "00",
                            "Request processed successfully",
                            jsonNode
                    );
                }

                if( !phoneMatches ){
                    responseModel = new ResponseModel(
                            "01",
                            "Your phone number doesn't match our records."
                    );
                }

                else if( !emailMatches ){
                    responseModel = new ResponseModel(
                            "01",
                            "Your email address doesn't match our records."
                    );
                }

            }
            else hasError = true;
        }
        else hasError = true;

        if( hasError ){
            responseModel = new ResponseModel(
                    "01",
                    message
            );
        }

        return responseModel;
    }

    @Override
    public OnlineActivationResponse memberOnlineAccountActivation(MemberOnlineAccActivation request) throws Exception{
        String requestUri = env.getProperty("lanster.api.activate-member");
        JsonNode jsonNode = objectMapper.convertValue(request, JsonNode.class);
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest( requestUri, jsonNode);

        // Process result
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return objectMapper.readValue(responseEntity.getBody(), OnlineActivationResponse.class);
        } else {
            OnlineActivationResponse result = new OnlineActivationResponse()
                    .setMessage("CBS returned invalid response")
                    .setStatus(1);
            return result;
        }
    }



    @Override
    public Map<String, Object> resendVerificationToken(ResendVerificationToken email) {
        Map<String, Object> res = new HashMap<>();
        Optional<Users> byEmail = userRepository.findByEmail(email.getEmail());
        if (!byEmail.isPresent()) {
            res.put("status", "01");
            res.put("message", "Unknown email");
            return res;
        }

        // Find user
        Users user = byEmail.get();
        String token = HelperFunctions.generatePhoneToken(4 );
        user.setMobileToken( token );
        userRepository.save( user );
//        sendEmail(user.getSurname(), user.getEmail(), emai);
        sendSMS(user.getPhone(), token );

        res.put("status", "00");
        res.put("message", "Token reset successful");
        res.put("userId", String.valueOf(user.getId()));
        return res;
    }

    /**
     * Fetch one's profile details
     *
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchMemberProfile() {
        Map<String, Object> map = new HashMap<>();
        String email = SecurityUtils.currentUser();
        Optional<Users> oUser = userRepository.findByEmail( email );
        Optional<Users> entity = this.userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "Unknown account.");
            return map;
        }

        Users user = oUser.get();
        map.put("status", "00");
        map.put("message", "Request processed successful");
        map.put("firstName", user.getFirstName());
        map.put("photoUrl", user.getPhotoUrl());
        map.put("passport", user.getPassport());
        map.put("surname", user.getSurname());
        map.put("middleName", user.getMiddleName());
        map.put("mobileNo", user.getPhone() );
        map.put("memberNo", user.getMemberNo());

        BigDecimal committedShares = BigDecimal.ZERO;
        int totalShares = 0;
        MemberProfileResults.DefaultAccount defaultAccount = null;
        List<MemberProfileResults.NextOfKinVm> nextOfKin = new ArrayList<>();
        String photoUrl = "";

        //Package request
        Map<String, Object> memberVerification = new HashMap<>();
        memberVerification.put("nationalID", "");
        memberVerification.put("memberNo", user.getMemberNo());
        memberVerification.put("cellPhone", "");
        memberVerification.put("email", "");

        try{
            String uri = env.getProperty("lanster.api.get-member-profile");
            JsonNode node = objectMapper.convertValue(memberVerification, JsonNode.class);
            ResponseEntity<String> responseEntity = lansterHttpService.postRequest( uri, node);

            if( responseEntity.getStatusCode().is2xxSuccessful() ){
                MemberProfileResults memberProfile =  objectMapper.readValue(responseEntity.getBody(), MemberProfileResults.class);
                committedShares = memberProfile.getCommittedShares();
                totalShares = memberProfile.getTotalShares();
                defaultAccount = memberProfile.getDefaultAccount();
                nextOfKin = memberProfile.getNextOfKin();
//                photoUrl = memberProfile.getMemberPicture();

                map.put("atmCards", memberProfile.getAtmCards() );
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        map.put("committedShares", committedShares);
        map.put("totalShares", totalShares);
        map.put("defaultAccount", defaultAccount);
        map.put("nextOfKin", nextOfKin);
        map.put("photoUrl", photoUrl );

        return map;
    }


    /**
     * Allow a user to change their password
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> changePassword(ChangePasswordRequest request){
        Map<String, Object> map = new HashMap<>();

        String email = SecurityUtils.currentUser();
        Optional<Users> entity = this.userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "Unknown account.");
            return map;
        }

        //When the new password matches the current password
        Users user = entity.get();
        if( checkPasswords(request.getNewPassword(), user.getPassword()) ){
            map.put("status", "01");
            map.put("message", "The new password matches the current password.");
            return map;
        }

        if( !checkPasswords(request.getOldPassword(), user.getPassword())){
            map.put("status", "02");
            map.put("message", "The current password doesn't match the one supplied.");
            return map;
        }

        //Update record
        user.setPassword( hashPassword( request.getNewPassword()));
        userRepository.save( user );

        AuditTrail log = new AuditTrail()
                .setActivity("Password changed successfully.")
                .setStatus( "Success" )
                .setUserNo( user.getId() );
        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully.");
        return map;
    }

    /**
     * Allow a user to reset their password
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> resetPassword(ResetPasswordRequest request){
        Map<String, Object> map = new HashMap<>();
        Optional<Users> entity = this.userRepository.findByEmail( request.getEmail() );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "The supplied email address could not be associated with any user.");
            return map;
        }

        Users user = entity.get();
        String token = HelperFunctions.generatePhoneToken(4 );
        user
                .setResetReqDate( new Date( System.currentTimeMillis() ) )
                .setResetKey( token );
        //Update record
        userRepository.save( user );

        //Send SMS
        sendSMS( user.getPhone(), token );

        AuditTrail log = new AuditTrail()
                .setActivity("Submitted a password reset request.")
                .setStatus( "Success" )
                .setUserNo( user.getId() );
        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully.");
        return map;
    }

    /**
     * Allow a user to verify their password token
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> verifyPasswordResetToken(VerifyPasswordResetToken request){
        Map<String, Object> map = new HashMap<>();
        String token = request.getToken();

        Optional<Users> oUser = userRepository.findByResetKey( token );
        if ( oUser.isPresent() ) {
            //Should we verify the mobile token validity?

            map.put("status", "00");
            map.put("message", "Success");
        } else {
            map.put("status", "01");
            map.put("message", "Invalid token");
        }
        return map;
    }

    /**
     * Allow a user to to change their passwords in  a reset password flow
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> resetPasswordCredentials(ResetPasswordCredentials request){
        Map<String, Object> map = new HashMap<>();
        String token = request.getToken();

        Optional<Users> oUser = userRepository.findByResetKey( token );
        if ( oUser.isPresent() ) {
            Users entity = oUser.get();
            String cipherPassword = hashPassword( request.getPassword() );
            entity
                    .setResetKey( null )
                    .setResetReqDate( null )
                    .setPassword( cipherPassword )
                    .setUpdatedOn( new Date( System.currentTimeMillis() ));
            userRepository.save( entity );

            AuditTrail log = new AuditTrail()
                    .setActivity("Password reset successful.")
                    .setStatus( "Success" )
                    .setUserNo( entity.getId() );
            auditTrailRepository.save( log );

            map.put("status", "00");
            map.put("message", "Password updated successfully.");
        } else {
            map.put("status", "01");
            map.put("message", "Invalid token");
        }
        return map;
    }

    /**
     *  Profile Update request
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> updateProfile(ProfileUpdateRequest request){
        Map<String, Object> map = new HashMap<>();
        boolean activateEmail = false, activatePhone = false;
        String currentUserEmail = SecurityUtils.currentUser();

        Optional<Users> entity = this.userRepository.findByEmail( currentUserEmail );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "The supplied email address could not be associated with any account.");
            return map;
        }

        //Update entity
        Users user = entity.get();

        //When email has been changed
        String email = request.getNewEmailAddress().trim();
        if( !StringUtils.isEmpty( email )) {
            if (!ObjectUtils.nullSafeEquals(email, user.getEmail())) {

                //Check if new email exists
                Optional<Users> oEmailUser = userRepository.findByEmail( email );
                if( oEmailUser.isPresent() ){
                    map.put("status", "01");
                    map.put("message", "The new email already exists.");
                    return map;
                }
                else {
                    String token = UUID.randomUUID().toString();
                    user.setEmail(email);
                    user.setEmailToken(token);
                    user.setEmailVerified( true );

                    activateEmail = true;
                    map.put("emailToken", token);
                }
            }
        }

        //When mobile number has been changed
        String mobile = request.getMobileNo();
        String intMobileFormat = smsServiceInterface.getInternationalPhoneNumber(mobile, "Kenya");
        String currentMobileNumber = smsServiceInterface.getInternationalPhoneNumber( user.getPhone(), "Kenya");

        if( !ObjectUtils.nullSafeEquals( intMobileFormat, currentMobileNumber )){

            //Check if the mobile number exists( new )
            String mobileNoWithNoLeadingZero = ( mobile ).replaceFirst("^0+(?!$)", "");
            List<String> phoneNumbers = new ArrayList<>();
            phoneNumbers.add( intMobileFormat );
            phoneNumbers.add( mobile );
            phoneNumbers.add( mobileNoWithNoLeadingZero );
            List<Users> oPhoneClient = userRepository.findAllByPhoneIn( phoneNumbers );
            //When phone number already exists
            if( oPhoneClient.size() > 0 ){
                map.put("status", "01");
                map.put("message", "The new mobile number already exists.");
                return map;
            }
            else {

                activatePhone = true;
                String mobileToken = HelperFunctions.generatePhoneToken( 4 );
                user
                        .setPhone( intMobileFormat )
                        .setMobileVerified(false)
                        .setMobileToken(mobileToken);
                map.put("mobileToken", mobileToken);
            }
        }

        userRepository.save( user );

        map.put("status", "00");
        map.put("message", "Request processed successfully.");
        map.put("activateEmail", activateEmail);
        map.put("activatePhone", activatePhone );
        return map;
    }

    /**
     * Register non-sacco member
     *
     * @param request
     * @return ResponseModel
     */
    @Override
    public ResponseModel registerNonSaccoMember(NonSaccoMemberRequest request) throws Exception{
        ResponseModel response;

        // Check if user with the details already exists
        Optional<Users> oUser = userRepository.findByEmail(request.getEmailAddress() );
        if ( oUser.isPresent()) {
            response = new ResponseModel("01", "Email already registered with another account");
            return response;
        }

        List<Users> pUsers = userRepository.findAllByPassport(request.getIdentificationNumber() );
        if ( pUsers.size() > 0 ) {
            response = new ResponseModel("01", "Passport/National Id already registered with another account");
            return response;
        }

        oUser = userRepository.findByPhone(request.getPrimaryPhoneNo() );
        if ( oUser.isPresent()) {
            response = new ResponseModel("01", "Phone No already registered with another account");
            return response;
        }

        JsonNode jsonNode = objectMapper.convertValue(request, JsonNode.class);
        String requestUri = env.getProperty("lanster.api.users.create-non-member");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        if( responseEntity.getStatusCode().is2xxSuccessful() ){
            jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
            int status =jsonNode.get("status").asInt();
            String message = jsonNode.get("message").asText();

            // When all went well
            if( 0 == status ){
                response = new ResponseModel(
                        "00",
                        "Request processed successfully",
                        jsonNode.get("applicationGUID")
                );
            }
            else{
                response = new ResponseModel("01", message);
            }

        }
        else{
            jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
            String message = jsonNode.get("message").asText();
            response = new ResponseModel("01", message);
        }
        return response;
    }

    /**
     * Fetch branches from the CBS
     *
     * @return ResponseModel
     * @throws Exception
     */
    @Override
    public ResponseModel fetchBranches() throws Exception{
        ResponseModel response;
        JsonNode jsonNode = objectMapper.createObjectNode();
//
        String requestUri = env.getProperty("lanster.api.users.fetch-attributes");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
        int status =jsonNode.get("status").asInt();
        String message = jsonNode.get("message").asText();
        if( 0 == status ){
            response = new ResponseModel(
                    "00",
                    "Request processed successfully",
                    jsonNode.get("branches")
            );
        }
        else{
            response = new ResponseModel("01", message);
        }

        return response;
    }

    /**
     * Upload registration documents
     *
     * @param referenceNo
     * @param file
     * @param imageType
     * @return
     * @throws Exception
     */
    @Override
    public ResponseModel uploadMemberImages(String referenceNo, MultipartFile file, ImageTypes imageType) throws Exception{
        ResponseModel response;

        MemberApplicationImage applicationImage = new MemberApplicationImage(
                imageType.toString(),
                file.getBytes(),
                referenceNo
        );
        JsonNode jsonNode = objectMapper.convertValue(applicationImage, JsonNode.class);
        String requestUri = env.getProperty("lanster.api.users.upload-application-images");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
        String message = jsonNode.get("message").asText();
        if( responseEntity.getStatusCode().is2xxSuccessful() ){
            int status =jsonNode.get("status").asInt();

            // When all went well
            if( 0 == status ){
                response = new ResponseModel(
                        "00",
                        "Request processed successfully"
                );
            }
            else{
                response = new ResponseModel("01", message);
            }
        }
        else{
            response = new ResponseModel("01", message);
        }

        return response;
    }

    /**
     * Submit Loan Application
     *
     * @param request
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel submitCardApplication(SubmitCardApplication request) throws Exception{
        ResponseModel response;
        String email = SecurityUtils.currentUser();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown user account"
            );
        }

        Users user = entity.get();
        String memberNo = user.getMemberNo();
        JsonNode jsonNode = objectMapper.convertValue(request, JsonNode.class);
        ((ObjectNode)jsonNode).put("memberNo", memberNo );

        String requestUri = env.getProperty("lanster.api.users.submit-card-application");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
        String message = jsonNode.get("message").asText();
        int status = jsonNode.get("status").asInt();

        if( 0 == status ){
            response = new ResponseModel(
                    "00",
                    message
            );
        }
        else{
            response = new ResponseModel(
                    String.valueOf( status ),
                    message
            );
        }

        return response;
    }

    @Async
    public void sendEmail(String surname, String email, String token) {
        try {
            Map<String, Object> attributes = new HashMap<>();

            //Remove this piece of code in the future
            boolean sent = mailerService.sendMail(mailerService.sendGridConfig()
                    .setTo(email)
                    .setTemplateId("bc9f5c78-fa8c-4048-8d5a-583ae65c813d")
                    .setSubject("Account Setup")
                    .addAttribute("_lastname",surname)
                    .addAttribute("_baseUrl", env.getProperty("internet-banking-otp-confirm"))
            );

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Async
    public void sendSMS(String phoneNo, String secure_token) {
        try {

            String message = "Your secure code for your app account is: " +
                    secure_token;
            smsServiceInterface.sendSMS(new SmsOptions()
                    .setMobileNo(phoneNo)
                    .setMessage( message )
            );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Hash passwords
     *
     * @param password
     * @return String
     */
    private static String hashPassword(String password)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /**
     * Check if a plain text password matches its hashed equivalent
     * @param rawpPassword
     * @param hashedPassword
     * @return boolean
     */
    private static boolean checkPasswords(String rawpPassword, String hashedPassword)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawpPassword, hashedPassword);
    }

    /**
     * Handle final leg of registration
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> closeAccount(String accountNo, CloseAccount request) throws Exception {

        Map<String, Object> result = new HashMap<>();

        // Check if user with the details already exists
        String email = SecurityUtils.currentUser();
        Optional<Users> oUser = userRepository.findByEmail(email);
        if ( !oUser.isPresent()) {
            result.put("status", "01");
            result.put("message", "Account does not exist");
            return result;
        }

        Users user= oUser.get();
        user
                .setReasonDescription(request.getMessage())
                .setFlag(AppConstants.STATUS_DEACTIVATED);
        userRepository.save(user);

        result.put("status", "00");
        result.put("message", "Account deactivated successfully");
        return result;
    }

}
