package com.binary.web.usermanager.services;

import com.binary.web.usermanager.NotificationsServiceInterface;
import com.binary.web.usermanager.auth.SecurityUtils;
import com.binary.web.usermanager.entities.Notifications;
import com.binary.web.usermanager.entities.Users;
import com.binary.web.usermanager.repository.NotificationsRepository;
import com.binary.web.usermanager.repository.UserRepository;
import com.binary.web.usermanager.vm.FlagNotificationRequest;
import com.binary.web.usermanager.vm.NotificationsVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class NotificationsService implements NotificationsServiceInterface {

    @Autowired private UserRepository userRepository;
    @Autowired private NotificationsRepository notificationsRepository;

    /**
     * Fetch notifications
     *
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchNotifications(){
        Map<String, Object> map = new HashMap<>();
        List<NotificationsVm> notificationsVm = new ArrayList<>();

        String currentUserEmail = SecurityUtils.currentUser();
        Optional<Users> entity = this.userRepository.findByEmail( currentUserEmail );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "The supplied email address could not be associated with any account.");
            return map;
        }

        //Update entity
        Users user = entity.get();
        Long userNo = user.getId();

        List<Notifications> notifications = notificationsRepository.findByClientNoAndReadOrderByIdDesc( userNo, false);

        notifications.forEach( k -> {
            NotificationsVm node = new NotificationsVm(k.getId(), k.getMessage(),k.getCreatedOn() );
            notificationsVm.add( node );
        });

        map.put("status", "00");
        map.put("message", "Success");
        map.put("data", notificationsVm );
        return map;
    }

    /**
     * Flag notifications
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> flagAsRead(FlagNotificationRequest request){
        Map<String, Object> map = new HashMap<>();

        String currentUserEmail = SecurityUtils.currentUser();
        Optional<Users> entity = this.userRepository.findByEmail( currentUserEmail );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "The supplied email address could not be associated with any account.");
            return map;
        }

        if( request.getNotificationIds().isEmpty() ){
            map.put("status", "01");
            map.put("message", "No notifications keys found.");
            return map;
        }

        //Update records
        List<Notifications> notifications = notificationsRepository.findByIdIn( request.getNotificationIds() );
        for( Notifications node : notifications ){
            node.setRead( true );
        }
        notificationsRepository.saveAll( notifications );

        map.put("status", "00");
        map.put("message", "Success");
        return map;
    }
}
