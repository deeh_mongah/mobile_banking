package com.binary.web.usermanager.services;

import com.binary.core.rpm.ResponseModel;
import com.binary.web.usermanager.vm.*;
import com.binary.web.usermanager.vm.request.ExistingMemberVerification;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface UserServiceInterface {

    /**
     * Check if a user has signed up on our platform
     * @param passportNo
     * @return Map<String, Object>
     */
    public Map<String, Object> isUserRegisteredLocally(String passportNo);

    Map<String, Object> resendVerificationToken(ResendVerificationToken email);

    /**
     * Fetch one's profile details
     *
     * @return Map<String, Object>
     */
    public Map<String, Object> fetchMemberProfile();

    /**
     * Handle final leg of registration
     *
     * @param request
     * @return Map<String, Object>
     */
    Map<String, Object> createAccount(CreateAccountRequest request) throws Exception;

    Map<String, Object> registrationPhoneNoValidation(PhoneValidationRequest request);

    /**
     * Validate member details
     *
     * @param request
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel validateMember(ExistingMemberVerification request) throws Exception;

    OnlineActivationResponse memberOnlineAccountActivation(MemberOnlineAccActivation request) throws Exception;




    /**
     * Allow a user to change their password
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> changePassword(ChangePasswordRequest request);

    /**
     * Allow a user to reset their password
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> resetPassword(ResetPasswordRequest request);

    /**
     * Allow a user to verify their password token
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> verifyPasswordResetToken(VerifyPasswordResetToken request);

    /**
     * Allow a user to to change their passwords in  a reset password flow
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> resetPasswordCredentials(ResetPasswordCredentials request);


    /**
     *  Profile Update request
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> updateProfile(ProfileUpdateRequest request);

    /**
     * Register non-sacco member
     *
     * @param request
     * @return ResponseModel
     */
    public ResponseModel registerNonSaccoMember(NonSaccoMemberRequest request) throws Exception;

    /**
     * Fetch branches from the CBS
     *
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel fetchBranches() throws Exception;

    /**
     * Upload registration documents
     *
     * @param referenceNo
     * @param file
     * @param imageType
     * @return
     * @throws Exception
     */
    public ResponseModel uploadMemberImages(String referenceNo, MultipartFile file, ImageTypes imageType) throws Exception;

    /**
     * Submit ATM Card Application
     *
     * @param request
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel submitCardApplication(SubmitCardApplication request) throws Exception;


    /**
     * Submit account closure
     *
     * @param request
     * @return ResponseModel
     * @throws Exception
     */
    Map<String, Object> closeAccount(String accountNo, CloseAccount request) throws Exception;



}
