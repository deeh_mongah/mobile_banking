package com.binary.web.general.controllers;

import com.binary.web.general.GenericServiceInterface;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class GenericController {

    @Autowired private GenericServiceInterface genericService;

    @GetMapping("/fetch-banks")
    @ApiOperation(value = "Fetch list of banks")
    public ResponseEntity fetchBanks() throws Exception{
        return ResponseEntity.ok( genericService.fetchBanks() );
    }
}
