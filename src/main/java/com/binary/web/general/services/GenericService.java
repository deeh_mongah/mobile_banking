package com.binary.web.general.services;

import com.binary.core.http.LansterHttpService;
import com.binary.core.rpm.ResponseModel;
import com.binary.web.general.GenericServiceInterface;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class GenericService implements GenericServiceInterface {

    @Autowired private Environment env;
    @Autowired private ObjectMapper objectMapper;
    @Autowired private LansterHttpService lansterHttpService;

    /**
     * Fetch list of banks from the CBS
     *
     * @return ResponseModel
     * @throws Exception
     */
    @Override
    public ResponseModel fetchBanks() throws Exception{
        ResponseModel response;
        JsonNode jsonNode = objectMapper.createObjectNode();
//
        String requestUri = env.getProperty("lanster.api.users.fetch-attributes");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
        int status =jsonNode.get("status").asInt();
        String message = jsonNode.get("message").asText();
        if( 0 == status ){
            response = new ResponseModel(
                    "00",
                    "Request processed successfully",
                    jsonNode.get("destinationBanks")
            );
        }
        else{
            response = new ResponseModel("01", message);
        }

        return response;
    }
}
