package com.binary.web.general;

import com.binary.core.rpm.ResponseModel;

public interface GenericServiceInterface {

    /**
     * Fetch list if banks from the CBS
     *
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel fetchBanks() throws Exception;
}
