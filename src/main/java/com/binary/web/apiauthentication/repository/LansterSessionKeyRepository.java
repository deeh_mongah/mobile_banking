package com.binary.web.apiauthentication.repository;

import com.binary.web.apiauthentication.entities.LansterSessionKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LansterSessionKeyRepository extends CrudRepository<LansterSessionKey, Long> {
}
