package com.binary.web.apiauthentication.vm;

public class Authorization {

    private String user;
    private String pass;
    private int accountType;
    private String accountID;
    private String sessionKey;

    public String getUser() {
        return user;
    }

    public Authorization setUser(String user) {
        this.user = user;
        return this;
    }

    public String getPass() {
        return pass;
    }

    public Authorization setPass(String pass) {
        this.pass = pass;
        return this;
    }

    public int getAccountType() {
        return accountType;
    }

    public Authorization setAccountType(int accountType) {
        this.accountType = accountType;
        return this;
    }

    public String getAccountID() {
        return accountID;
    }

    public Authorization setAccountID(String accountID) {
        this.accountID = accountID;
        return this;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public Authorization setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
        return this;
    }
}
