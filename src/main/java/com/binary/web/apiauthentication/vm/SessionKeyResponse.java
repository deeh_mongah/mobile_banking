package com.binary.web.apiauthentication.vm;

public class SessionKeyResponse {

    private int status;
    private String message;
    private String sessionKey;

    public int getStatus() {
        return status;
    }

    public SessionKeyResponse setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public SessionKeyResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public SessionKeyResponse setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
        return this;
    }
}
