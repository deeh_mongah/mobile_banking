package com.binary.web.apiauthentication.vm;

public class APIAuthorization {
    private String user;
    private String pass;
    private int accountType;
    private String accountID;
    private String requestDate;
    private String requestTime;


    public String getUser() {
        return user;
    }

    public APIAuthorization setUser(String user) {
        this.user = user;
        return this;
    }

    public String getPass() {
        return pass;
    }

    public APIAuthorization setPass(String pass) {
        this.pass = pass;
        return this;
    }

    public int getAccountType() {
        return accountType;
    }

    public APIAuthorization setAccountType(int accountType) {
        this.accountType = accountType;
        return this;
    }

    public String getAccountID() {
        return accountID;
    }

    public APIAuthorization setAccountID(String accountID) {
        this.accountID = accountID;
        return this;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public APIAuthorization setRequestDate(String requestDate) {
        this.requestDate = requestDate;
        return this;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public APIAuthorization setRequestTime(String requestTime) {
        this.requestTime = requestTime;
        return this;
    }
}
