package com.binary.web.apiauthentication.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "lanster_session_key")
public class LansterSessionKey implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Size(max = 255)
    @Column(name = "session_key")
    private String sessionKey;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateOn;

    public Long getId() {
        return id;
    }

    public LansterSessionKey setId(Long id) {
        this.id = id;
        return this;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public LansterSessionKey setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
        return this;
    }

    public Date getUpdateOn() {
        return updateOn;
    }

    public LansterSessionKey setUpdateOn(Date updateOn) {
        this.updateOn = updateOn;
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LansterSessionKey)) {
            return false;
        }
        LansterSessionKey other = (LansterSessionKey) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "LansterSessionKey [ id=" + id + " ]";
    }
}
