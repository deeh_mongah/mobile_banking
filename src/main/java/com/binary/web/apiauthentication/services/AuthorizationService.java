package com.binary.web.apiauthentication.services;

import com.binary.core.http.LansterHttpService;
import com.binary.web.apiauthentication.entities.LansterSessionKey;
import com.binary.web.apiauthentication.repository.LansterSessionKeyRepository;
import com.binary.web.apiauthentication.vm.APIAuthorization;
import com.binary.web.apiauthentication.vm.SessionKeyResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
@Transactional
public class AuthorizationService {

    @Value("${lanster.api.user}")
    private String lansterAPIUser;

    @Value("${lanster.api.password}")
    private String lansterAPIPassword;

    @Value("${lanster.api.salt}")
    private String lansterAPISalt;

    @Value("${lanster.api.account-type}")
    private String lansterAccountType;

    @Value("${lanster.api.account-id}")
    private String lansterAccountID;

    private final long SESSION_VALIDITY_WINDOW_IN_MINS = 5;

    @Autowired private Environment env;
    @Autowired private LansterSessionKeyRepository sessionKeyRepository;
    @Autowired private ObjectMapper objectMapper;
    private LansterHttpService lansterHttpService;

    /**
     * Fetch user authorization
     *
     * @return
     * @throws NoSuchAlgorithmException
     */
    public Map<String, Object> findUserAuthorization( LansterHttpService lansterHttpService) throws NoSuchAlgorithmException {
        this.lansterHttpService = lansterHttpService;
        Map<String, Object> sessionKeyMap = fetchActiveAPISessionKey();
        if (sessionKeyMap.get("status").equals("01")) {
            return sessionKeyMap;
        }
        String sessionKey = String.valueOf(sessionKeyMap.get("sessionKey"));
        Map<String, Object> map = new HashMap<>();
        map.put("status", sessionKeyMap.get("status"));
        map.put("message", sessionKeyMap.get("message"));
        map.put("user", getSessionKeyUser(sessionKey));
        map.put("pass", getSessionKeyPass(sessionKey));
        map.put("accountID", lansterAccountID);
        map.put("accountType", Integer.valueOf(lansterAccountType));
        map.put("sessionKey", sessionKey);
        return map;
    }

    /**
     * Fetch Active session key
     *
     * @return session key
     */
    public Map<String, Object> fetchActiveAPISessionKey() {
        //Retrieve session key from the DB
        List<LansterSessionKey> sessionKeyList = (List<LansterSessionKey>) sessionKeyRepository.findAll();
        if ( sessionKeyList.size() > 0  ) {
            LansterSessionKey sessionKey = sessionKeyList.get( 0 );

            // Check validity
            boolean isValid = sessionKeyValidity(sessionKey.getUpdateOn());
            if ( isValid ) {
                Map<String, Object> sessionResult = new HashMap<>();
                sessionResult.put("status", "00");
                sessionResult.put("message", "session key found");
                sessionResult.put("sessionKey", sessionKey.getSessionKey());
                return sessionResult;
            }
        }

        //Fetch key remotely
        return fetchRemoteSessionKey();
    }

    /**
     * Check Session validity
     *
     * @param lastUpdated last update time
     * @return validity
     */
    private boolean sessionKeyValidity(Date lastUpdated) {
        Long timeSinceLastUpdate = new Date().getTime() - lastUpdated.getTime();
        long diffInMinutes = (TimeUnit.MILLISECONDS.toMinutes( timeSinceLastUpdate ) );
        return diffInMinutes < SESSION_VALIDITY_WINDOW_IN_MINS;
    }

    /**
     * Fetch API Session Key Remotely
     *
     * @return
     */
    private Map<String, Object> fetchRemoteSessionKey() {
        Map<String, Object> map = new HashMap<>();
        Date currentDate = new Date( System.currentTimeMillis() );
        String requestDate = (new SimpleDateFormat("yyyy-MM-dd")).format( currentDate );
        String requestTime = (new SimpleDateFormat("hh:mm:ss")).format( currentDate );
        try {
            String sha256user = getSessionKeyUser( requestDate );
            String sha256password = getSessionKeyPass(requestTime );

            APIAuthorization auth = new APIAuthorization()
                    .setAccountType(Integer.valueOf(lansterAccountType))
                    .setAccountID(lansterAccountID)
                    .setRequestDate( requestDate )
                    .setRequestTime( requestTime )
                    .setUser( sha256user )
                    .setPass( sha256password );

            // Send request to remote
            ResponseEntity<String> responseEntity = lansterHttpService.postWithoutAuth(
                    env.getProperty("lanster.api.get-session-key"),
                    auth
            );

            // Process results
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                SessionKeyResponse keyResponse = objectMapper.readValue(responseEntity.getBody(), SessionKeyResponse.class);

                if (keyResponse.getStatus() == 0) {
                    // Persist new session key
                    String newSessionKey = keyResponse.getSessionKey();
                    List<LansterSessionKey> sessionKeyList = (List<LansterSessionKey>)sessionKeyRepository.findAll();
                    if ( sessionKeyList.size() > 0 ) {
                        LansterSessionKey sessionKey = sessionKeyList.get( 0 );

                        sessionKey.setSessionKey( newSessionKey )
                                .setUpdateOn( currentDate );
                        // Update session key
                        sessionKeyRepository.save( sessionKey );
                    } else {
                        // First time created
                        LansterSessionKey sessionKey = new LansterSessionKey()
                                .setSessionKey( newSessionKey )
                                .setUpdateOn( currentDate );
                        sessionKeyRepository.save(sessionKey);
                    }
                    map.put("status", "00");
                    map.put("message", "session key found");
                    map.put("sessionKey", keyResponse.getSessionKey());
                    return map;

                } else {
                    map.put("status", "01");
                    map.put("message", keyResponse.getMessage());
                    return map;
                }
            } else {
                map.put("status", "01");
                map.put("message", "Failed to fetch CBS API session key");
                return map;
            }
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error occurred.");
            return map;
        }
    }

    /**
     * Hash User ID
     *
     * @return current user's hashed session key
     */
    public String getSessionKeyUser(String requestDateOrSessionKey) throws NoSuchAlgorithmException {
        String originalString = requestDateOrSessionKey + lansterAPIUser;
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedHash = digest.digest(originalString.getBytes(StandardCharsets.UTF_8));
        return new String(Hex.encode( encodedHash )) + lansterAPISalt;
    }

    /**
     * Hash User Password
     *
     * @return current user hashed password
     */
    public String getSessionKeyPass(String requestDateOrSessionKey) throws NoSuchAlgorithmException {
        String originalString = requestDateOrSessionKey + lansterAPIPassword;
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] byteHash = digest.digest(originalString.getBytes(StandardCharsets.UTF_8));
        return new String(Hex.encode(byteHash)) + lansterAPISalt;
    }

    /**
     * Hash a plain text appended with a given param
     */
    public String hashPlainText(String plainText, String appendParam) throws NoSuchAlgorithmException {
        String originalString = plainText + appendParam;
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] byteHash = digest.digest(originalString.getBytes(StandardCharsets.UTF_8));
        return new String(Hex.encode(byteHash)) + lansterAPISalt;
    }

}
