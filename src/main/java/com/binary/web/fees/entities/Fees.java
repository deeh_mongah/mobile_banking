package com.binary.web.fees.entities;

import com.binary.web.transactions.entities.TransactionTypes;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author mghanga
 */
@Entity
@Table(name = "fees")
public class Fees implements Serializable {

    public static final String FIXED_MODE = "fixed";
    public static final String PERCENTAGE_MODE = "percentage";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Size(max = 100)
    @Column(name = "NAME", length = 100)
    private String name;

    @Column(name = "MIN_AMOUNT", precision = 18, scale = 2)
    private BigDecimal minAmount;

    @Column(name = "MAX_AMOUNT", precision = 18, scale = 2)
    private BigDecimal maxAmount;

    @Column(name = "FEE_GROUP_NO")
    private Long feeGroupNo;

    @Column(name = "CREATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "UPDATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "DELETED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedOn;

    @Column(name = "CREATED_BY")
    private Long createdBy;

    @Column(name = "UPDATED_BY")
    private Long updatedBy;

    @Size(max = 20)
    @Column(name = "FLAG", length = 20)
    private String flag;

    @Size(max = 200)
    @Column(name = "REASON_DESCRIPTION", length = 200)
    private String reasonDescription;

    @Lob
    @Size(max = 2147483647)
    @Column(name = "EDIT_DATA", length = 2147483647)
    private String editData;

    @Column( name = "REASON_CODE_NO")
    private Long reasonCodeNo;

    @Column(name = "TRANSACTION_TYPE_NO")
    private Long transactionTypeNo;

    @Column(name = "FEE_ORIGIN_NO")
    private Long feeOriginNo;

    @Column(name = "SACCO_AMOUNT", precision = 18, scale = 2)
    private BigDecimal saccoAmount;

    @Column(name = "binary_amount", precision = 18, scale = 2)
    private BigDecimal binaryAmount;

    @Column(name = "SACCO_RATIO", precision = 18, scale = 2)
    private BigDecimal saccoRatio;

    @Column(name = "binary_ratio", precision = 18, scale = 2)
    private BigDecimal binaryRatio;

    @Size(max = 100)
    @Column(name = "PAYMENT_MODE", length = 100)
    private String paymentMode;

    @JoinColumn(name = "TRANSACTION_TYPE_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private TransactionTypes transactionTypeLink;

    @JoinColumn(name = "FEE_ORIGIN_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private FeeOrigin feeOriginLink;

    @JoinColumn(name = "FEE_GROUP_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private FeeGroups feeGroupLink;

    public Fees() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Long getFeeGroupNo() {
        return feeGroupNo;
    }

    public void setFeeGroupNo(Long feeGroupNo) {
        this.feeGroupNo = feeGroupNo;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getFlag() {  return flag;  }
    public Fees setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getEditData() {
        return editData;
    }

    public void setEditData(String editData) {
        this.editData = editData;
    }

    public Long getReasonCodeNo() {
        return reasonCodeNo;
    }

    public void setReasonCodeNo(Long reasonCodeNo) {
        this.reasonCodeNo = reasonCodeNo;
    }

    public Long getTransactionTypeNo() {
        return transactionTypeNo;
    }

    public void setTransactionTypeNo(Long transactionTypeNo) {
        this.transactionTypeNo = transactionTypeNo;
    }

    public Long getFeeOriginNo() {
        return feeOriginNo;
    }

    public void setFeeOriginNo(Long feeOriginNo) {
        this.feeOriginNo = feeOriginNo;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public void setFeeOriginLink(FeeOrigin feeOriginLink) {
        this.feeOriginLink = feeOriginLink;
    }

    public void setFeeGroupLink(FeeGroups feeGroupLink) {
        this.feeGroupLink = feeGroupLink;
    }

    public BigDecimal getSaccoAmount() {
        return saccoAmount;
    }

    public void setSaccoAmount(BigDecimal saccoAmount) {
        this.saccoAmount = saccoAmount;
    }

    public BigDecimal getSaccoRatio() {
        return saccoRatio;
    }

    public void setSaccoRatio(BigDecimal saccoRatio) {
        this.saccoRatio = saccoRatio;
    }

    public BigDecimal getBinaryAmount() {
        return binaryAmount;
    }

    public void setBinaryAmount(BigDecimal binaryAmount) {
        this.binaryAmount = binaryAmount;
    }

    public BigDecimal getBinaryRatio() {
        return binaryRatio;
    }

    public void setBinaryRatio(BigDecimal binaryRatio) {
        this.binaryRatio = binaryRatio;
    }

    @JsonIgnore public FeeGroups getFeeGroupLink() {   return feeGroupLink; }
    @JsonIgnore public TransactionTypes getTransactionTypeLink() {  return transactionTypeLink;  }
    @JsonIgnore public FeeOrigin getFeeOriginLink() {  return feeOriginLink; }

    public Fees createdOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.createdOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        this.createdBy = userId;
        return this;
    }

    public Fees updatedOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        return this;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fees)) {
            return false;
        }
        Fees other = (Fees) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Fees[ id=" + id + " ]";
    }
    
}
