package com.binary.web.fees.repository;

import com.binary.web.fees.entities.FeeOrigin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeeOriginRepository extends CrudRepository<FeeOrigin, Long> {
}
