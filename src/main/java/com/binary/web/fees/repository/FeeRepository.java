package com.binary.web.fees.repository;

import com.binary.web.fees.entities.Fees;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface FeeRepository extends CrudRepository<Fees, Long> {

    /**
     * Fetch a record given their name
     *
     * @param name
     * @return Optional<Fees>
     */
    public Optional<Fees> findByName(String name);

    /**
     * Retrieve commission values
     *
     * @param transactionTypeCode
     * @return
     */
    @Query("FROM Fees a WHERE a.transactionTypeLink.code = ?1 AND ?2 BETWEEN a.minAmount AND a.maxAmount")
    public List<Fees> fetchCommission(String transactionTypeCode, BigDecimal amount);
}
