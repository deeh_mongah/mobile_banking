package com.binary.web.fees.service;

import com.binary.web.fees.entities.Fees;
import com.binary.web.fees.repository.FeeRepository;
import com.binary.web.fundstransfer.vm.CommissionVm;
import com.binary.web.fundstransfer.vm.FeeModes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class CommissionService {

    @Autowired private FeeRepository feeRepository;

    /**
     * Fetch commission values
     *
     * @param transactionCode
     * @param amount
     * @return CommissionVm.Fee
     */
    public CommissionVm fetchCommision(String transactionCode, BigDecimal amount){
        CommissionVm commission = new CommissionVm();
        CommissionVm.Fee feeNode = new CommissionVm.Fee();
        String feeMode = FeeModes.fixed.toString();
        List<Fees> feesList = feeRepository.fetchCommission( transactionCode, amount );
        if( feesList.size() > 0  ){

            //Get the first record
            Fees fee = feesList.get( 0 );
            String paymentMode = fee.getPaymentMode();
            BigDecimal binaryAmount = BigDecimal.ZERO;
            BigDecimal saccoAmount = BigDecimal.ZERO;
            BigDecimal totalFee = BigDecimal.ZERO;

            if( Fees.FIXED_MODE.equals( paymentMode )){
                binaryAmount = fee.getBinaryAmount();
                saccoAmount = fee.getSaccoAmount();
                feeMode = FeeModes.fixed.toString();
            }
            else{
                feeMode = FeeModes.percentage.toString();
                binaryAmount = (fee.getBinaryRatio().multiply( amount ) ).divide( new BigDecimal( 100 ), 2, BigDecimal.ROUND_HALF_UP );
                saccoAmount = (fee.getSaccoRatio().multiply( amount ) ).divide( new BigDecimal( 100 ), 2, BigDecimal.ROUND_HALF_UP  );
            }

            totalFee = binaryAmount.add( saccoAmount );

            feeNode
                    .setInstitution( totalFee )
                    .setAgent( saccoAmount )
                    .setThirdParty( binaryAmount );
        }

        commission
                .setAction( transactionCode )
                .setFeeMode( feeMode )
                .setFee( feeNode );
        return commission;
    }
}
