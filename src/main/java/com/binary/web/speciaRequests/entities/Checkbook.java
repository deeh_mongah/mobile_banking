package com.binary.web.speciaRequests.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "checkbook_applications")
public class Checkbook implements Serializable {

    public static final String SUCCESS = "Success";
    public static final String FAILED = "Failed";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "collection_branch", length = 100)
    private String collectionBranch;

    @Column(name = "account_no", length = 100)
    private String accountNo;

    @Column(name = "flag", length = 50)
    private String flag;

    @Column(name = "leaf_no", length = 50)
    private Long leafNo;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date( System.currentTimeMillis() );

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn = new Date( System.currentTimeMillis() );


    public Checkbook(){}


    public Long getLeafNo() {
        return leafNo;
    }

    public Checkbook setLeafNo(Long leafNo) {
        this.leafNo = leafNo;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Checkbook setId(Long id) {
        this.id = id;
        return this;
    }

    public String getFlag() {
        return flag;
    }

    public Checkbook setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public Checkbook setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public String getCollectionBranch() {
        return collectionBranch;
    }

    public Checkbook setCollectionBranch(String collectionBranch) {
        this.collectionBranch = collectionBranch;
        return this;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public Checkbook setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public Checkbook setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }
    public Checkbook createdOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.createdOn = new Date( System.currentTimeMillis() );
        return this;
    }

    public Checkbook updatedOn(Date updatedOn){
        this.updatedOn = new Date( System.currentTimeMillis() );
        return this;
    }



    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Checkbook)) return false;
        Checkbook other = (Checkbook) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "Checkbook [ id=" + id + " ]";
    }


}
