package com.binary.web.speciaRequests.repository;

import com.binary.web.speciaRequests.entities.Checkbook;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CheckbookRepository extends CrudRepository<Checkbook, Long> {



}
