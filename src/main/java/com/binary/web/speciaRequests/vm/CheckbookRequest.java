package com.binary.web.speciaRequests.vm;

public class CheckbookRequest {

    public String accountNo;
    public String collectionBranch;
    public Long noOfLeafs;
    public String status;
    public String message;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getCollectionBranch() {
        return collectionBranch;
    }

    public void setCollectionBranch(String collectionBranch) {
        this.collectionBranch = collectionBranch;
    }

    public Long getNoOfLeafs() {
        return noOfLeafs;
    }

    public void setNoOfLeafs(Long noOfLeafs) {
        this.noOfLeafs = noOfLeafs;
    }
}
