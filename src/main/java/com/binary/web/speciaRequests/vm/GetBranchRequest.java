package com.binary.web.speciaRequests.vm;

public class GetBranchRequest {

    private String status;
    private String message;
    private String code;
    private String name;

    public GetBranchRequest() {
    }

    public GetBranchRequest(String name, String code) {
        this.name=name;
        this.code=code;
    }

    public String getName() {
        return name;
    }

    public GetBranchRequest setName(String name) {
        this.name = name;
        return this;
    }

    public String getCode() {
        return code;
    }

    public GetBranchRequest setCode(String code) {
        this.code = code;
        return this;
    }

}
