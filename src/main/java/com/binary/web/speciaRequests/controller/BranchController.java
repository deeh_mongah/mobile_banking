package com.binary.web.speciaRequests.controller;

import com.binary.web.speciaRequests.BranchServiceInterface;
import com.binary.web.speciaRequests.vm.CheckbookRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/SpecialRequests")
public class BranchController {

    @Autowired private BranchServiceInterface entityService;

    /**
     * Fetch list of Branches
     *
     * @return
     */
    @GetMapping("/Branches")
    @ApiOperation(value = "Fetch list of Branches", notes = "This method is called to get the branch details.")
    public ResponseEntity<?> fetchBranches() throws Exception{
        return ResponseEntity.ok().body( entityService.fetchBranches() );
    }


    /**
     * Submit Checkbook application
     *
     * @return
     */
    @PostMapping("/Checkbook/Application")
    @ApiOperation(value = "Submit Checkbook Application", notes = "This method is use to submit an application for a Checkbook.")
    public ResponseEntity<?> postCheckbook(CheckbookRequest request) throws Exception{
        return ResponseEntity.ok().body( entityService.postCheckbook(request) );
    }

}
