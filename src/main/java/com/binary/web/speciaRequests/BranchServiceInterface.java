package com.binary.web.speciaRequests;

import com.binary.web.speciaRequests.vm.CheckbookRequest;
import com.binary.web.speciaRequests.vm.ResponseModel;

public interface BranchServiceInterface {

  ResponseModel fetchBranches() throws Exception;


  ResponseModel postCheckbook(CheckbookRequest request) throws Exception;


}
