package com.binary.web.speciaRequests.services;

import com.binary.core.utils.AppConstants;
import com.binary.web.speciaRequests.BranchServiceInterface;
import com.binary.web.speciaRequests.entities.Checkbook;
import com.binary.web.speciaRequests.repository.CheckbookRepository;
import com.binary.web.speciaRequests.vm.CheckbookRequest;
import com.binary.web.speciaRequests.vm.GetBranchRequest;
import com.binary.web.speciaRequests.vm.ResponseModel;
import com.binary.web.usermanager.entities.Branches;
import com.binary.web.usermanager.repository.BranchesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class BranchService implements BranchServiceInterface {

    @Autowired
    private BranchesRepository branchesRepository;

    @Autowired
    private CheckbookRepository checkbookRepository;

    /**
     * Fetch branches
     *
     * @return Array List of branches
     */
    @Override
    public ResponseModel fetchBranches() throws Exception {

        List<Branches> branchesArrayList = new ArrayList<>(branchesRepository.findAllByFlag(AppConstants.STATUS_ACTIVERECORD));
        List<GetBranchRequest> responseLists = new ArrayList<>();

        if (branchesArrayList.isEmpty()) {

            return new ResponseModel("01", "Branch not found");
        }

        for (Branches node : branchesArrayList) {

            GetBranchRequest getBranchRequest = new GetBranchRequest();

            getBranchRequest.setName(node.getName());
            getBranchRequest.setCode(node.getCode());

            responseLists.add(getBranchRequest);

        }
        return new ResponseModel("00", "Successful", responseLists);
    }


    @Override
    public ResponseModel postCheckbook(CheckbookRequest request) throws Exception {

        String status = "00".equals(request.getStatus()) ? Checkbook.SUCCESS : Checkbook.FAILED;

        Checkbook checkBook = new Checkbook();
        checkBook.setUpdatedOn(new Date(System.currentTimeMillis()));
        checkBook.setCreatedOn(new Date(System.currentTimeMillis()));
        checkBook.setLeafNo(request.getNoOfLeafs());
        checkBook.setCollectionBranch(request.getCollectionBranch());
        checkBook.setAccountNo(request.getAccountNo());
        checkBook.setFlag(status);

        checkbookRepository.save(checkBook);


        return new ResponseModel("00", "Request Successful", checkBook);
    }

}
