package com.binary.web.transactions.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Administrator on 3/19/2019.
 */

@Entity
@Table(name = "loan_applications")
public class LoanApplications implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column( name = "loan_type_no")
    private Long loanTypeNo;

    @Column(name = "repayment_period")
    private Long repaymentPeriod;

    @Column(name = "amount", precision = 18, scale = 2)
    private BigDecimal amount;

    @Column(name = "channel_no")
    private Long channel;

    @Column(name = "customer_ref", length = 100)
    private String customerRef;

    @Column(name = "flag", length = 50)
    private String flag;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date( System.currentTimeMillis() );

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn = new Date( System.currentTimeMillis() );

    @Column(name = "UPDATED_BY")
    private Long updatedBy;

    @Column( name = "REASON_CODE_NO")
    private Long reasonCodeNo;

    @Size(max = 200)
    @Column(name = "REASON_DESCRIPTION", length = 200)
    private String reasonDescription;

    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private LoanApplications updatedByLink;

//    @JoinColumn(name = "REASON_CODE_NO", referencedColumnName = "ID", insertable = false, updatable = false)
//    @ManyToOne(fetch = FetchType.LAZY)
////    private ReasonCodes reasonCodeLink;

    public LoanApplications(){}

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLoanTypeNo() {
        return loanTypeNo;
    }

    public void setLoanTypeNo(Long loanTypeNo) {
        this.loanTypeNo = loanTypeNo;
    }

    public Long getRepaymentPeriod() {
        return repaymentPeriod;
    }

    public void setRepaymentPeriod(Long repaymentPeriod) {
        this.repaymentPeriod = repaymentPeriod;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getChannel() {
        return channel;
    }

    public void setChannel(Long channel) {
        this.channel = channel;
    }

    public String getCustomerRef() {
        return customerRef;
    }

    public void setCustomerRef(String customerRef) {
        this.customerRef = customerRef;
    }


    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public LoanApplications setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }


    public Long getReasonCodeNo() {
        return reasonCodeNo;
    }

    public void setReasonCodeNo(Long reasonCodeNo) {
        this.reasonCodeNo = reasonCodeNo;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    @JsonIgnore public LoanApplications getUpdatedByLink() {  return updatedByLink; }
//    @JsonIgnore public ReasonCodes getReasonCodeLink() {  return reasonCodeLink; }

    public LoanApplications createdOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.createdOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        return this;
    }

    public LoanApplications updatedOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        return this;
    }



    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LoanApplications)) return false;
        LoanApplications other = (LoanApplications) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "LoanApplications [ id=" + id + " ]";
    }

}
