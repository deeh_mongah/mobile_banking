package com.binary.web.transactions.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table( name = "failed_transactions_log")
public class FailedTransactionsLog implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column( name = "transaction_component_no")
    private Long transactionComponentNo;

    @Column( name = "http_code")
    private int httpCode;

    @Column( name = "message")
    private String message;

    @Size(max = 100)
    @Column(name = "reversal_reference_no", length = 100)
    private String reversalReferenceNo;

    @Size(max = 100)
    @Column(name = "reversal_status", length = 100)
    private String reversalStatus;

    @Column( name = "reversal_attempts")
    private Long reversalAttempts = 0L;

    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Transactions transactionLink;

    public FailedTransactionsLog(){}

    public Long getId() {  return id; }
    public FailedTransactionsLog setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getTransactionComponentNo() {  return transactionComponentNo; }
    public FailedTransactionsLog setTransactionComponentNo(Long transactionComponentNo) {
        this.transactionComponentNo = transactionComponentNo;
        return this;
    }

    public int getHttpCode() {  return httpCode; }
    public FailedTransactionsLog setHttpCode(int httpCode) {
        this.httpCode = httpCode;
        return this;
    }

    public String getMessage() {  return message; }
    public FailedTransactionsLog setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getReversalReferenceNo() {  return reversalReferenceNo; }
    public FailedTransactionsLog setReversalReferenceNo(String reversalReferenceNo) {
        this.reversalReferenceNo = reversalReferenceNo;
        return this;
    }

    public String getReversalStatus() {  return reversalStatus; }
    public FailedTransactionsLog setReversalStatus(String reversalStatus) {
        this.reversalStatus = reversalStatus;
        return this;
    }

    public Long getReversalAttempts() {   return reversalAttempts; }
    public FailedTransactionsLog setReversalAttempts(Long reversalAttempts) {
        this.reversalAttempts = reversalAttempts;
        return this;
    }

    public Transactions getTransactionLink() {
        return transactionLink;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FailedTransactionsLog)) return false;
        FailedTransactionsLog other = (FailedTransactionsLog) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "FailedTransactionsLog [ id=" + id + " ]";
    }
}
