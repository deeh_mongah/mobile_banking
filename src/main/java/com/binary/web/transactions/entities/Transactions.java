package com.binary.web.transactions.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "transactions")
public class Transactions implements Serializable {

    public static final String SUCCESS = "Success";
    public static final String FAILED = "Failed";

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Size(max = 100)
    @Column(name = "reference_no", length = 100)
    private String referenceNo;

    @Column(name = "amount", precision = 18, scale = 2)
    private BigDecimal amount;

    @Column(name = "sacco_fee", precision = 18, scale = 2 )
    private BigDecimal saccoFee;

    @Column(name = "binary_fee", precision = 18, scale = 2 )
    private BigDecimal binaryFee;

    @Column( name = "user_no")
    private Long userNo;

    @Column( name = "transaction_type_no")
    private Long transactionTypeNo;

    @Column(name = "flag", length = 100)
    private String flag;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date( System.currentTimeMillis() );

    public Transactions(){}

    public Long getId() { return id; }
    public Transactions setId(Long id) {
        this.id = id;
        return this;
    }

    public String getReferenceNo() { return referenceNo; }
    public Transactions setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
        return this;
    }

    public BigDecimal getAmount() { return amount; }
    public Transactions setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public BigDecimal getSaccoFee() {  return saccoFee; }
    public Transactions setSaccoFee(BigDecimal saccoFee) {
        this.saccoFee = saccoFee;
        return this;
    }

    public BigDecimal getBinaryFee() { return binaryFee;}
    public Transactions setBinaryFee(BigDecimal binaryFee) {
        this.binaryFee = binaryFee;
        return this;
    }

    public Long getUserNo() { return userNo; }
    public Transactions setUserNo(Long userNo) {
        this.userNo = userNo;
        return this;
    }

    public Long getTransactionTypeNo() {  return transactionTypeNo; }
    public Transactions setTransactionTypeNo(Long transactionTypeNo) {
        this.transactionTypeNo = transactionTypeNo;
        return this;
    }

    public String getFlag() {  return flag; }
    public Transactions setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public Date getCreatedOn() {  return createdOn; }
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transactions)) return false;
        Transactions other = (Transactions) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "Transactions [ id=" + id + " ]";
    }
}

