package com.binary.web.transactions.repositories;

import com.binary.web.transactions.entities.FailedTransactionsLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FailedTnxLogRepository extends CrudRepository<FailedTransactionsLog, Long> {
}
