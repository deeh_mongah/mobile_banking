package com.binary.web.transactions.repositories;

import com.binary.web.transactions.entities.TransactionTypes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TransactionTypesRepository extends CrudRepository<TransactionTypes, Long> {

    /**
     * Fetch record by code
     *
     * @param code
     * @return Optional<TransactionTypes>
     */
    public Optional<TransactionTypes> findByCode( String code);
}
