package com.binary.web.transactions.repositories;

import com.binary.web.transactions.entities.TransactionComponents;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TransactionComponentsRepository extends CrudRepository<TransactionComponents, Long> {

    public Optional<TransactionComponents> findByCode(String code );
}
