package com.binary.web.transactions.repositories;

import com.binary.web.transactions.entities.ExternalTransactionsLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExternalTnxLogRepository extends CrudRepository<ExternalTransactionsLog, Long> {
}
