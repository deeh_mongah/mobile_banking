package com.binary.web.transactions.repositories;

import com.binary.web.transactions.entities.LoanApplications;
import org.springframework.data.repository.CrudRepository;

public interface LoanApplicationRepository extends CrudRepository<LoanApplications, Long> {
}
