package com.binary.web.transactions.repositories;

import com.binary.web.transactions.entities.Transactions;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TransactionsRepository extends CrudRepository<Transactions, Long> {

}
