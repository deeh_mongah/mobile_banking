package com.binary.web.accounts;

import com.binary.web.accounts.vm.*;

import java.util.Map;

public interface AccountsServiceInterface {

    /**
     * Fetch Customer A/Cs
     *
     * @return Array List of customer A/Cs
     */
    public GetAccountsResponse fetchAccounts() throws Exception;

    /**
     * Fetch account statement
     *
     * @param accountNo
     * @param request
     * @return AccStatementResponse
     * @throws Exception
     */
    public AccStatementResponse fetchAccountStatement(String accountNo, AccStatementRequest request) throws Exception;

    /**
     * Deposit money to FOSA account from M-Pesa
     *
     * @param destinationAccount
     * @param action
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    public Map<String, Object> deposit(String destinationAccount,
                                       AccountActions action,
                                       DepositRequest request) throws Exception;

    /**
     * Withdraw money from FOSA account to M-Pesa
     *
     * @param sourceAccountNo
     * @param action
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    public Map<String, Object> withdraw(String sourceAccountNo,
                                        AccountActions action,
                                        WithdrawalRequest request) throws Exception;

    /**
     * Transfer funds between speciaRequests
     *
     * @param sourceAccountNo
     * @param action
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    public Map<String, Object> transferBtwAccounts(String sourceAccountNo,
                                                   AccountActions action,
                                                   AccTransferRequest request )throws Exception;

    /**
     * Transfer funds to other banks
     *
     * @param sourceAccountNo
     * @param action
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    public Map<String, Object> transferToOtherBanks(String sourceAccountNo,
                                                    AccountActions action,
                                                    ExternalTransferRequest request )throws Exception;

    /**
     * Repay loan using a FOSA speciaRequests
     *
     * @param sourceAccountNo
     * @param action
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    public Map<String, Object> repayLoan(String sourceAccountNo,
                                         AccountActions action,
                                         AccTransferRequest request )throws Exception;
}
