package com.binary.web.accounts.controller;

import com.binary.web.accounts.AccountsServiceInterface;
import com.binary.web.accounts.vm.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/accounts")
public class AccountController {

    @Autowired private AccountsServiceInterface entityService;

    /**
     * Fetch list of user
     *
     * @return
     */
    @ApiOperation(value = "Fetch list of accounts")
    @GetMapping
    public ResponseEntity<?> fetchAccounts() throws Exception{
        GetAccountsResponse response = entityService.fetchAccounts( );
        return ResponseEntity.ok().body( response );
    }

    /**
     * Fetch account statement
     *
     * @param request
     * @return
     */
    @ApiOperation(value = "Fetch account statement", notes = "This method is called to get the statement to a member’s " +
            "account. This is currently valid only for the current financial year.")
    @PostMapping("/{accountNo}/statement")
    public ResponseEntity<?> fetchMiniStatement(@PathVariable("accountNo") String accountNo,
                                                @RequestBody AccStatementRequest request) throws Exception {
        AccStatementResponse result = entityService.fetchAccountStatement(accountNo, request);
        return ResponseEntity.ok().body( result );
    }

    @PostMapping("/{accountNo}/deposit")
    public ResponseEntity<?> depositFromMpesa(@PathVariable("accountNo") String accountNo,
                                              @RequestParam("action")AccountActions action,
                                              @RequestBody DepositRequest request) throws Exception{

        Map<String, Object> response = entityService.deposit(
                accountNo,
                action,
                request
        );
        return ResponseEntity.ok().body( response );
    }

    @PostMapping("/{accountNo}/withdraw")
    public ResponseEntity<?> withdrawToMpesa(@PathVariable("accountNo") String accountNo,
                                             @RequestParam("action")AccountActions action,
                                             @RequestBody WithdrawalRequest request) throws Exception{
        Map<String, Object> response = entityService.withdraw(
                accountNo,
                action,
                request
        );
        return ResponseEntity.ok().body( response );
    }

    /**
     * Transfer funds between two FOSA speciaRequests
     *
     * @param accountNo
     * @param action
     * @param accTransferRequest
     * @return ResponseEntity<?>
     * @throws Exception
     */
    @ApiOperation(value = "Transfer funds to internal speciaRequests")
    @PostMapping("/{accountNo}/transfer")
    public ResponseEntity<?> transferToInternalAccount(@PathVariable("accountNo") String accountNo,
                                                       @RequestParam("action")AccountActions action,
                                                       @RequestBody AccTransferRequest accTransferRequest) throws Exception{
        Map<String, Object> response = entityService.transferBtwAccounts(
                accountNo,
                action,
                accTransferRequest
        );
        return ResponseEntity.ok().body( response );
    }

    /**
     * Transfer funds between two FOSA speciaRequests
     *
     * @param accountNo
     * @param action
     * @param accTransferRequest
     * @return ResponseEntity<?>
     * @throws Exception
     */
    @ApiOperation(value = "Transfer funds to internal speciaRequests")
    @PostMapping("/{accountNo}/external-transfer")
    public ResponseEntity<?> transferToExternalAccount(@PathVariable("accountNo") String accountNo,
                                                       @RequestParam("action")AccountActions action,
                                                       @RequestBody ExternalTransferRequest accTransferRequest) throws Exception{
        Map<String, Object> response = entityService.transferToOtherBanks(
                accountNo,
                action,
                accTransferRequest
        );
        return ResponseEntity.ok().body( response );
    }

    /**
     * Transfer funds between two FOSA speciaRequests
     *
     * @param accountNo
     * @param action
     * @param accTransferRequest
     * @return ResponseEntity<?>
     * @throws Exception
     */
    @PostMapping("/{accountNo}/repay")
    public ResponseEntity<?> repayLoan(@PathVariable("accountNo") String accountNo,
                                       @RequestParam("action")AccountActions action,
                                       @RequestBody AccTransferRequest accTransferRequest) throws Exception{
        Map<String, Object> response = entityService.repayLoan(
                accountNo,
                action,
                accTransferRequest
        );
        return ResponseEntity.ok().body( response );
    }
}
