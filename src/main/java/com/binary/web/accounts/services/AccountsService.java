package com.binary.web.accounts.services;

import com.binary.web.accounts.AccountsServiceInterface;
import com.binary.web.accounts.vm.*;
import com.binary.web.fees.service.CommissionService;
import com.binary.web.fundstransfer.service.FundsTransferHelper;
import com.binary.web.fundstransfer.vm.CommissionVm;
import com.binary.web.fundstransfer.vm.FundsTransferTypes;
import com.binary.web.reports.services.ReportService;
import com.binary.web.reports.vm.TransactionsEvent;
import com.binary.web.transactions.entities.FailedTransactionsLog;
import com.binary.web.transactions.entities.TransactionComponents;
import com.binary.web.transactions.entities.TransactionTypes;
import com.binary.web.transactions.entities.Transactions;
import com.binary.web.transactions.repositories.TransactionTypesRepository;
import com.binary.web.transactions.repositories.TransactionsRepository;
import com.binary.web.usermanager.auth.SecurityUtils;
import com.binary.web.usermanager.entities.Notifications;
import com.binary.web.usermanager.entities.Users;
import com.binary.web.usermanager.repository.NotificationsRepository;
import com.binary.web.usermanager.repository.UserRepository;
import com.binary.web.usermanager.vm.MemberProfileResults;
import com.binary.web.usermanager.vm.lanster.MemberProfileRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
@Transactional
public class AccountsService extends FundsTransferHelper implements AccountsServiceInterface {

    @Autowired private Environment env;
    @Autowired private CommissionService commissionService;

    @Autowired private UserRepository userRepository;
    @Autowired private TransactionsRepository transactionsRepository;
    @Autowired private NotificationsRepository notificationsRepository;
    @Autowired private TransactionTypesRepository transactionTypesRepository;

    @Autowired private ReportService reportService;


    /**
     * Fetch Customer A/Cs
     *
     * @return Array List of customer A/Cs
     */
    @Override
    public GetAccountsResponse fetchAccounts() throws Exception{
        GetAccountsResponse response = null;

        String email = SecurityUtils.currentUser();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            response = new GetAccountsResponse("01", "Unknown user account.");
            return response;
        }

        Users user = entity.get();
        String memberNo = user.getMemberNo();

        JsonNode jsonNode = objectMapper.createObjectNode();
        ((ObjectNode) jsonNode).put("memberNo", memberNo);

        String requestUri = env.getProperty("lanster.api.member-accounts");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        if( responseEntity.getStatusCode().is2xxSuccessful() ){
            response =  objectMapper.readValue(
                    responseEntity.getBody(),
                    GetAccountsResponse.class
            );
        }
        else{
            jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
            String message = jsonNode.get("message").asText();
            response = new GetAccountsResponse(
                    "01",
                    message
            );
        }

        return response;
    }

    /**
     * Fetch account statement
     *
     * @param accountNo
     * @param request
     * @return AccStatementResponse
     * @throws Exception
     */
    @Override
    public AccStatementResponse fetchAccountStatement(String accountNo, AccStatementRequest request) throws Exception {
        AccStatementResponse response;

        String email = SecurityUtils.currentUser();
        Optional<Users> entity = userRepository.findByEmail(email);
        if (!entity.isPresent()) {
            response = new AccStatementResponse("01", "Unknown user account.");
            return response;
        }

        //gettting the difference between start and end dates

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date startDate = sdf.parse(request.getStartDate());
        Date endDate = sdf.parse(request.getEndDate());

        long diffInMillies = Math.abs(endDate.getTime() - startDate.getTime());
        long diffInDays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

        // Restrict the number of days to a max of 180 days
        if (diffInDays > 180) {

            response = new AccStatementResponse(
                    "01",
                    "Period out of range must be between 6 months"
            );
            return response;
            
        } 
        
        else if (diffInDays > 90) {

            Users user = entity.get();
            String memberNo = user.getMemberNo();

            JsonNode jsonNode = objectMapper.createObjectNode();
            ((ObjectNode) jsonNode)
                    .put("memberNo", memberNo)
                    .put("accountNo", accountNo)
                    .put("startDate", request.getStartDate())
                    .put("endDate", request.getEndDate())
                    .put("accountType", request.getAccountType());

            String requestUri = env.getProperty("lanster.api.member-account-statement");
            
            //TODO: Unchecked call
            ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                    requestUri,
                    jsonNode
            );

            response = new AccStatementResponse(
                    "00",
                    "Statement sent to your mail"
            );
            return response;
        }

        Users user = entity.get();
        String memberNo = user.getMemberNo();

        JsonNode jsonNode = objectMapper.createObjectNode();
        ((ObjectNode) jsonNode)
                .put("memberNo", memberNo)
                .put("accountNo", accountNo)
                .put("startDate", request.getStartDate())
                .put("endDate", request.getEndDate())
                .put("accountType", request.getAccountType());

        String requestUri = env.getProperty("lanster.api.member-account-statement");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            response = objectMapper.readValue(
                    responseEntity.getBody(),
                    AccStatementResponse.class
            );
        } else {
            jsonNode = objectMapper.readValue(responseEntity.getBody(), JsonNode.class);
            String message = jsonNode.get("message").asText();
            response = new AccStatementResponse(
                    "01",
                    message
            );
        }

        //Report event
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest httpServletRequest = attr.getRequest();
        TransactionsEvent event = new TransactionsEvent()
                .setStatus(response.getStatus())
                .setTransactionType("Mini-Statement");

        reportService.transactionEvent(event, httpServletRequest.getHeader("X-Channel"));

        return response;

    }

    /**
     * Deposit money to FOSA account from M-Pesa
     *
     * @param destinationAccount
     * @param action
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    @Override
    public Map<String, Object> deposit(String destinationAccount,
                                       AccountActions action,
                                       DepositRequest request) throws Exception{

        String email = SecurityUtils.currentUser();
        Map<String, Object> map = new HashMap<>();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "Unknown user account.");
            return map;
        }

        Users user = entity.get();
        Long userNo = user.getId();
        String memberNo = user.getMemberNo();
        BigDecimal amount = request.getAmount();
        String transactionType = TransactionTypes.DEPOSIT;
        String pin = request.getTransactionPin();
        boolean isCompleteRequest = action.equals( AccountActions.complete );

        // Update destination accordingly
//        String formattedDestination = String.format("ACCT#%s", destinationAccount);
        String formattedDestination = String.format("%s#%s", request.getIdentifyType().accountPrefix(), destinationAccount);

        //Generate request object
        ApiAccountRequest apiAccountRequest = new ApiAccountRequest(
                memberNo,
                request.getPhoneNumber(),
                amount,
                FundsTransferTypes.DEPOSIT_FROM_MPESA.toString(),
                "",
                formattedDestination,
                request.getReferenceNo(),
                null
        );

        //Handle request
        map =  handleRequest( apiAccountRequest,transactionType, action, pin, userNo );



        return map;
    }

    /**
     * Withdraw money from FOSA account to M-Pesa
     *
     * @param sourceAccountNo
     * @param action
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    @Override
    public Map<String, Object> withdraw(String sourceAccountNo,
                                       AccountActions action,
                                        WithdrawalRequest request) throws Exception{

        String email = SecurityUtils.currentUser();
        Map<String, Object> map = new HashMap<>();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "Unknown user account.");
            return map;
        }

        Users user = entity.get();
        Long userNo = user.getId();
        String memberNo = user.getMemberNo();
        BigDecimal amount = request.getAmount();
        String transactionType = TransactionTypes.WITHDRAWAL;
        String pin = request.getTransactionPin();
        boolean isCompleteRequest = action.equals( AccountActions.complete );

        //Generate request object
        ApiAccountRequest apiAccountRequest = new ApiAccountRequest(
                memberNo,
                sourceAccountNo,
                amount,
                FundsTransferTypes.WITHDRAW_TO_MPESA.toString(),
                "",
                request.getPhoneNumber(),
                request.getReferenceNo(),
                null
        );

        //Handle request
        map =  handleRequest( apiAccountRequest,transactionType, action, pin, userNo );

        //Generate notifications
        //TODO: Log the external GUID for further querying of data
        // The following underlying DB transactions should not cause a rollback
        //for a successful transaction
        if( isCompleteRequest ) {
            String message = "";
            String status = (String) map.get("status");
            if ("00".equals(status)) {
                message = new StringBuilder().append("You've sent KES ").append( amount )
                        .append(" to ").append(request.getPhoneNumber())
                        .append(" successfully. ").toString();
                ;
            } else {
                message = new StringBuilder().append("An attempt to sent KES ").append(amount)
                        .append(" to ").append(request.getPhoneNumber())
                        .append(" has failed. Contact customer care for further details. ")
                        .toString()
                ;
            }

            Notifications notification = new Notifications();
            notification
                    .setClientNo(userNo)
                    .setMessage(message);

            //Save a record
            notificationsRepository.save(notification);

//            //Send SMS
//            boolean send = smsService.sendSMS(new SmsOptions()
//                    .setMessage(message)
//                    .setMobileNo(user.getPhone())
//            );

        }

        return map;
    }

    /**
     * Transfer funds between speciaRequests
     *
     * @param sourceAccountNo
     * @param action
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    @Override
    public Map<String, Object> transferBtwAccounts(String sourceAccountNo,
                                                   AccountActions action,
                                                   AccTransferRequest request )throws Exception{

        String email = SecurityUtils.currentUser();
        Map<String, Object> map = new HashMap<>();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "Unknown user account.");
            return map;
        }

        Users user = entity.get();
        Long userNo = user.getId();
        String memberNo = user.getMemberNo();
        BigDecimal amount = request.getAmount();

        String pin = request.getTransactionPin();
        boolean isCompleteRequest = action.equals( AccountActions.complete );

        String transactionType;
        String paymentCategory = request.getPaymentCategory();
        if("internal-self".equals( paymentCategory ) ){
            transactionType = TransactionTypes.INTERNAL_TRANSFER_TO_SELF;
        }
        else transactionType = TransactionTypes.INTERNAL_TRANSFER_TO_OTHERS;

        //Generate request object
        ApiAccountRequest apiAccountRequest = new ApiAccountRequest(
                memberNo,
                sourceAccountNo,
                amount,
                request.getIdentifyType().paymentOption(),
                "",
                request.getDestinationAccount(),
                request.getReferenceNo(),
                null
        );

        //Handle request
        map =  handleRequest( apiAccountRequest,transactionType, action, pin, userNo );

        //Generate notifications
        if( isCompleteRequest ) {
            String message = "";
            String status = (String) map.get("status");
            if ("00".equals(status)) {
                MemberProfileResults.DefaultAccount defaultAccount = fetchDefaultAccount( memberNo );
                BigDecimal availableBalance = defaultAccount.getAvailableBalance();
                message = new StringBuilder().append("You've transfered KES ").append(amount)
                        .append(" to ").append(request.getDestinationAccount())
                        .append(" successfully. Your default account new balance is KES ").append(availableBalance)
                        .toString()
                ;
            } else {
                message = new StringBuilder().append("An attempt to transfer KES ").append(amount)
                        .append(" to ").append(request.getDestinationAccount())
                        .append(" has failed. Contact customer care for further details. ")
                        .toString()
                ;
            }

            Notifications notification = new Notifications();
            notification
                    .setClientNo(userNo)
                    .setMessage(message);

            //Save a record
            notificationsRepository.save(notification);

//            //Send SMS
//            boolean send = smsService.sendSMS(new SmsOptions()
//                    .setMessage(message)
//                    .setMobileNo(user.getPhone())
//            );
        }

        return map;
    }

    /**
     * Transfer funds to other banks
     *
     * @param sourceAccountNo
     * @param action
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    @Override
    public Map<String, Object> transferToOtherBanks(String sourceAccountNo,
                                                    AccountActions action,
                                                    ExternalTransferRequest request) throws Exception {

        String email = SecurityUtils.currentUser();
        Map<String, Object> map = new HashMap<>();
        Optional<Users> entity = userRepository.findByEmail(email);
        if (!entity.isPresent()) {
            map.put("status", "01");
            map.put("message", "Unknown user account.");
            return map;
        }

        Users user = entity.get();
        Long userNo = user.getId();
        String memberNo = user.getMemberNo();
        BigDecimal amount = request.getAmount();

        String pin = request.getTransactionPin();
        boolean isCompleteRequest = action.equals(AccountActions.complete);

        String transactionType = TransactionTypes.EXTERNAL_FUNDS_TRANSFER;

        //Generate request object
        ApiAccountRequest apiAccountRequest = new ApiAccountRequest(
                memberNo,
                sourceAccountNo,
                amount,
                FundsTransferTypes.EXTERNAL_TRANSFERS.toString(),
                request.getBankCode(),
                request.getDestinationAccount(),
                request.getReferenceNo(),
                null
        );

        //Handle request
        map = handleRequest(apiAccountRequest, transactionType, action, pin, userNo);

        //Generate notifications
        if (isCompleteRequest) {
            String message = "";
            String status = (String) map.get("status");
            if ("00".equals(status)) {
                MemberProfileResults.DefaultAccount defaultAccount = fetchDefaultAccount(memberNo);
                BigDecimal availableBalance = defaultAccount.getAvailableBalance();
                message = new StringBuilder().append("You've transfered KES ").append(amount)
                        .append(" to ").append(request.getDestinationAccount())
                        .append(" successfully. Your default account new balance is KES ").append(availableBalance)
                        .toString()
                ;
            } else {
                message = new StringBuilder().append("An attempt to transfer KES ").append(amount)
                        .append(" to ").append(request.getDestinationAccount())
                        .append(" has failed. Contact customer care for further details. ")
                        .toString()
                ;
            }

            Notifications notification = new Notifications();
            notification
                    .setClientNo(userNo)
                    .setMessage(message);

            //Save a record
            notificationsRepository.save(notification);
        }

        return map;
    }


    /**
     * Repay loan using a FOSA speciaRequests
     *
     * @param sourceAccountNo
     * @param action
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    @Override
    public Map<String, Object> repayLoan(String sourceAccountNo,
                                         AccountActions action,
                                         AccTransferRequest request) throws Exception {

        String email = SecurityUtils.currentUser();
        Map<String, Object> map = new HashMap<>();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "Unknown user account.");
            return map;
        }

        Users user = entity.get();
        Long userNo = user.getId();
        String memberNo = user.getMemberNo();
        BigDecimal amount = request.getAmount();
        String transactionType = TransactionTypes.INTERNAL_TRANSFER_TO_OTHERS;
        String pin = request.getTransactionPin();
        boolean isCompleteRequest = action.equals( AccountActions.complete );

        //Generate request object
        ApiAccountRequest apiAccountRequest = new ApiAccountRequest(
                memberNo,
                sourceAccountNo,
                amount,
                FundsTransferTypes.LOAN_REPAYMENT.toString(),
                "",
                request.getDestinationAccount(),
                request.getReferenceNo(),
                null
        );



        //Handle request
        map =  handleRequest( apiAccountRequest, transactionType, action, pin, userNo );

        //Generate notifications
        if( isCompleteRequest ) {
            String message = "";
            String status = (String) map.get("status");
            if ("00".equals(status)) {
                MemberProfileResults.DefaultAccount defaultAccount = fetchDefaultAccount( memberNo );
                BigDecimal availableBalance = defaultAccount.getAvailableBalance();
                message = new StringBuilder().append("You've repaid KES ").append(amount)
                        .append(" to your loan account - ").append(request.getDestinationAccount())
                        .append(" successfully. Your new balance is KES ").append(availableBalance)
                        .toString()
                ;
            } else {
                message = new StringBuilder().append("An attempt to repay KES ").append(amount)
                        .append(" to your loan account - ").append(request.getDestinationAccount())
                        .append(" has failed. Contact customer care for further details. ")
                        .toString()
                ;
            }

            Notifications notification = new Notifications();
            notification
                    .setClientNo(userNo)
                    .setMessage(message);

            //Save a record
            notificationsRepository.save(notification);

//            //Send SMS
//            boolean send = smsService.sendSMS(new SmsOptions()
//                    .setMessage(message)
//                    .setMobileNo(user.getPhone())
//            );
        }
        return map;
    }

    /**
     * GetBranchRequest transactions handler
     *
     * @param request
     * @param transactionType
     * @param action
     * @param transactionPin
     * @return Map<String, Object>
     * @throws Exception
     */
    public Map<String, Object> handleRequest(ApiAccountRequest request,
                                              String transactionType,
                                              AccountActions action,
                                              String transactionPin, Long userNo) throws Exception{
        String requestAction = action.toString();
        boolean isCompleteRequest = requestAction.equals( AccountActions.complete.toString() );
        String requestUri = requestAction.equals( AccountActions.initiate.toString() ) ?
                initTransferUri :
                requestAction.equals( AccountActions.complete.toString() ) ? completeTransferUri : null;

        //Populate commission object
        CommissionVm commissionVm = commissionService.fetchCommision( transactionType, request.getAmount());
        request.setCommission( commissionVm );

        //Generate json holder
        JsonNode jsonNode = objectMapper.convertValue( request, JsonNode.class );

        //When to append PIN
        if( isCompleteRequest ){
            ((ObjectNode) jsonNode).put("transactionPIN", transactionPin);
        }

        //Parse response
        Map<String, Object> apiResponse =  apiResponse( requestUri, jsonNode );

        if( isCompleteRequest ) {
            String status = (String) apiResponse.get("status");
            String message = (String) apiResponse.get("message");
            String flag = ( "00".equals( status ) ? Transactions.SUCCESS : Transactions.FAILED );

            Long transactionTypeNo = null ;
            Optional<TransactionTypes> olTransactionType = transactionTypesRepository.findByCode( transactionType );
            if( olTransactionType.isPresent() ){
                transactionTypeNo = olTransactionType.get().getId();
            }

            CommissionVm.Fee fee = commissionVm.getFee();
            Transactions transaction = new Transactions();
            transaction
                    .setUserNo( userNo )
                    .setFlag( flag )
                    .setReferenceNo( request.getReferenceNo() )
                    .setTransactionTypeNo( transactionTypeNo )
                    .setBinaryFee( fee.getThirdParty() )
                    .setSaccoFee( fee.getInstitution() )
                    .setAmount( request.getAmount() );

            //Save transaction
            transactionsRepository.save( transaction );

            //When this transaction failed
            if( "01".equals( status ) ){
                //Log failed transaction
                int httpCode = (int)apiResponse.get("httpCode");

                //Retrieve the component PK
                Long tnxComponentNo = null;
                Optional<TransactionComponents> oComponent = tnxComponentsRepository.findByCode(TransactionComponents.LANSTER_API );
                if( oComponent.isPresent() ) tnxComponentNo = oComponent.get().getId();

                FailedTransactionsLog log = new FailedTransactionsLog();
                log
                        .setId( transaction.getId() )
                        .setHttpCode( httpCode )
                        .setMessage( message )
                        .setTransactionComponentNo( tnxComponentNo ); //Lanster

                failedTnxLogRepository.save( log );

                //TODO: Attempt transaction reversal
            }

            //Report event
            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            HttpServletRequest httpServletRequest = attr.getRequest();
            TransactionsEvent event = new TransactionsEvent()
                    .setStatus( status )
                    .setTransactionType( transactionType )
                    .setAmount( request.getAmount() )
                    .setTransactionRef( request.getReferenceNo() )
                    .setCommission( new TransactionsEvent.Commission()
                            .setBinary( fee.getThirdParty() )
                            .setSacco( fee.getInstitution() )
                    );
            reportService.transactionEvent( event, httpServletRequest.getHeader("X-Channel") );
        }

        apiResponse.put("commission", commissionVm);
        return apiResponse;
    }

    /**
     * Retrieve member default account
     *
     * @param memberNo
     * @return String
     * @throws Exception
     */
    private MemberProfileResults.DefaultAccount fetchDefaultAccount(String memberNo) throws Exception{
        MemberProfileResults.DefaultAccount defaultAccount = new MemberProfileResults.DefaultAccount();
        MemberProfileRequest memberProfileRequest = new MemberProfileRequest();
        memberProfileRequest.setMemberNo( memberNo );

        String uri = env.getProperty("lanster.api.get-member-profile");
        JsonNode node = objectMapper.convertValue(memberProfileRequest, JsonNode.class);
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest( uri, node);

        if( responseEntity.getStatusCode().is2xxSuccessful() ){
            MemberProfileResults memberProfile =  objectMapper.readValue(responseEntity.getBody(), MemberProfileResults.class);
            defaultAccount = memberProfile.getDefaultAccount();
        }

        return defaultAccount;
    }



}
