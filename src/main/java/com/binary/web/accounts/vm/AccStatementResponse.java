package com.binary.web.accounts.vm;

import java.util.ArrayList;
import java.util.List;

public class AccStatementResponse {
    private String status;
    private String message;
    private String memberNo;
    private String accountNo;
    private List<Statement> entries = new ArrayList<>();

    public AccStatementResponse(){}

    public AccStatementResponse(String status, String message){
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public AccStatementResponse setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public AccStatementResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public AccStatementResponse setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public AccStatementResponse setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }


    public List<Statement> getEntries() {   return entries; }
    public AccStatementResponse setEntries(List<Statement> entries) {
        this.entries = entries;
        return this;
    }
}
