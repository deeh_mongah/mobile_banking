package com.binary.web.accounts.vm;

import com.binary.web.fundstransfer.vm.CommissionVm;
import com.binary.web.fundstransfer.vm.InitiatePaymentRequest;

import java.math.BigDecimal;


public class ApiAccountRequest extends InitiatePaymentRequest {

    public ApiAccountRequest(){}

    public ApiAccountRequest(String memberNo, String sourceAccount, BigDecimal amount, String paymentOption,
                             String organisationCode, String destinationAccount, String referenceNo, CommissionVm commission ) {
        this.memberNo = memberNo;
        this.sourceAccount = sourceAccount;
        this.amount = amount;
        this.paymentOption = paymentOption;
        this.organisationCode = organisationCode;
        this.destinationAccount = destinationAccount;
        this.referenceNo = referenceNo;
        this.commission = commission;
    }

}
