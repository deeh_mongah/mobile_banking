package com.binary.web.accounts.vm;

import java.util.ArrayList;
import java.util.List;

public class GetAccountsResponse {

    private String status;
    private String message;
    private List<Accounts> accounts = new ArrayList<>();

    public GetAccountsResponse(){}
    public GetAccountsResponse(String status, String message){
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public GetAccountsResponse setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public GetAccountsResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Accounts> getAccounts() {
        return accounts;
    }

    public GetAccountsResponse setAccounts(List<Accounts> accounts) {
        this.accounts = accounts;
        return this;
    }
}
