package com.binary.web.accounts.vm;

import java.math.BigDecimal;

public class AccTransferRequest {

    private BigDecimal amount;
    private String referenceNo;
    private String destinationAccount;
    private String transactionPin;
    private IdentifyTypes identifyType;
    private String paymentCategory;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    public String getTransactionPin() {
        return transactionPin;
    }

    public void setTransactionPin(String transactionPin) {
        this.transactionPin = transactionPin;
    }

    public IdentifyTypes getIdentifyType() {
        return identifyType;
    }

    public void setIdentifyType(IdentifyTypes identifyType) {
        this.identifyType = identifyType;
    }

    public String getPaymentCategory() {
        return paymentCategory;
    }

    public void setPaymentCategory(String paymentCategory) {
        this.paymentCategory = paymentCategory;
    }
}
