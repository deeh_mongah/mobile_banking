package com.binary.web.accounts.vm;

import java.math.BigDecimal;

public class DepositRequest {

    private BigDecimal amount;
    private String phoneNumber;
    private String referenceNo;
    private String transactionPin;
    private IdentifyTypes identifyType;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getTransactionPin() {
        return transactionPin;
    }

    public void setTransactionPin(String transactionPin) {
        this.transactionPin = transactionPin;
    }

    public IdentifyTypes getIdentifyType() {
        return identifyType;
    }

    public void setIdentifyType(IdentifyTypes identifyType) {
        this.identifyType = identifyType;
    }
}
