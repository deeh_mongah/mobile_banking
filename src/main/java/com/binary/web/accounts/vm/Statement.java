package com.binary.web.accounts.vm;

import java.math.BigDecimal;

public class Statement {

    private BigDecimal debit;
    private BigDecimal credit;
    private BigDecimal runningBalance;
    private String description;
    private String date;
    private String reference;

    public BigDecimal getDebit() {
        return debit;
    }

    public Statement setDebit(BigDecimal debit) {
        this.debit = debit;
        return this;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public Statement setCredit(BigDecimal credit) {
        this.credit = credit;
        return this;
    }

    public BigDecimal getRunningBalance() {
        return runningBalance;
    }

    public Statement setRunningBalance(BigDecimal runningBalance) {
        this.runningBalance = runningBalance;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Statement setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getDate() {
        return date;
    }

    public Statement setDate(String date) {
        this.date = date;
        return this;
    }

    public String getReference() {
        return reference;
    }

    public Statement setReference(String reference) {
        this.reference = reference;
        return this;
    }
}
