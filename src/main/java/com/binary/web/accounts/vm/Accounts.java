package com.binary.web.accounts.vm;

import java.math.BigDecimal;

public class Accounts {

    private String accountType;
    private String accountName;
    private String accountNo;
    private String acceptsDeposit;
    private String status;
    private String classification;
    private String acceptsWithdrawal;
    private BigDecimal availableBalance;
    private BigDecimal actualBalance;
    private BigDecimal loanAmount;
    private String endDate = "";

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAcceptsDeposit() {
        return acceptsDeposit;
    }

    public void setAcceptsDeposit(String acceptsDeposit) {
        this.acceptsDeposit = acceptsDeposit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getAcceptsWithdrawal() {
        return acceptsWithdrawal;
    }

    public void setAcceptsWithdrawal(String acceptsWithdrawal) {
        this.acceptsWithdrawal = acceptsWithdrawal;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getActualBalance() {
        return actualBalance;
    }

    public void setActualBalance(BigDecimal actualBalance) {
        this.actualBalance = actualBalance;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
