package com.binary.web.accounts.vm;

import com.binary.web.fundstransfer.vm.FundsTransferTypes;

public enum IdentifyTypes {

    accountno {
        @Override
        public String toString() {
            return "accountno";
        }

        public String paymentOption(){
            return FundsTransferTypes.FOSA_TRANSFERS_VIA_ACCOUNT_NO.toString();
        }

        public String accountPrefix(){
            return "ACCT";
        }
    },
    idno {
        @Override
        public String toString() {
            return "idno";
        }

        public String paymentOption(){
            return FundsTransferTypes.FOSA_TRANSFERS_VIA_ID_NO.toString();
        }

        public String accountPrefix(){
            return "IDNO";
        }
    },
    memberno {
        @Override
        public String toString() {
            return "memberno";
        }

        public String paymentOption(){
            return FundsTransferTypes.FOSA_TRANSFERS_VIA_MEMBER_NO.toString();
        }

        public String accountPrefix(){
            return "MNO";
        }
    },
    email {
        @Override
        public String toString() {
            return "email";
        }

        public String paymentOption(){
            return FundsTransferTypes.FOSA_TRANSFERS_VIA_EMAIL.toString();
        }

        public String accountPrefix(){
            return "MAIL";
        }
    },
    
    phoneno {
        @Override
        public String toString() {
            return "phoneno";
        }

        public String paymentOption(){
            return FundsTransferTypes.FOSA_TRANSFERS_VIA_PHONE_NO.toString();
        }

        public String accountPrefix(){
            return "CELL";
        }
    },
    shares {
        @Override
        public String toString() {
            return "shares";
        }

        public String paymentOption(){
            return FundsTransferTypes.DEPOSIT_TO_SHARES.toString();
        }

        public String accountPrefix(){
            return "SHARES";
        }
    },

    loan{
        @Override
        public String toString() {
            return "loan";
        }

        public String paymentOption(){
            return FundsTransferTypes.LOAN_REPAYMENT.toString();
        }

        public String accountPrefix(){
            return "LOAN";
        }
    };

    public abstract String paymentOption();
    public abstract String accountPrefix();

}
