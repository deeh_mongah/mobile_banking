package com.binary.web.loans.controller;

import com.binary.core.rpm.ResponseModel;
import com.binary.web.accounts.vm.AccountActions;
import com.binary.web.accounts.vm.GetAccountsResponse;
import com.binary.web.loans.LoanServiceInterface;
import com.binary.web.loans.vm.lanster.LoanMonthlyRepaymentBreakDown;
import com.binary.web.loans.vm.lanster.MonthlyRepaymentBreakDownDetails;
import com.binary.web.loans.vm.request.*;
import com.binary.web.loans.vm.responses.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/loans")
public class LoansController {

    @Autowired private LoanServiceInterface loanService;

    /**
     * Fetch list of user speciaRequests
     *
     * @return
     */
    @ApiOperation(value = "Fetch list of loan speciaRequests")
    @GetMapping("/accounts")
    public ResponseEntity<?> fetchAccounts() throws Exception{
        GetAccountsResponse response = loanService.fetchLoanAccounts( );
        return ResponseEntity.ok().body( response );
    }

    /**
     * Fetch list of user speciaRequests
     *
     * @return
     */
    @ApiOperation(value = "Fetch list of active loans and guarantors")
    @GetMapping
    public ResponseEntity<ResponseModel> fetchActiveLoans() throws Exception{
        ResponseModel response = loanService.fetchActiveLoans( );
        return ResponseEntity.ok().body( response );
    }

    /**
     * Transfer funds between two FOSA speciaRequests
     *
     * @param accountNo
     * @param action
     * @return ResponseEntity<?>
     * @throws Exception
     */
    @PostMapping("/accounts/{accountNo}/repay")
    public ResponseEntity<?> repayLoan(@PathVariable("accountNo") String accountNo,
                                       @RequestParam("action")AccountActions action,
                                       @RequestBody LoanRepaymentRequest repaymentRequest) throws Exception{
        Map<String, Object> response = loanService.repayLoan(
                accountNo,
                action,
                repaymentRequest
        );
        return ResponseEntity.ok().body( response );
    }

    /**
     * Fetch list loan products
     *
     * @return list of loan products
     */
    @ApiOperation(value = "Fetch loan products", notes = "This method is called to get details of loans on offer")
    @GetMapping("/products")
    public ResponseEntity<ResponseModel> fetchLoanProducts() throws Exception {
        ResponseModel response = loanService.fetchLoanProducts();
        return ResponseEntity.ok().body( response );
    }

    /**
     * Fetch list loan products
     *
     * @return list of loan products
     */
    @ApiOperation(value = "Calculate loan repayments", notes = "This method is called to give a basic calculation of loans on offer.")
    @PostMapping("/calculate")
    public ResponseEntity<ResponseModel> fetchLoanApplicationEstimates(@RequestBody LoanCalculationRequest request) throws Exception {
        ResponseModel response = loanService.calculateLoanApplication(request);
        return ResponseEntity.ok().body(response);
    }

    /**
     * Fetch list loan products
     *
     * @return list of loan products
     */
    @ApiOperation(value = "Loan repayment breakdown", notes = "This method is called to give calculation breakdown of loans on offer.")
    @PostMapping("/breakdown")
    public ResponseEntity<LoanRepaymentBreakDown> loanRepaymentBreakDown(@RequestBody LoanCalculationRequest request) throws Exception {
        Map<String, Object> res = new HashMap<>();
        LoanMonthlyRepaymentBreakDown result = loanService.loanRepaymentBreakDown(request);
        LoanRepaymentBreakDown breakDown = new LoanRepaymentBreakDown();
        breakDown.setMessage(result.getMessage());
        if (result.getStatus() != 0) {
            breakDown.setStatus(String.valueOf(result.getStatus()).equals("1") ? "01" : String.valueOf(result.getStatus()));
            return ResponseEntity.status(402).body(breakDown);
        }

        // Package response
        MonthlyRepaymentBreakDownDetails[] breakDownDetails = new MonthlyRepaymentBreakDownDetails[result.getPaymentMonth().size()];
        breakDown.setStatus("00");
        breakDown.setBreakDownDetails(result.getPaymentMonth().toArray(breakDownDetails));
        return ResponseEntity.ok().body(breakDown);
    }

    /**
     * Loan application history per member
     *
     * @return guarantors details
     */
    @ApiOperation(value = "Fetch loan applications submitted by a member")
    @GetMapping("/applications")
    public ResponseEntity<ResponseModel> fetchLoanApplications() throws Exception {
        ResponseModel result = loanService.fetchLoanApplications();
        return ResponseEntity.ok().body(result);
    }


    /**
     * Submit loan application request
     *
     * @return guarantors details
     */
    @ApiOperation(value = "Submit loan application request", notes = "Submit loan application request")
    @PostMapping("/applications/new")
    public ResponseEntity<ResponseModel> newLoanApplication(@RequestBody LoanApplicationRequest request) throws Exception {
        ResponseModel response = loanService.submitLoanApplication( request );
        return ResponseEntity.ok().body( response );
    }

    /**
     * Submit loan application request
     *
     * @return guarantors details
     */
    @ApiOperation(value = "Fetch guaranteed loan applications")
    @GetMapping("/applications/guaranteed")
    public ResponseEntity<ResponseModel<List<GuaranteedLoansResponse.Application>>> fetchGuaranteedApplications() throws Exception {
        ResponseModel response = loanService.fetchGuaranteedApplications();
        return ResponseEntity.ok().body(response);
    }

    @ApiOperation(value = "Handler to process acceptance to guarantee")
    @PostMapping("/applications/{referenceNo}/accept")
    public ResponseEntity<ResponseModel> acceptToGuarantee( @PathVariable("referenceNo") String referenceNo,
                                                            @RequestBody @Valid AcceptGuaranteeRequest request) throws Exception {
        ResponseModel response = loanService.acceptToGuaranteeHandler( referenceNo, request );
        return ResponseEntity.ok().body(response);
    }

    @ApiOperation(value = "Handler to process decline to guarantee")
    @PostMapping("/applications/{referenceNo}/decline")
    public ResponseEntity<ResponseModel> declineToGuarantee( @PathVariable("referenceNo") String referenceNo) throws Exception {
        ResponseModel response = loanService.declineToGuaranteeHandler( referenceNo );
        return ResponseEntity.ok().body(response);
    }

    @ApiOperation(value = "Cancel loan application")
    @PostMapping("/applications/{referenceNo}/cancel")
    public ResponseEntity<?> cancelLoan(@PathVariable("referenceNo") String referenceNo) throws Exception {
        Map<String, Object> map = loanService.cancelLoanApplication(referenceNo);
        return ResponseEntity.ok().body(map);
    }

    @ApiOperation(value = "Handler to process replace a guarantor")
    @PostMapping("/applications/{referenceNo}/replace-guarantor")
    public ResponseEntity<ResponseModel> replaceGuarantor( @PathVariable("referenceNo") String referenceNo,
                                                           @RequestBody @Valid ReplaceGuarantorRequest request) throws Exception {
        ResponseModel response = loanService.replaceGuarantor( referenceNo, request );
        return ResponseEntity.ok().body(response);
    }


    @ApiOperation(value = "Re-submit a loan application")
    @PostMapping("/applications/{referenceNo}/resubmit")
    public ResponseEntity<ResponseModel> resubmitApplication( @PathVariable("referenceNo") String referenceNo,
                                                           @RequestBody @Valid ResubmitApplicationRequest request) throws Exception {
        ResponseModel response = loanService.resubmitApplication( referenceNo, request );
        return ResponseEntity.ok().body(response);
    }


    /**
     * Submit loan application request
     *
     * @return guarantors details
     */
    @ApiOperation(value = "Fetch loans a member is currently committed as a guarantor")
    @GetMapping("/guaranteed-loans")
    public ResponseEntity<ResponseModel<List<CommittedLoansResponse>>> guaranteedLoans() throws Exception {
        ResponseModel response = loanService.guaranteedLoans();
        return ResponseEntity.ok().body(response);
    }

    /**
     * Fetch guarantor details
     *
     * @return guarantors details
     */
    @ApiOperation(value = "Fetch guarantor details", notes = "Verify guarantor details")
    @PostMapping("/find-guarantors")
    public ResponseEntity<GuarantorDetailResponse> findGuarantor(@RequestBody @Valid FindGuarantorRequest request) throws Exception {
        Map<String, Object> res = new HashMap<>();
        GuarantorDetailResponse result = loanService.fetchGuarantorDetails(request);

        if (!result.getStatus().equals("00")) {
            res.put("status", String.valueOf(result.getStatus()));
            return ResponseEntity.status(402).body(result);
        }
        return ResponseEntity.ok().body(result);
    }


    /**
     * Fetch guarantor requests
     *
     * @return ResponseEntity<ResponseModel<List<GuarantorRequestsVm>>>
     */
    @ApiOperation(value = "Fetch guarantor requests")
    @GetMapping("/applications/guarantor-requests")
    public ResponseEntity<ResponseModel<List<GuarantorRequestsVm>>> fetchGuarantorRequests() throws Exception {
        ResponseModel response = loanService.fetchGuarantorRequests();
        return ResponseEntity.ok().body( response );
    }

    /**
     * Add or remove guarantors from an already submitted application
     *
     * @return application result
     */
    @ApiOperation(value = "Add or remove a guarantor on a submitted loan application", notes = "This method is called to add or remove a guarantor to an already submitted loan application." +
            "Removing a guarantor fails if the guarantor has already accepted to guarantee the loan application.")
    @PostMapping("/add-remove-guarantor")
    public ResponseEntity<GuaranteeLoanApplicationResponse> addRemoveGuarantors(@RequestBody AddRemoveGuarantorsSubmittedApplication request) throws Exception {
        Map<String, Object> res = new HashMap<>();
        GuaranteeLoanApplicationResponse result = loanService.addRemoveGuarantorsFromSubmittedApplication(request);

        if (!result.getStatus().equals("00")) {
            res.put("status", String.valueOf(result.getStatus()));
            return ResponseEntity.status(402).body(result);
        }
        return ResponseEntity.ok().body(result);
    }

}
