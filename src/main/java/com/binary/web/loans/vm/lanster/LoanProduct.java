package com.binary.web.loans.vm.lanster;

import java.math.BigDecimal;

public class LoanProduct {
    private String loanTypeCode;
    private String loanTypeName;
    private String loanCategory;
    private BigDecimal monthlyInterest;
    private String rateCalculation;
    private int noOfTimesShares;
    private int loanMaxDuration;
    private int minimumGuarantorsNo;
    private boolean usesDividends;

    public String getLoanTypeCode() {
        return loanTypeCode;
    }

    public LoanProduct setLoanTypeCode(String loanTypeCode) {
        this.loanTypeCode = loanTypeCode;
        return this;
    }

    public String getLoanTypeName() {
        return loanTypeName;
    }

    public LoanProduct setLoanTypeName(String loanTypeName) {
        this.loanTypeName = loanTypeName;
        return this;
    }

    public String getLoanCategory() {
        return loanCategory;
    }

    public void setLoanCategory(String loanCategory) {
        this.loanCategory = loanCategory;
    }

    public BigDecimal getMonthlyInterest() {
        return monthlyInterest;
    }

    public LoanProduct setMonthlyInterest(BigDecimal monthlyInterest) {
        this.monthlyInterest = monthlyInterest;
        return this;
    }

    public String getRateCalculation() {
        return rateCalculation;
    }

    public LoanProduct setRateCalculation(String rateCalculation) {
        this.rateCalculation = rateCalculation;
        return this;
    }

    public int getNoOfTimesShares() {
        return noOfTimesShares;
    }

    public LoanProduct setNoOfTimesShares(int noOfTimesShares) {
        this.noOfTimesShares = noOfTimesShares;
        return this;
    }

    public int getLoanMaxDuration() {
        return loanMaxDuration;
    }

    public LoanProduct setLoanMaxDuration(int loanMaxDuration) {
        this.loanMaxDuration = loanMaxDuration;
        return this;
    }

    public int getMinimumGuarantorsNo() {
        return minimumGuarantorsNo;
    }

    public LoanProduct setMinimumGuarantorsNo(int minimumGuarantorsNo) {
        this.minimumGuarantorsNo = minimumGuarantorsNo;
        return this;
    }

    public boolean isUsesDividends() {
        return usesDividends;
    }

    public void setUsesDividends(boolean usesDividends) {
        this.usesDividends = usesDividends;
    }
}
