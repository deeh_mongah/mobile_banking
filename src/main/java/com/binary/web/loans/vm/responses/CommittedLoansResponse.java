package com.binary.web.loans.vm.responses;

import java.math.BigDecimal;

public class CommittedLoansResponse {

    private BigDecimal amount;
    private BigDecimal balance;
    private BigDecimal committed;
    private String loanee;
    private String lastPaid;
    private String startDate;
    private String endDate;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getCommitted() {
        return committed;
    }

    public void setCommitted(BigDecimal committed) {
        this.committed = committed;
    }

    public String getLoanee() {
        return loanee;
    }

    public void setLoanee(String loanee) {
        this.loanee = loanee;
    }

    public String getLastPaid() {
        return lastPaid;
    }

    public void setLastPaid(String lastPaid) {
        this.lastPaid = lastPaid;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
