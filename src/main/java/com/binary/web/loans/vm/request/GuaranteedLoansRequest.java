package com.binary.web.loans.vm.request;

import com.binary.web.apiauthentication.vm.Authorization;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Fetch guaranteeing loans", description = "Fetch list of all loans you have been requested to guarantee")
public class GuaranteedLoansRequest {
    @ApiModelProperty(value = "member no", required = true)
    private String memberNo;
    @ApiModelProperty(value = "API Auth", hidden = true)
    private Authorization auth;

    public String getMemberNo() {
        return memberNo;
    }

    public GuaranteedLoansRequest setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public Authorization getAuth() {
        return auth;
    }

    public GuaranteedLoansRequest setAuth(Authorization auth) {
        this.auth = auth;
        return this;
    }
}
