package com.binary.web.loans.vm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Accept Loan guaranteeing request")
public class AcceptDeclineGuaranteeRequest {

    @ApiModelProperty(value = "Member number of member accepting to guarantee loan", required = true)
    private String memberNo;

    @ApiModelProperty(value = "amount member is guaranteeing. Shares to commit to this loan.", required = true)
    private String amount;

    @ApiModelProperty(value = "Unique ID as provided by getGuarantorRequests of loan\n" +
            "whose guarantee is being accepted.", required = true)
    private String referenceNo;

    @ApiModelProperty(value = "accept or decline to guarantee this request. Sample values 1=Accept or 2=Decline", required = true)
    private String status;


    public String getMemberNo() {
        return memberNo;
    }

    public AcceptDeclineGuaranteeRequest setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public String getAmount() {
        return amount;
    }

    public AcceptDeclineGuaranteeRequest setAmount(String amount) {
        this.amount = amount;
        return this;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public AcceptDeclineGuaranteeRequest setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public AcceptDeclineGuaranteeRequest setStatus(String status) {
        this.status = status;
        return this;
    }
}
