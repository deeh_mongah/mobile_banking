package com.binary.web.loans.vm.responses;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class GetLoanApplicationsResponse {

    private String message;
    private int status;
    private List<Application> applications = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Application> getApplications() {
        return applications;
    }

    public void setApplications(List<Application> applications) {
        this.applications = applications;
    }


    public static class Application {

        private Applicant applicant;
        private String loanTypeCode;
        private String loanTypeName;
        private String loanShortName;
        private String applicationGUID;
        private BigDecimal loanAmount;
        private int repaymentPeriod;
        private ApplicationStatus applicationStatus;
        private String appliedOn;
        private List<Guarantor> guarantors = new ArrayList<>();

        public Applicant getApplicant() {
            return applicant;
        }

        public void setApplicant(Applicant applicant) {
            this.applicant = applicant;
        }

        public String getLoanTypeCode() {
            return loanTypeCode;
        }

        public void setLoanTypeCode(String loanTypeCode) {
            this.loanTypeCode = loanTypeCode;
        }

        public String getLoanTypeName() {
            return loanTypeName;
        }

        public void setLoanTypeName(String loanTypeName) {
            this.loanTypeName = loanTypeName;
        }

        public String getLoanShortName() {
            return loanShortName;
        }

        public void setLoanShortName(String loanShortName) {
            this.loanShortName = loanShortName;
        }

        public String getApplicationGUID() {
            return applicationGUID;
        }

        public void setApplicationGUID(String applicationGUID) {
            this.applicationGUID = applicationGUID;
        }

        public BigDecimal getLoanAmount() {
            return loanAmount;
        }

        public void setLoanAmount(BigDecimal loanAmount) {
            this.loanAmount = loanAmount;
        }

        public int getRepaymentPeriod() {
            return repaymentPeriod;
        }

        public void setRepaymentPeriod(int repaymentPeriod) {
            this.repaymentPeriod = repaymentPeriod;
        }

        public ApplicationStatus getApplicationStatus() {
            return applicationStatus;
        }

        public void setApplicationStatus(ApplicationStatus applicationStatus) {
            this.applicationStatus = applicationStatus;
        }

        public String getAppliedOn() {
            return appliedOn;
        }

        public void setAppliedOn(String appliedOn) {
            this.appliedOn = appliedOn;
        }

        public List<Guarantor> getGuarantors() {
            return guarantors;
        }

        public void setGuarantors(List<Guarantor> guarantors) {
            this.guarantors = guarantors;
        }
    }

    public static class ApplicationStatus {

        private String mainStatus;
        private String subStatus;

        public String getMainStatus() {
            return mainStatus;
        }

        public void setMainStatus(String mainStatus) {
            this.mainStatus = mainStatus;
        }

        public String getSubStatus() {
            return subStatus;
        }

        public void setSubStatus(String subStatus) {
            this.subStatus = subStatus;
        }
    }

    public static class Applicant {

        private String fullNames;
        private String memberNo;
        private String payrollNo;

        public String getFullNames() {
            return fullNames;
        }

        public void setFullNames(String fullNames) {
            this.fullNames = fullNames;
        }

        public String getMemberNo() {
            return memberNo;
        }

        public void setMemberNo(String memberNo) {
            this.memberNo = memberNo;
        }

        public String getPayrollNo() {
            return payrollNo;
        }

        public void setPayrollNo(String payrollNo) {
            this.payrollNo = payrollNo;
        }
    }

    public static class Guarantor {

        private String fullNames;
        private String memberNo;
        private String payrollNo;
        private BigDecimal amountCommitted;
        private String guarantorStatus;

        public String getFullNames() {
            return fullNames;
        }

        public void setFullNames(String fullNames) {
            this.fullNames = fullNames;
        }

        public String getMemberNo() {
            return memberNo;
        }

        public void setMemberNo(String memberNo) {
            this.memberNo = memberNo;
        }

        public String getPayrollNo() {
            return payrollNo;
        }

        public void setPayrollNo(String payrollNo) {
            this.payrollNo = payrollNo;
        }

        public BigDecimal getAmountCommitted() {
            return amountCommitted;
        }

        public void setAmountCommitted(BigDecimal amountCommitted) {
            this.amountCommitted = amountCommitted;
        }

        public String getGuarantorStatus() {
            return guarantorStatus;
        }

        public void setGuarantorStatus(String guarantorStatus) {
            this.guarantorStatus = guarantorStatus;
        }
    }

}
