package com.binary.web.loans.vm.lanster;

import java.math.BigDecimal;

public class LoanGuarantors {
    private String fullNames;
    private String memberNo;
    private BigDecimal amountCommitted;
    private String payrollNo;
    private String guarantorStatus;

    public String getFullNames() {
        return fullNames;
    }

    public LoanGuarantors setFullNames(String fullNames) {
        this.fullNames = fullNames;
        return this;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public LoanGuarantors setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public BigDecimal getAmountCommitted() {  return amountCommitted; }
    public LoanGuarantors setAmountCommitted(BigDecimal amountCommitted) {
        this.amountCommitted = amountCommitted;
        return this;
    }

    public String getPayrollNo() {
        return payrollNo;
    }

    public LoanGuarantors setPayrollNo(String payrollNo) {
        this.payrollNo = payrollNo;
        return this;
    }

    public String getGuarantorStatus() {
        return guarantorStatus;
    }

    public LoanGuarantors setGuarantorStatus(String guarantorStatus) {
        this.guarantorStatus = guarantorStatus;
        return this;
    }
}
