package com.binary.web.loans.vm.responses;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class GuarantorRequestsVm {

    private int repaymentPeriod;
    private String applicationGUID;
    private String loanTypeCode;
    private String loanTypeName;
    private String loanShortName;
    private BigDecimal loanAmount;
    private ApplicationStatus applicationStatus;
    private String appliedOn;
    private Applicant applicant;
    private List<Guarantor> guarantors = new ArrayList<>();

    public int getRepaymentPeriod() {
        return repaymentPeriod;
    }

    public void setRepaymentPeriod(int repaymentPeriod) {
        this.repaymentPeriod = repaymentPeriod;
    }

    public String getApplicationGUID() {
        return applicationGUID;
    }

    public void setApplicationGUID(String applicationGUID) {
        this.applicationGUID = applicationGUID;
    }

    public String getLoanTypeCode() {
        return loanTypeCode;
    }

    public void setLoanTypeCode(String loanTypeCode) {
        this.loanTypeCode = loanTypeCode;
    }

    public String getLoanTypeName() {
        return loanTypeName;
    }

    public void setLoanTypeName(String loanTypeName) {
        this.loanTypeName = loanTypeName;
    }

    public String getLoanShortName() {
        return loanShortName;
    }

    public void setLoanShortName(String loanShortName) {
        this.loanShortName = loanShortName;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public ApplicationStatus getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(ApplicationStatus applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public String getAppliedOn() {
        return appliedOn;
    }

    public void setAppliedOn(String appliedOn) {
        this.appliedOn = appliedOn;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public List<Guarantor> getGuarantors() {
        return guarantors;
    }

    public void setGuarantors(List<Guarantor> guarantors) {
        this.guarantors = guarantors;
    }

    public static class ApplicationStatus {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class Applicant {

        private String memberNo;
        private String fullNames;
        private String payrollNo;

        public String getMemberNo() {
            return memberNo;
        }

        public void setMemberNo(String memberNo) {
            this.memberNo = memberNo;
        }

        public String getFullNames() {
            return fullNames;
        }

        public void setFullNames(String fullNames) {
            this.fullNames = fullNames;
        }

        public String getPayrollNo() {
            return payrollNo;
        }

        public void setPayrollNo(String payrollNo) {
            this.payrollNo = payrollNo;
        }
    }

    public static class Guarantor {

        private String fullNames;
        private String memberNo;
        private BigDecimal amountCommitted;
        private String payrollNo;
        private String guarantorStatus;

        public String getFullNames() {
            return fullNames;
        }

        public void setFullNames(String fullNames) {
            this.fullNames = fullNames;
        }

        public String getMemberNo() {
            return memberNo;
        }

        public void setMemberNo(String memberNo) {
            this.memberNo = memberNo;
        }

        public BigDecimal getAmountCommitted() {
            return amountCommitted;
        }

        public void setAmountCommitted(BigDecimal amountCommitted) {
            this.amountCommitted = amountCommitted;
        }

        public String getPayrollNo() {
            return payrollNo;
        }

        public void setPayrollNo(String payrollNo) {
            this.payrollNo = payrollNo;
        }

        public String getGuarantorStatus() {
            return guarantorStatus;
        }

        public void setGuarantorStatus(String guarantorStatus) {
            this.guarantorStatus = guarantorStatus;
        }
    }
}
