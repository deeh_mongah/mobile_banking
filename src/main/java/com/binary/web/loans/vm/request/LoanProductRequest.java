package com.binary.web.loans.vm.request;

import com.binary.web.apiauthentication.vm.Authorization;

public class LoanProductRequest {
    Authorization auth;

    public Authorization getAuth() {
        return auth;
    }

    public LoanProductRequest setAuth(Authorization auth) {
        this.auth = auth;
        return this;
    }
}
