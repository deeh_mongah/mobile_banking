package com.binary.web.loans.vm.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

@ApiModel(value = "Loan Guarantee Requests to member", description = "Fetch a list of applied loans you are to guarantee")
public class GuaranteeRequestsResponse {

    @ApiModelProperty(value = "processing status")
    private String status;

    @ApiModelProperty(value = "processing status description")
    private String message;

    @ApiModelProperty(value = "List of applications")
    private List<GuaranteeRequestsDescriptionResponse> applications = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public GuaranteeRequestsResponse setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public GuaranteeRequestsResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public List<GuaranteeRequestsDescriptionResponse> getApplications() {
        return applications;
    }

    public GuaranteeRequestsResponse setApplications(List<GuaranteeRequestsDescriptionResponse> applications) {
        this.applications = applications;
        return this;
    }
}
