package com.binary.web.loans.vm.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

@ApiModel(value ="Loan applications history by member No", description = "Fetch a list of all loan application made by a member")
public class MemberLoanApplicationHistory {

    @ApiModelProperty(value = "processing status")
    private String status;

    @ApiModelProperty(value = "processing status description")
    private String message;

    @ApiModelProperty(value = "List of applications")
    private List<SingleLoanApplication> applications = new ArrayList<>();

    public MemberLoanApplicationHistory(){}
    public MemberLoanApplicationHistory(String status, String message){
        this.status = status;
        this.message = message;
    }

    public MemberLoanApplicationHistory(String status, String message, List<SingleLoanApplication> applications){
        this.status = status;
        this.message = message;
        this.applications = applications;
    }


    public String getStatus() {
        return status;
    }

    public MemberLoanApplicationHistory setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public MemberLoanApplicationHistory setMessage(String message) {
        this.message = message;
        return this;
    }

    public List<SingleLoanApplication> getApplications() {
        return applications;
    }

    public MemberLoanApplicationHistory setApplications(List<SingleLoanApplication> applications) {
        this.applications = applications;
        return this;
    }
}
