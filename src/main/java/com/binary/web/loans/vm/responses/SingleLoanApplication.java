package com.binary.web.loans.vm.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "Loan Application", description = "Loan Application detail")
public class SingleLoanApplication {

    @ApiModelProperty(value = "Loan product type")
    private String loanProduct;

    @ApiModelProperty(value = "Applied amount")
    private String amount;

    @ApiModelProperty(value = "application date")
    private String appliedOn;

    @ApiModelProperty(value = "loan application unique identifier")
    private String referenceNo;

    @ApiModelProperty(value = "loan repayment period in months")
    private String repaymentPeriod;

    @ApiModelProperty(value = "loan status. Note that, proper status have not been provided yet but i will simulator for now")
    private String status;

    @ApiModelProperty(value = "applicant name")
    private String applicantName;

    @ApiModelProperty(value = "Guarantor details")
    private List<GuarantorDetail> guarantorDetails;

    public String getLoanProduct() {
        return loanProduct;
    }

    public SingleLoanApplication setLoanProduct(String loanProduct) {
        this.loanProduct = loanProduct;
        return this;
    }

    public String getAmount() {
        return amount;
    }

    public SingleLoanApplication setAmount(String amount) {
        this.amount = amount;
        return this;
    }

    public String getAppliedOn() {
        return appliedOn;
    }

    public SingleLoanApplication setAppliedOn(String appliedOn) {
        this.appliedOn = appliedOn;
        return this;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public SingleLoanApplication setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
        return this;
    }

    public String getRepaymentPeriod() {
        return repaymentPeriod;
    }

    public SingleLoanApplication setRepaymentPeriod(String repaymentPeriod) {
        this.repaymentPeriod = repaymentPeriod;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public SingleLoanApplication setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public SingleLoanApplication setApplicantName(String applicantName) {
        this.applicantName = applicantName;
        return this;
    }

    public List<GuarantorDetail> getGuarantorDetails() {
        return guarantorDetails;
    }

    public SingleLoanApplication setGuarantorDetails(List<GuarantorDetail> guarantorDetails) {
        this.guarantorDetails = guarantorDetails;
        return this;
    }
}
