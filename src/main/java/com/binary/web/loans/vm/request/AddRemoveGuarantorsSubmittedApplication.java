package com.binary.web.loans.vm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Add or Remove Guarantors", description = "Used to add or remove guarantors from a submitted loan application")
public class AddRemoveGuarantorsSubmittedApplication {
    @ApiModelProperty(value = "applicant member no", required = true)
    private String applicantMemberNo;

    @ApiModelProperty(value = "Guarantor being added or removed", required = true)
    private String guarantorMemberNo;

    @ApiModelProperty(value = "Loan reference number", required = true)
    private String referenceNo;

    @ApiModelProperty(value = "action i.e. 1=Add and 2=remove")
    private String action;

    public String getApplicantMemberNo() {
        return applicantMemberNo;
    }

    public AddRemoveGuarantorsSubmittedApplication setApplicantMemberNo(String applicantMemberNo) {
        this.applicantMemberNo = applicantMemberNo;
        return this;
    }

    public String getGuarantorMemberNo() {
        return guarantorMemberNo;
    }

    public AddRemoveGuarantorsSubmittedApplication setGuarantorMemberNo(String guarantorMemberNo) {
        this.guarantorMemberNo = guarantorMemberNo;
        return this;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public AddRemoveGuarantorsSubmittedApplication setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
        return this;
    }

    public String getAction() {
        return action;
    }

    public AddRemoveGuarantorsSubmittedApplication setAction(String action) {
        this.action = action;
        return this;
    }
}
