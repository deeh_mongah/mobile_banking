package com.binary.web.loans.vm.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ReplaceGuarantorRequest {

    @NotEmpty
    @NotBlank
    @NotNull
    private String memberNoToRemove;

    @NotEmpty
    @NotBlank
    @NotNull
    private String memberNoToAdd;

    public String getMemberNoToRemove() {
        return memberNoToRemove;
    }

    public void setMemberNoToRemove(String memberNoToRemove) {
        this.memberNoToRemove = memberNoToRemove;
    }

    public String getMemberNoToAdd() {
        return memberNoToAdd;
    }

    public void setMemberNoToAdd(String memberNoToAdd) {
        this.memberNoToAdd = memberNoToAdd;
    }
}
