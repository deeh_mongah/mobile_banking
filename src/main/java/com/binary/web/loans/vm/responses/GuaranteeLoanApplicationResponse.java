package com.binary.web.loans.vm.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Guarantors standard processing response", description = "Decline/accept guaranteeing loan application result and" +
        " add or remove guarantors from an already submitted application")
public class GuaranteeLoanApplicationResponse {
    @ApiModelProperty(value = "processing status")
    private String status;
    @ApiModelProperty(value = "processing status description")
    private String message;

    public String getStatus() {
        return status;
    }

    public GuaranteeLoanApplicationResponse setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public GuaranteeLoanApplicationResponse setMessage(String message) {
        this.message = message;
        return this;
    }
}
