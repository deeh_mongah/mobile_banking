package com.binary.web.loans.vm.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Loan application response", description = "Loan application result")
public class LoanApplicationResponse {

    @ApiModelProperty(value = "processing status")
    private String status;

    @ApiModelProperty(value = "processing status description")
    private String message;

    public String getStatus() {
        return status;
    }

    public LoanApplicationResponse setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public LoanApplicationResponse setMessage(String message) {
        this.message = message;
        return this;
    }
}
