package com.binary.web.loans.vm.lanster;

import java.util.List;

public class LansterLoanApplicationResult {

    private int status;
    private String message;
    private List<LoanApplication> applications;

    public int getStatus() {
        return status;
    }

    public LansterLoanApplicationResult setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public LansterLoanApplicationResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public List<LoanApplication> getApplications() {
        return applications;
    }

    public LansterLoanApplicationResult setApplications(List<LoanApplication> applications) {
        this.applications = applications;
        return this;
    }
}
