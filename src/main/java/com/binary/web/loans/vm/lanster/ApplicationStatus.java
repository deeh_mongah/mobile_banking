package com.binary.web.loans.vm.lanster;

public class ApplicationStatus {
    private int id;
    private String name;
    private String status;

    public int getId() {
        return id;
    }

    public ApplicationStatus setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ApplicationStatus setName(String name) {
        this.name = name;
        return this;
    }

    public String getStatus() {
//        1 - > Awaiting guarantor(s) commitment
//        8 - > Awaiting adding to CBS
//        17 -> Loan Appraised (OR Appraised OR Approved OR In-Service)
//        10 - >Application Rejected at Sacco Appraisal
//        14 -> Application Cancelled by Applicant

        switch (id) {
            case 1:
                status = "Pending Guarantor(s) Approval";
                break;
            case 8:
                status = "Awaiting SACCO";
                break;
            case 10:
                status = "Rejected";
                break;
            case 14:
                status = "Cancelled";
                break;
            case 17:
                status = "Loan Appraised";
                break;
            default:
                status = this.name;
                break;
        }

        return status;
    }
}
