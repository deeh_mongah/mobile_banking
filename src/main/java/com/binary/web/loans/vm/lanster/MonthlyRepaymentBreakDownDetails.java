package com.binary.web.loans.vm.lanster;

public class MonthlyRepaymentBreakDownDetails {
    private String period;
    private int installmentAmt;
    private int interest;
    private int amountToPay;
    private int bulkClearance;

    public String getPeriod() {
        return period;
    }

    public MonthlyRepaymentBreakDownDetails setPeriod(String period) {
        this.period = period;
        return this;
    }

    public int getInterest() {
        return interest;
    }

    public MonthlyRepaymentBreakDownDetails setInterest(int interest) {
        this.interest = interest;
        return this;
    }

    public int getInstallmentAmt() {
        return installmentAmt;
    }

    public MonthlyRepaymentBreakDownDetails setInstallmentAmt(int installmentAmt) {
        this.installmentAmt = installmentAmt;
        return this;
    }

    public int getAmountToPay() {
        return amountToPay;
    }

    public MonthlyRepaymentBreakDownDetails setAmountToPay(int amountToPay) {
        this.amountToPay = amountToPay;
        return this;
    }

    public int getBulkClearance() {
        return bulkClearance;
    }

    public MonthlyRepaymentBreakDownDetails setBulkClearance(int bulkClearance) {
        this.bulkClearance = bulkClearance;
        return this;
    }
}
