package com.binary.web.loans.vm.request;

import com.binary.web.apiauthentication.vm.Authorization;
import io.swagger.annotations.ApiModelProperty;

public class LoanCalculationRequest {
    private Long repaymentPeriod;
    private String loanTypeCode;
    private double loanAmount;

    @ApiModelProperty(hidden = true)
    private Authorization auth;

    public Long getRepaymentPeriod() {
        return repaymentPeriod;
    }

    public LoanCalculationRequest setRepaymentPeriod(Long repaymentPeriod) {
        this.repaymentPeriod = repaymentPeriod;
        return this;
    }

    public String getLoanTypeCode() {
        return loanTypeCode;
    }

    public LoanCalculationRequest setLoanTypeCode(String loanTypeCode) {
        this.loanTypeCode = loanTypeCode;
        return this;
    }

    public double getLoanAmount() {
        return loanAmount;
    }

    public LoanCalculationRequest setLoanAmount(double loanAmount) {
        this.loanAmount = loanAmount;
        return this;
    }

    public Authorization getAuth() {
        return auth;
    }

    public LoanCalculationRequest setAuth(Authorization auth) {
        this.auth = auth;
        return this;
    }
}
