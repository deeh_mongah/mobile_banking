package com.binary.web.loans.vm.request;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class AcceptGuaranteeRequest {

    @ApiModelProperty(value = "amount member is guaranteeing. Shares to commit to this loan.", required = true)
    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
