package com.binary.web.loans.vm.lanster;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class LoanProductResponse {
    private int status;
    private String message;
    private List<Product> loanTypes = new ArrayList<>();

    public int getStatus() {
        return status;
    }

    public LoanProductResponse setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public LoanProductResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Product> getLoanTypes() {
        return loanTypes;
    }

    public LoanProductResponse setLoanTypes(List<Product> loanTypes) {
        this.loanTypes = loanTypes;
        return this;
    }

    public static class Product{
        private String loanTypeCode;
        private String loanCategory;
        private String loanTypeName;
        private BigDecimal monthlyInterest;
        private boolean usesDividends = false;
        private boolean usesFosaScheme = false;
        private String rateCalculation;
        private long noOfTimesShares;
        private long loanMaxDuration;
        private long minimumGuarantorsNo;
        private BigDecimal maximumLoanAmount;

        public String getLoanTypeCode() {
            return loanTypeCode;
        }

        public void setLoanTypeCode(String loanTypeCode) {
            this.loanTypeCode = loanTypeCode;
        }

        public String getLoanCategory() {
            return loanCategory;
        }

        public void setLoanCategory(String loanCategory) {
            this.loanCategory = loanCategory;
        }

        public String getLoanTypeName() {
            return loanTypeName;
        }

        public void setLoanTypeName(String loanTypeName) {
            this.loanTypeName = loanTypeName;
        }

        public BigDecimal getMonthlyInterest() {
            return monthlyInterest;
        }

        public void setMonthlyInterest(BigDecimal monthlyInterest) {
            this.monthlyInterest = monthlyInterest;
        }

        public boolean isUsesDividends() {
            return usesDividends;
        }

        public void setUsesDividends(boolean usesDividends) {
            this.usesDividends = usesDividends;
        }

        public boolean isUsesFosaScheme() {
            return usesFosaScheme;
        }

        public void setUsesFosaScheme(boolean usesFosaScheme) {
            this.usesFosaScheme = usesFosaScheme;
        }

        public String getRateCalculation() {
            return rateCalculation;
        }

        public void setRateCalculation(String rateCalculation) {
            this.rateCalculation = rateCalculation;
        }

        public long getNoOfTimesShares() {
            return noOfTimesShares;
        }

        public void setNoOfTimesShares(long noOfTimesShares) {
            this.noOfTimesShares = noOfTimesShares;
        }

        public long getLoanMaxDuration() {
            return loanMaxDuration;
        }

        public void setLoanMaxDuration(long loanMaxDuration) {
            this.loanMaxDuration = loanMaxDuration;
        }

        public long getMinimumGuarantorsNo() {
            return minimumGuarantorsNo;
        }

        public void setMinimumGuarantorsNo(long minimumGuarantorsNo) {
            this.minimumGuarantorsNo = minimumGuarantorsNo;
        }

        public BigDecimal getMaximumLoanAmount() {
            return maximumLoanAmount;
        }

        public void setMaximumLoanAmount(BigDecimal maximumLoanAmount) {
            this.maximumLoanAmount = maximumLoanAmount;
        }
    }
}
