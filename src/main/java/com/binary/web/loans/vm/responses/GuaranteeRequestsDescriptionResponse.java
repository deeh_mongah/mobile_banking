package com.binary.web.loans.vm.responses;

import com.binary.web.loans.vm.lanster.LoanApplicant;
import com.binary.web.loans.vm.lanster.LoanApplication;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "Guarantee Description", description = "Guarantee Loan Request Description")
public class GuaranteeRequestsDescriptionResponse {

    @ApiModelProperty(value = "Loan applicant member no")
    private String applicantMemberNo;

    @ApiModelProperty(value = "Loan product name")
    private String loanProduct;

    @ApiModelProperty(value = "Loan repayment period")
    private String repaymentPeriod;

    @ApiModelProperty(value = "Loan application date")
    private String applicationDate;

    @ApiModelProperty(value = "Loan applicant's name")
    private String applicantName;

    @ApiModelProperty(value = "Applied amount")
    private String amount;

    @ApiModelProperty(value = "Loan reference no")
    private String referenceNo;

    public GuaranteeRequestsDescriptionResponse(){}
    public GuaranteeRequestsDescriptionResponse(LoanApplication loanApplication){
        LoanApplicant applicant = loanApplication.getApplicant();
        this.applicantMemberNo = applicant.getMemberNo();
        this.amount = String.valueOf(loanApplication.getLoanAmount());
        this.applicantName = applicant.getFullNames();
        this.applicationDate = loanApplication.getAppliedOn();
        this.loanProduct = loanApplication.getLoanTypeName();
        this.repaymentPeriod = String.valueOf(loanApplication.getRepaymentPeriod());
        this.referenceNo = loanApplication.getApplicationGUID();
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public GuaranteeRequestsDescriptionResponse setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
        return this;
    }

    public String getApplicantMemberNo() {
        return applicantMemberNo;
    }

    public GuaranteeRequestsDescriptionResponse setApplicantMemberNo(String applicantMemberNo) {
        this.applicantMemberNo = applicantMemberNo;
        return this;
    }

    public String getLoanProduct() {
        return loanProduct;
    }

    public GuaranteeRequestsDescriptionResponse setLoanProduct(String loanProduct) {
        this.loanProduct = loanProduct;
        return this;
    }

    public String getRepaymentPeriod() {
        return repaymentPeriod;
    }

    public GuaranteeRequestsDescriptionResponse setRepaymentPeriod(String repaymentPeriod) {
        this.repaymentPeriod = repaymentPeriod;
        return this;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public GuaranteeRequestsDescriptionResponse setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
        return this;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public GuaranteeRequestsDescriptionResponse setApplicantName(String applicantName) {
        this.applicantName = applicantName;
        return this;
    }

    public String getAmount() {
        return amount;
    }

    public GuaranteeRequestsDescriptionResponse setAmount(String amount) {
        this.amount = amount;
        return this;
    }
}
