package com.binary.web.loans.vm.lanster;

public class LansterStandardGuaranteeResponse {
    private String message;
    private int status;

    public String getMessage() {
        return message;
    }

    public LansterStandardGuaranteeResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public int getStatus() {
        return status;
    }

    public LansterStandardGuaranteeResponse setStatus(int status) {
        this.status = status;
        return this;
    }
}
