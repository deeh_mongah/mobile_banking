package com.binary.web.loans.vm.lanster;

import java.util.ArrayList;

public class LoanMonthlyRepaymentBreakDown {
    private ArrayList<MonthlyRepaymentBreakDownDetails> paymentMonth;
    private String message;
    private int status;

    public ArrayList<MonthlyRepaymentBreakDownDetails> getPaymentMonth() {
        return paymentMonth;
    }

    public LoanMonthlyRepaymentBreakDown setPaymentMonth(ArrayList<MonthlyRepaymentBreakDownDetails> paymentMonth) {
        this.paymentMonth = paymentMonth;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public LoanMonthlyRepaymentBreakDown setMessage(String message) {
        this.message = message;
        return this;
    }

    public int getStatus() {
        return status;
    }

    public LoanMonthlyRepaymentBreakDown setStatus(int status) {
        this.status = status;
        return this;
    }
}
