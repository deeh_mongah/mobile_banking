package com.binary.web.loans.vm.lanster;

public class LoanApplicant {
    private String memberNo;
    private String fullNames;
    private String payrollNo;

    public String getMemberNo() {
        return memberNo;
    }

    public LoanApplicant setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public String getFullNames() {
        return fullNames;
    }

    public LoanApplicant setFullNames(String fullNames) {
        this.fullNames = fullNames;
        return this;
    }

    public String getPayrollNo() {
        return payrollNo;
    }

    public LoanApplicant setPayrollNo(String payrollNo) {
        this.payrollNo = payrollNo;
        return this;
    }
}
