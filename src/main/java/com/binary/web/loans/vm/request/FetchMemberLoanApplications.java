package com.binary.web.loans.vm.request;

import com.binary.web.apiauthentication.vm.Authorization;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Fetch Member Loan Application", description = "List of all applied loans by a member")
public class FetchMemberLoanApplications {

    @ApiModelProperty(value = "memberNo", required = true)
    private String memberNo;

    @ApiModelProperty(value = "API Authentication object", hidden = true)
    private Authorization auth;

    public String getMemberNo() {
        return memberNo;
    }

    public FetchMemberLoanApplications setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public Authorization getAuth() {
        return auth;
    }

    public FetchMemberLoanApplications setAuth(Authorization auth) {
        this.auth = auth;
        return this;
    }
}
