package com.binary.web.loans.vm.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;

@ApiModel(value ="Loan applications guarantee request by member No", description = "Fetch a list of all loan application to be guaranteed by a member")
public class LoanApplicationGuaranteeRequests {
    @ApiModelProperty(value = "processing status")
    private String status;
    @ApiModelProperty(value = "processing status description")
    private String message;
    @ApiModelProperty(value = "List of applications")
    private ArrayList<SingleLoanApplication> applications;

    public String getStatus() {
        return status;
    }

    public LoanApplicationGuaranteeRequests setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public LoanApplicationGuaranteeRequests setMessage(String message) {
        this.message = message;
        return this;
    }

    public ArrayList<SingleLoanApplication> getApplications() {
        return applications;
    }

    public LoanApplicationGuaranteeRequests setApplications(ArrayList<SingleLoanApplication> applications) {
        this.applications = applications;
        return this;
    }
}
