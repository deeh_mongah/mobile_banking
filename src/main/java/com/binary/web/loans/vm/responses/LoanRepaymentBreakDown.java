package com.binary.web.loans.vm.responses;

import com.binary.web.loans.vm.lanster.MonthlyRepaymentBreakDownDetails;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Loan Repayment Breakdown", description = "Loan repayment break down and monthly repayment")
public class LoanRepaymentBreakDown {
    @ApiModelProperty(value = "transaction status")
    private String status;
    @ApiModelProperty(value = "transaction status description")
    private String message;
    @ApiModelProperty(value = "monthly repayment information")
    private MonthlyRepaymentBreakDownDetails[] breakDownDetails;

    public String getStatus() {
        return status;
    }

    public LoanRepaymentBreakDown setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public LoanRepaymentBreakDown setMessage(String message) {
        this.message = message;
        return this;
    }

    public MonthlyRepaymentBreakDownDetails[] getBreakDownDetails() {
        return breakDownDetails;
    }

    public LoanRepaymentBreakDown setBreakDownDetails(MonthlyRepaymentBreakDownDetails[] breakDownDetails) {
        this.breakDownDetails = breakDownDetails;
        return this;
    }
}
