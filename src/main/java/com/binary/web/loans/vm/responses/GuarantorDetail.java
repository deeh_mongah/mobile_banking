package com.binary.web.loans.vm.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

@ApiModel(value = "Guarantor details", description = "Guarantor details description")
public class GuarantorDetail {

    @ApiModelProperty(value = "guarantor names")
    private String name;

    @ApiModelProperty(value = "guarantor status i.e 1=Confirmed and 0=Pending")
    private String status;

    @ApiModelProperty(value = "guarantor member no")
    private String memberNo;

    private BigDecimal amountCommitted = BigDecimal.ZERO;

    public GuarantorDetail(){}

    public GuarantorDetail(String name, String status, String memberNo){
        this.name = name;
        this.status = status;
        this.memberNo = memberNo;
    }

    public String getName() { return name; }
    public GuarantorDetail setName(String name) {
        this.name = name;
        return this;
    }

    public String getMemberNo() { return memberNo; }
    public GuarantorDetail setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public String getStatus() {  return status; }
    public GuarantorDetail setStatus(String status) {
        this.status = status;
        return this;
    }

    public BigDecimal getAmountCommitted() {  return amountCommitted; }
    public GuarantorDetail setAmountCommitted(BigDecimal amountCommitted) {
        this.amountCommitted = amountCommitted;
        return this;
    }
}
