package com.binary.web.loans.vm.request;

import io.swagger.annotations.ApiModelProperty;

public class DeclineGuaranteeRequest {

    @ApiModelProperty(value = "Loan application GUID", required = true)
    private String referenceNo;

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }
}
