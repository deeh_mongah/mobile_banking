package com.binary.web.loans.vm.request;

import io.swagger.annotations.ApiModelProperty;

public class CancelLoanRequest {

    @ApiModelProperty(value = "Loan reference number", required = true)
    private String referenceNo;

    public CancelLoanRequest(){}

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }
}
