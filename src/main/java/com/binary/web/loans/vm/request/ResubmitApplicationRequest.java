package com.binary.web.loans.vm.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;

public class ResubmitApplicationRequest {

    @JsonIgnore
    private String loanTypeCode;
    private BigDecimal loanAmount;
    private int repaymentPeriod;

    public String getLoanTypeCode() {
        return loanTypeCode;
    }

    public void setLoanTypeCode(String loanTypeCode) {
        this.loanTypeCode = loanTypeCode;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public int getRepaymentPeriod() {
        return repaymentPeriod;
    }

    public void setRepaymentPeriod(int repaymentPeriod) {
        this.repaymentPeriod = repaymentPeriod;
    }
}
