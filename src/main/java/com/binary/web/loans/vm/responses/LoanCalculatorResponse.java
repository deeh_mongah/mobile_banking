package com.binary.web.loans.vm.responses;

import io.swagger.annotations.ApiModel;

import java.math.BigDecimal;

@ApiModel(value = "Loan Calculator response", description = "Basic calculation of loans on offer")
public class LoanCalculatorResponse {

    private Long period;
    private BigDecimal amount;
    private BigDecimal interest;
    private BigDecimal monthlyInstallment;
    private BigDecimal totalPayment;

    public Long getPeriod() {
        return period;
    }

    public void setPeriod(Long period) {
        this.period = period;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    public BigDecimal getMonthlyInstallment() {
        return monthlyInstallment;
    }

    public void setMonthlyInstallment(BigDecimal monthlyInstallment) {
        this.monthlyInstallment = monthlyInstallment;
    }

    public BigDecimal getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(BigDecimal totalPayment) {
        this.totalPayment = totalPayment;
    }
}
