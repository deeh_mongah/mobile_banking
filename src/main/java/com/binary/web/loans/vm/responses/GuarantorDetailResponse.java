package com.binary.web.loans.vm.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Guarantor Details ", description = "Returns guarantors details to be used duting loan application " +
        "process")
public class GuarantorDetailResponse {

    @ApiModelProperty(value = "processing status")
    private String status;

    @ApiModelProperty(value = "processing status description")
    private String message;

    @ApiModelProperty(value = "member's name")
    private String name;

    @ApiModelProperty(value = "member No")
    private String memberNo;

    public String getStatus() {
        return status;
    }

    public GuarantorDetailResponse setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public GuarantorDetailResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getName() {
        return name;
    }

    public GuarantorDetailResponse setName(String name) {
        this.name = name;
        return this;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public GuarantorDetailResponse setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }
}
