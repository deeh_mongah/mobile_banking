package com.binary.web.loans.vm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;

@ApiModel(value = "Submit Loan Application Form", description = "Loan application submission request")
public class LoanApplicationRequest {

    @ApiModelProperty(value = "loan product type", required = true)
    private String loanTypeCode;

    @ApiModelProperty(value = "loan amount", required = true)
    private BigDecimal loanAmount;

    @ApiModelProperty(value = "loan repayment period", required = true)
    private Long loanRepaymentPeriod;

    @ApiModelProperty(value = "list of guarantor's membership no", required = true)
    private List<String> guarantors;

    public String getLoanTypeCode() {
        return loanTypeCode;
    }

    public LoanApplicationRequest setLoanTypeCode(String loanTypeCode) {
        this.loanTypeCode = loanTypeCode;
        return this;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public LoanApplicationRequest setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
        return this;
    }

    public List<String> getGuarantors() {
        return guarantors;
    }

    public LoanApplicationRequest setGuarantors(List<String> guarantors) {
        this.guarantors = guarantors;
        return this;
    }

    public Long getLoanRepaymentPeriod() {
        return loanRepaymentPeriod;
    }

    public LoanApplicationRequest setLoanRepaymentPeriod(Long loanRepaymentPeriod) {
        this.loanRepaymentPeriod = loanRepaymentPeriod;
        return this;
    }
}
