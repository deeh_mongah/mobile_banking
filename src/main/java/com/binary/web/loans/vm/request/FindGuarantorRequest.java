package com.binary.web.loans.vm.request;

public class FindGuarantorRequest {

    private MemberIdentityTypes identifyType;
    private String value;

    public MemberIdentityTypes getIdentifyType() {
        return identifyType;
    }

    public void setIdentifyType(MemberIdentityTypes identifyType) {
        this.identifyType = identifyType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public enum MemberIdentityTypes {

        nationalId {
            @Override
            public String toString() {
                return "nationalID";
            }
        },
        memberNo {
            @Override
            public String toString() {
                return "memberNo";
            }
        },
        email {
            @Override
            public String toString() {
                return "email";
            }
        },
        cellPhone {
            @Override
            public String toString() {
                return "cellPhone";
            }
        };

    }
}
