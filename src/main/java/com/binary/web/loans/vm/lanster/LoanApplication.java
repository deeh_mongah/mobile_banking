package com.binary.web.loans.vm.lanster;

import java.util.List;

public class LoanApplication {
    private String applicationGUID;
    private String loanTypeCode;
    private String loanTypeName;
    private String loanShortName;
    private ApplicationStatus applicationStatus;
    private int loanAmount;
    private int repaymentPeriod;
    private String appliedOn;
    private LoanApplicant applicant;
    private List<LoanGuarantors> guarantors;

    public String getApplicationGUID() {
        return applicationGUID;
    }

    public LoanApplication setApplicationGUID(String applicationGUID) {
        this.applicationGUID = applicationGUID;
        return this;
    }

    public String getLoanTypeCode() {
        return loanTypeCode;
    }

    public LoanApplication setLoanTypeCode(String loanTypeCode) {
        this.loanTypeCode = loanTypeCode;
        return this;
    }

    public String getLoanTypeName() {
        return loanTypeName;
    }

    public LoanApplication setLoanTypeName(String loanTypeName) {
        this.loanTypeName = loanTypeName;
        return this;
    }

    public String getLoanShortName() {
        return loanShortName;
    }

    public LoanApplication setLoanShortName(String loanShortName) {
        this.loanShortName = loanShortName;
        return this;
    }

    public int getLoanAmount() {
        return loanAmount;
    }

    public LoanApplication setLoanAmount(int loanAmount) {
        this.loanAmount = loanAmount;
        return this;
    }

    public ApplicationStatus getApplicationStatus() {
        return applicationStatus;
    }

    public LoanApplication setApplicationStatus(ApplicationStatus applicationStatus) {
        this.applicationStatus = applicationStatus;
        return this;
    }

    public int getRepaymentPeriod() {
        return repaymentPeriod;
    }

    public LoanApplication setRepaymentPeriod(int repaymentPeriod) {
        this.repaymentPeriod = repaymentPeriod;
        return this;
    }

    public String getAppliedOn() {
        return appliedOn;
    }

    public LoanApplication setAppliedOn(String appliedOn) {
        this.appliedOn = appliedOn;
        return this;
    }

    public LoanApplicant getApplicant() {
        return applicant;
    }

    public LoanApplication setApplicant(LoanApplicant applicant) {
        this.applicant = applicant;
        return this;
    }

    public List<LoanGuarantors> getGuarantors() {
        return guarantors;
    }

    public LoanApplication setGuarantors(List<LoanGuarantors> guarantors) {
        this.guarantors = guarantors;
        return this;
    }
}
