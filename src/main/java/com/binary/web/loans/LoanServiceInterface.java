package com.binary.web.loans;

import com.binary.core.rpm.ResponseModel;
import com.binary.web.accounts.vm.AccountActions;
import com.binary.web.accounts.vm.GetAccountsResponse;
import com.binary.web.loans.vm.lanster.LoanMonthlyRepaymentBreakDown;
import com.binary.web.loans.vm.request.*;
import com.binary.web.loans.vm.responses.GuaranteeLoanApplicationResponse;
import com.binary.web.loans.vm.responses.GuarantorDetailResponse;

import java.util.Map;

public interface LoanServiceInterface {

    /**
     * Fetch loan speciaRequests - loan applications already processed
     *
     * @return GetAccountsResponse
     * @throws Exception
     */
    public GetAccountsResponse fetchLoanAccounts() throws Exception;

    /**
     * Fetch member active loans
     *
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel fetchActiveLoans() throws Exception;

    /**
     * Repay loan using one's default account
     *
     * @param loanAccountNo
     * @param action
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    public Map<String, Object> repayLoan(String loanAccountNo,
                                         AccountActions action,
                                         LoanRepaymentRequest request )throws Exception;

    /**
     * Fetch loan products from Fine Extreme CBS
     *
     * @return LoanProductResponse
     * @throws Exception
     */
    public ResponseModel fetchLoanProducts() throws Exception;

    /**
     * Calculate loan application
     *
     * @param request
     * @return LoanCalculationResult
     * @throws Exception
     */
    public ResponseModel calculateLoanApplication(LoanCalculationRequest request) throws Exception;

    /**
     * Generate monthly repayment breakdown
     *
     * @param request
     * @return LoanMonthlyRepaymentBreakDown
     */
    LoanMonthlyRepaymentBreakDown loanRepaymentBreakDown(LoanCalculationRequest request) throws Exception;

    GuarantorDetailResponse fetchGuarantorDetails(FindGuarantorRequest request) throws Exception;

    /**
     * Process a new loan application
     *
     * @param request
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel submitLoanApplication(LoanApplicationRequest request) throws Exception;

    /**
     * Fetch loans history
     *
     * @return MemberLoanApplicationHistory
     */
    public ResponseModel fetchLoanApplications() throws Exception;

    /**
     * Fetch guaranteed requests
     *
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel fetchGuarantorRequests() throws Exception;

    GuaranteeLoanApplicationResponse addRemoveGuarantorsFromSubmittedApplication(AddRemoveGuarantorsSubmittedApplication request) throws Exception;




    /**
     * Fetch member guaranteed loans
     *
     * @return  Object
     * @throws Exception
     */
    public ResponseModel guaranteedLoans() throws Exception;


    /**
     * Fetch guaranteed applications
     *
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel fetchGuaranteedApplications() throws Exception;

    /**
     * Application guarantee acceptance handler
     *
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel acceptToGuaranteeHandler( String referenceNo, AcceptGuaranteeRequest request ) throws Exception;

    /**
     * Application guarantee decline handler
     *
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel declineToGuaranteeHandler(String referenceNo) throws Exception;

    /**
     * Cancel loan application
     *
     * @param loanReferenceNo
     * @return Object
     * @throws Exception
     */
    public Map<String, Object> cancelLoanApplication(String loanReferenceNo ) throws Exception;

    /**
     * Replace a guarantor in a given loan application
     *
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel replaceGuarantor( String referenceNo, ReplaceGuarantorRequest request ) throws Exception;

    /**
     * Re-submit loan application
     *
     * @return ResponseModel
     * @throws Exception
     */
    public ResponseModel resubmitApplication( String referenceNo, ResubmitApplicationRequest request ) throws Exception;



}
