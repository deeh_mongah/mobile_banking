package com.binary.web.loans.service;

import com.binary.core.http.LansterHttpService;
import com.binary.core.rpm.ResponseModel;
import com.binary.core.utils.AppConstants;
import com.binary.web.accounts.services.AccountsService;
import com.binary.web.accounts.vm.AccountActions;
import com.binary.web.accounts.vm.Accounts;
import com.binary.web.accounts.vm.ApiAccountRequest;
import com.binary.web.accounts.vm.GetAccountsResponse;
import com.binary.web.fundstransfer.vm.FundsTransferTypes;
import com.binary.web.loans.LoanServiceInterface;
import com.binary.web.loans.vm.lanster.LansterStandardGuaranteeResponse;
import com.binary.web.loans.vm.lanster.LoanCalculationResult;
import com.binary.web.loans.vm.lanster.LoanMonthlyRepaymentBreakDown;
import com.binary.web.loans.vm.lanster.LoanProductResponse;
import com.binary.web.loans.vm.request.*;
import com.binary.web.loans.vm.responses.*;
import com.binary.web.transactions.entities.LoanApplications;
import com.binary.web.transactions.entities.TransactionTypes;
import com.binary.web.transactions.repositories.LoanApplicationRepository;
import com.binary.web.usermanager.auth.SecurityUtils;
import com.binary.web.usermanager.entities.Notifications;
import com.binary.web.usermanager.entities.Users;
import com.binary.web.usermanager.repository.NotificationsRepository;
import com.binary.web.usermanager.repository.UserRepository;
import com.binary.web.usermanager.vm.MemberProfileResults;
import com.binary.web.usermanager.vm.lanster.MemberProfileRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class LoanService implements LoanServiceInterface {

    @Autowired private Environment env;
    @Autowired private UserRepository userRepository;
    @Autowired private NotificationsRepository notificationsRepository;

    @Autowired private ObjectMapper objectMapper;
    @Autowired private LansterHttpService lansterHttpService;

    //TODO: I dont if this works
    @Autowired private AccountsService accountsService;
    @Autowired private LoanNotificationsService loanNotificationsService;
    @Autowired private LoanApplicationRepository loanApplicationRepository;



    /**
     * Fetch loan speciaRequests - loan applications already processed
     *
     * @return GetAccountsResponse
     * @throws Exception
     */
    @Override
    public GetAccountsResponse fetchLoanAccounts() throws Exception{
        GetAccountsResponse response = new GetAccountsResponse();

        String email = SecurityUtils.currentUser();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            response = new GetAccountsResponse("01", "Unknown user account.");
            return response;
        }

        Users user = entity.get();
        String memberNo = user.getMemberNo();

        JsonNode jsonNode = objectMapper.createObjectNode();
        ((ObjectNode) jsonNode).put("memberNo", memberNo);

        String requestUri = env.getProperty("lanster.api.member-speciaRequests");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        if( responseEntity.getStatusCode().is2xxSuccessful() ){
            response =  objectMapper.readValue(
                    responseEntity.getBody(),
                    GetAccountsResponse.class
            );

            //Filter loan speciaRequests only
            List<Accounts> accounts = response.getAccounts().stream()
                    .filter(  e -> e.getAccountType().equals( "LOAN") )
                    .collect( Collectors.toList());

            response.setAccounts( accounts );
        }
        else{
            jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
            String message = jsonNode.get("message").asText();
            response = new GetAccountsResponse(
                    "01",
                    message
            );
        }

        return response;
    }

    /**
     * Fetch member active loans
     *
     * @return ResponseModel
     * @throws Exception
     */
    @Override
    public ResponseModel fetchActiveLoans() throws Exception{
        ResponseModel responseModel;
        String email = SecurityUtils.currentUser();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown user account."
            );
        }

        Users user = entity.get();
        String memberNo = user.getMemberNo();

        JsonNode jsonNode = objectMapper.createObjectNode();
        ((ObjectNode) jsonNode).put("memberNo", memberNo);

        String requestUri = env.getProperty("lanster.api.loans.servicing-loans");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        jsonNode = objectMapper.readValue(responseEntity.getBody(), JsonNode.class);
        int status = jsonNode.get("status").asInt();
        String message = jsonNode.get("message").asText();

        if( 0 == status ){
            responseModel = new ResponseModel(
                    "00",
                    "Request processed successfully",
                    jsonNode.get("loans")
            );
        }
        else{
            responseModel = new ResponseModel("01", message);
        }

        return responseModel;
    }

    /**
     * Repay loan using one's default account
     *
     * @param loanAccountNo
     * @param action
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    @Override
    public Map<String, Object> repayLoan(String loanAccountNo,
                                         AccountActions action,
                                         LoanRepaymentRequest request )throws Exception{

        String email = SecurityUtils.currentUser();
        Map<String, Object> map = new HashMap<>();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "Unknown user account.");
            return map;
        }

        Users user = entity.get();
        Long userNo = user.getId();
        String memberNo = user.getMemberNo();

        MemberProfileResults.DefaultAccount defaultAccount = fetchDefaultAccount( memberNo );
        String sourceAccountNo = defaultAccount.getAccountNo();


        BigDecimal amount = request.getAmount();
        String transactionType = TransactionTypes.INTERNAL_TRANSFER_TO_OTHERS;
        String pin = request.getTransactionPin();
        boolean isCompleteRequest = action.equals( AccountActions.complete );

        //Generate request object
        ApiAccountRequest apiAccountRequest = new ApiAccountRequest(
                memberNo,
                sourceAccountNo,
                amount,
                FundsTransferTypes.LOAN_REPAYMENT.toString(),
                "",
                loanAccountNo,
                request.getReferenceNo(),
                null
        );

        //Handle request
        map =  accountsService.handleRequest( apiAccountRequest, transactionType, action, pin, userNo );

        //Generate notifications
        if( isCompleteRequest ) {
            String message = "";
            String status = (String) map.get("status");
            if ("00".equals(status)) {
                defaultAccount = fetchDefaultAccount( memberNo );
                BigDecimal availableBalance = defaultAccount.getAvailableBalance();
                message = new StringBuilder().append("You've repaid KES ").append(amount)
                        .append(" to your loan account - ").append( loanAccountNo )
                        .append(" successfully. Your default account new balance is KES ").append(availableBalance)
                        .toString()
                ;
            } else {
                message = new StringBuilder().append("An attempt to repay KES ").append(amount)
                        .append(" to your loan account - ").append( loanAccountNo )
                        .append(" has failed. Contact customer care for further details. ")
                        .toString()
                ;
            }

            Notifications notification = new Notifications();
            notification
                    .setClientNo(userNo)
                    .setMessage(message);

            //Save a record
            notificationsRepository.save(notification);

//            //Send SMS
//            boolean send = smsService.sendSMS(new SmsOptions()
//                    .setMessage(message)
//                    .setMobileNo(user.getPhone())
//            );
        }
        return map;
    }

    /**
     * Fetch loan products from Fine Extreme CBS
     *
     * @return ResponseModel
     * @throws Exception
     */
    @Override
    public ResponseModel fetchLoanProducts() throws Exception {
        ResponseModel response;
        JsonNode jsonNode = objectMapper.createObjectNode();
        String requestUri = env.getProperty("lanster.api.loans.products");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        // Process result
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            LoanProductResponse loanProductResponse =  objectMapper.readValue(
                    responseEntity.getBody(),
                    LoanProductResponse.class
            );

            if (0 == loanProductResponse.getStatus()) {
                response = new ResponseModel(
                        "00",
                        "Request processed successfully",
                        loanProductResponse.getLoanTypes()
                );
            } else {
                response = new ResponseModel(
                        "01",
                        loanProductResponse.getMessage()
                );
            }

        } else {
            jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
            String message = jsonNode.get("message").asText();
            response = new ResponseModel(
                    "01",
                    message
            );
        }

        return response;
    }

    /**
     * Calculate loan application
     *
     * @param request
     * @return LoanCalculationResult
     * @throws Exception
     */
    @Override
    public ResponseModel calculateLoanApplication(LoanCalculationRequest request) throws Exception{
        ResponseModel responseModel;
        JsonNode jsonNode = objectMapper.convertValue( request, JsonNode.class );
        String requestUri = env.getProperty("lanster.api.loans.calculation");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        // Process result
        jsonNode = objectMapper.readTree( responseEntity.getBody() );
        int status = jsonNode.get("status").asInt();
        String message = jsonNode.get("message").asText();
        if ( responseEntity.getStatusCode().is2xxSuccessful() ) {

            if( status == 0 ){
                LoanCalculationResult result =  objectMapper.readValue(
                        responseEntity.getBody(), LoanCalculationResult.class
                );

                LoanCalculatorResponse data = new LoanCalculatorResponse();
                data.setAmount( result.getLoanAmount() );
                data.setInterest( result.getTotalInterest() );
                data.setMonthlyInstallment( result.getMonthlyPayment() );
                data.setPeriod( result.getRepaymentPeriod() );
                data.setTotalPayment( result.getTotalPayment() );
                responseModel =  new ResponseModel("00", "Request processed successfully", data  );
            }
            else{
                responseModel =  new ResponseModel("01", message);
            }
        }
        else {
            responseModel =  new ResponseModel("01", message);
        }

        return responseModel;
    }

    /**
     * Generate monthly repayment breakdown
     *
     * @param request
     * @return LoanMonthlyRepaymentBreakDown
     */
    @Override
    public LoanMonthlyRepaymentBreakDown loanRepaymentBreakDown(LoanCalculationRequest request) throws Exception {
        LoanMonthlyRepaymentBreakDown response;
        JsonNode jsonNode = objectMapper.convertValue( request, JsonNode.class );
        String requestUri = env.getProperty("lanster.api.loan-payment-breakdown");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        // Process result
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            response =  objectMapper.readValue(responseEntity.getBody(), LoanMonthlyRepaymentBreakDown.class);
        }
        else {
            response = new LoanMonthlyRepaymentBreakDown()
                    .setMessage("CBS returned invalid response")
                    .setStatus(1);
        }

        return response;
    }

    /**
     * Fetch guarantor details
     *
     * @param request
     * @return
     */
    @Override
    public GuarantorDetailResponse fetchGuarantorDetails(FindGuarantorRequest request) throws Exception {
        String identifyType = request.getIdentifyType().toString();
        String value = request.getValue();
        Map<String, Object> node = new HashMap<>();
        node.put("nationalID", identifyType.equals( FindGuarantorRequest.MemberIdentityTypes.nationalId.toString() ) ? value : "");
        node.put("memberNo", identifyType.equals( FindGuarantorRequest.MemberIdentityTypes.memberNo.toString() ) ? value : "");
        node.put("cellPhone", identifyType.equals( FindGuarantorRequest.MemberIdentityTypes.cellPhone.toString() ) ? value : "");
        node.put("email", identifyType.equals( FindGuarantorRequest.MemberIdentityTypes.email.toString() ) ? value : "");

        // Place request
        String requestUri = env.getProperty("lanster.api.get-member-profile");
        JsonNode jsonNode = objectMapper.convertValue( node, JsonNode.class );
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(requestUri, jsonNode);

        // Process result
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            MemberProfileResults memberProfileResults = objectMapper.readValue(
                    responseEntity.getBody(),
                    MemberProfileResults.class
            );
            GuarantorDetailResponse result = new GuarantorDetailResponse();
            if (memberProfileResults.getStatus() == 0) {
                result.setMessage("Request processed successfully")
                        .setStatus("00")
                        .setMemberNo(memberProfileResults.getMemberNo())
                        .setName(memberProfileResults.getFullNames());
            } else {
                result.setMessage(memberProfileResults.getMessage())
                        .setStatus("01");
            }
            return result;
        } else {
            GuarantorDetailResponse result = new GuarantorDetailResponse();
            result.setMessage("CBS returned invalid response");
            result.setStatus("01");
            return result;
        }
    }

    /**
     * Process a new loan application
     *
     * @param request
     * @return ResponseModel
     * @throws Exception
     */
    @Override
    public ResponseModel submitLoanApplication(LoanApplicationRequest request) throws Exception{
        ResponseModel responseModel;
        // Fetch loan applicant information
        String email = SecurityUtils.currentUser();
        Optional<Users> oUser = userRepository.findByEmail( email );
        if (!oUser.isPresent()) {
            return new ResponseModel("01", "Unknown user account.");
        }

        Users user = oUser.get();
        Map<String, Object> map = new HashMap<>();
        map.put("loanTypeCode", request.getLoanTypeCode());
        map.put("loanAmount", request.getLoanAmount());
        map.put("repaymentPeriod", request.getLoanRepaymentPeriod());
        map.put("memberNo", user.getMemberNo() );
        map.put("alternateEmail", "");
        map.put("alternateCellPhone", "" );
        map.put("nextOfKinName", "" );
        map.put("nextOfKinCellPhone", "" );
        map.put("nextOfKinRelationship", "" );

        // Prepare guarantors
        ArrayList<Map<String, String>> guarantors = new ArrayList<>();
        request.getGuarantors().forEach(guarantorMemberNo -> {
            Map<String, String> guarantor = new HashMap<>();
            guarantor.put("memberNo", guarantorMemberNo);
            guarantors.add(guarantor);
        });

        //TODO: Test
        if( guarantors.size() > 0 ) {
            map.put("guarantors", guarantors);
        }

        // Place request
        String requestUri = env.getProperty("lanster.api.submit-application");
        JsonNode jsonNode = objectMapper.convertValue( map, JsonNode.class );
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(requestUri, jsonNode);

        // Process result
        jsonNode = objectMapper.readTree( responseEntity.getBody() );
        String message = jsonNode.get("message").asText();
        int status = jsonNode.get("status").asInt();
        if (responseEntity.getStatusCode().is2xxSuccessful()) {

            if( 0 == status ){

                LoanApplications loan= new LoanApplications();
                loan.setRepaymentPeriod(request.getLoanRepaymentPeriod());
                loan.setAmount(request.getLoanAmount());
                loan.setLoanTypeNo(Long.valueOf(request.getLoanTypeCode()));
                loan.setCustomerRef(user.getId().toString());
                loan.setFlag(AppConstants.STATUS_NEWRECORD);
                loanApplicationRepository.save( loan );

                responseModel = new ResponseModel(
                        "00",
                        "Loan application submitted successfully."
                );

                // Notify all guarantors, if any
                loanNotificationsService.notifyGuarantorsOnSubmit( user, request);
            }
            else{

                responseModel = new ResponseModel(
                        "01",
                        message
                );
            }

        } else {
            responseModel = new ResponseModel(
                    "01",
                    message
            );
        }

        return responseModel;
    }

    /**
     * Fetch loans applications
     *
     * @return MemberLoanApplicationHistory
     */
    @Override
    public ResponseModel fetchLoanApplications() throws Exception{
        ResponseModel response;
        String email = SecurityUtils.currentUser();
        Optional<Users> entity = this.userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            return new ResponseModel("01", "Unknown user account.");
        }

        //When the new password matches the current password
        Users user = entity.get();
        String memberNo = user.getMemberNo();

        JsonNode jsonNode = objectMapper.createObjectNode();
        ((ObjectNode) jsonNode).put("memberNo", memberNo);

        String requestUri = env.getProperty("lanster.api.loans.member-applications");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        // Process result
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            GetLoanApplicationsResponse applicationsResponse = objectMapper.readValue(
                    responseEntity.getBody(),
                    GetLoanApplicationsResponse.class
            );

            int intStatus = applicationsResponse.getStatus();
            if ( 0 != intStatus ) {
                String status = ( intStatus == 1 ) ? "01" : String.valueOf( intStatus );
                response = new ResponseModel(
                        "01",
                        applicationsResponse.getMessage()
                );
            }

            //When all went well
            else {

                // Let's use a simple way to respond
                List<Object> data = new ArrayList<>();

                for ( GetLoanApplicationsResponse.Application application : applicationsResponse.getApplications() ){
                    Map<String, Object> map = new HashMap<>();
                    GetLoanApplicationsResponse.ApplicationStatus appStatus = application.getApplicationStatus();
                    map.put("loanProduct", application.getLoanTypeName() );
                    map.put("amount", application.getLoanAmount() );
                    map.put("appliedOn", application.getAppliedOn()  );
                    map.put("referenceNo", application.getApplicationGUID() );
                    map.put("repaymentPeriod", application.getRepaymentPeriod() );
                    map.put("status", appStatus.getMainStatus() );
                    map.put("subStatus", appStatus.getSubStatus() );
                    map.put("applicantName", application.getApplicant().getFullNames() );

                    //List of guarantors
                    List<Object> guarantorList = new ArrayList<>();
                    List<GetLoanApplicationsResponse.Guarantor> guarantors = application.getGuarantors();
                    for( GetLoanApplicationsResponse.Guarantor el: guarantors){
                        Map<String, Object> node = new HashMap<>();
                        node.put("name", el.getFullNames() );
                        node.put("status", el.getGuarantorStatus() );
                        node.put("memberNo", el.getMemberNo());
                        node.put("amountCommitted", el.getAmountCommitted() );

                        guarantorList.add( node );
                    }

                    map.put("guarantorDetails", guarantorList);
                    data.add( map );
                }

                response = new ResponseModel(
                        "00",
                        "Request processed successfully.",
                        data
                );
            }
        }

        //When the response returned an error
        else {
            jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
            String message = jsonNode.get("message").asText();
            response = new ResponseModel(
                    "01",
                    message
            );
        }

        return response;
    }

    /**
     * Fetch guaranteed applications
     *
     * @return ResponseModel
     * @throws Exception
     */
    @Override
    public ResponseModel fetchGuaranteedApplications() throws Exception{
        ResponseModel response;
        String email = SecurityUtils.currentUser();
        Optional<Users> entity = this.userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            return new ResponseModel("01", "Unknown user account.");
        }

        //When the new password matches the current password
        Users user = entity.get();
        String memberNo = user.getMemberNo();

        JsonNode jsonNode = objectMapper.createObjectNode();
        ((ObjectNode) jsonNode).put("memberNo", memberNo);

        String requestUri = env.getProperty("lanster.api.loans.guaranteed-applications");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        if( responseEntity.getStatusCode().is2xxSuccessful() ){
            GuaranteedLoansResponse loansResponse = objectMapper.readValue(
                    responseEntity.getBody(),
                    GuaranteedLoansResponse.class
            );

            int status = loansResponse.getStatus();
            String message = loansResponse.getMessage();
            if( 0 == status ){
                response = new ResponseModel(
                        "00",
                        "Request processed successfully",
                        loansResponse.getApplications()
                );
            }
            else{
                response = new ResponseModel(
                        "01",
                        message
                );
            }
        }
        else{
            jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
            String message = jsonNode.get("message").asText();
            response = new ResponseModel(
                    "01",
                    message
            );
        }

        return response;
    }

    /**
     * Application guarantee acceptance handler
     *
     * @return ResponseModel
     * @throws Exception
     */
    @Override
    public ResponseModel acceptToGuaranteeHandler(String referenceNo, AcceptGuaranteeRequest request) throws Exception{
        ResponseModel response;
        String email = SecurityUtils.currentUser();
        Optional<Users> entity = this.userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            return new ResponseModel("01", "Unknown user account.");
        }

        //When the new password matches the current password
        Users user = entity.get();
        String memberNo = user.getMemberNo();

        JsonNode jsonNode = objectMapper.createObjectNode();
        ((ObjectNode) jsonNode)
                .put("memberNo", memberNo)
                .put("applicationGUID", referenceNo )
                .put("amountToCommit", BigDecimal.ZERO );

        String requestUri = env.getProperty("lanster.api.loans.accept-to-guarantee");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        if( responseEntity.getStatusCode().is2xxSuccessful() ){

            jsonNode = objectMapper.readTree( responseEntity.getBody() );
            int status = jsonNode.get("status").asInt();
            String message = jsonNode.get("message").asText();
            if( 0 == status ){
                response = new ResponseModel(
                        "00",
                        "Request processed successfully"
                );
            }
            else{
                response = new ResponseModel(
                        "01",
                        message
                );
            }
        }
        else{
            jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
            String message = jsonNode.get("message").asText();
            response = new ResponseModel(
                    "01",
                    message
            );
        }

        return response;
    }

    /**
     * Application guarantee decline handler
     *
     * @return ResponseModel
     * @throws Exception
     */
    @Override
    public ResponseModel declineToGuaranteeHandler(String referenceNo) throws Exception{
        ResponseModel response;
        String email = SecurityUtils.currentUser();
        Optional<Users> entity = this.userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            return new ResponseModel("01", "Unknown user account.");
        }

        //When the new password matches the current password
        Users user = entity.get();
        String memberNo = user.getMemberNo();

        JsonNode jsonNode = objectMapper.createObjectNode();
        ((ObjectNode) jsonNode)
                .put("memberNo", memberNo)
                .put("applicationGUID", referenceNo );

        String requestUri = env.getProperty("lanster.api.loans.decline-to-guarantee");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        if( responseEntity.getStatusCode().is2xxSuccessful() ){

            jsonNode = objectMapper.readTree( responseEntity.getBody() );
            int status = jsonNode.get("status").asInt();
            String message = jsonNode.get("message").asText();
            if( 0 == status ){
                response = new ResponseModel(
                        "00",
                        "Request processed successfully"
                );
            }
            else{
                response = new ResponseModel(
                        "01",
                        message
                );
            }
        }
        else{
            jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
            String message = jsonNode.get("message").asText();
            response = new ResponseModel(
                    "01",
                    message
            );
        }

        return response;
    }

    /**
     * Fetch guarantor requests
     *
     * @return GuaranteeRequestsResponse
     * @throws Exception
     */
    public ResponseModel fetchGuarantorRequests() throws Exception{
        ResponseModel response;
        String email = SecurityUtils.currentUser();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown user account."
            );
        }

        Users user = entity.get();
        String memberNo = user.getMemberNo();

        // Place request
        String uri = env.getProperty("lanster.api.loans.guarantor-requests");
        JsonNode jsonNode = objectMapper.createObjectNode()
                .put("memberNo", memberNo);
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest( uri, jsonNode);

        // Process result
        jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
        int status = jsonNode.get("status").asInt();
        String message = jsonNode.get("message").asText();

        if( 0 == status ){
            CollectionType listType = objectMapper.getTypeFactory()
                    .constructCollectionType(List.class, GuarantorRequestsVm.class);

            String jsonData = jsonNode.path("applications").toString();
            List<GuarantorRequestsVm> guarantorRequests = objectMapper.readValue(
                    jsonData,
                    listType
            );

            response = new ResponseModel(
                    "00",
                    "Request processed successfully.",
                    guarantorRequests
            );
        }
        else{
            response = new ResponseModel(
                    "01",
                    message
            );
        }

        return response;
    }

    @Override
    public GuaranteeLoanApplicationResponse addRemoveGuarantorsFromSubmittedApplication(AddRemoveGuarantorsSubmittedApplication request) throws Exception{
        Map<String, Object> guaranteeAcceptance = new HashMap<>();
        guaranteeAcceptance.put("memberNo", request.getGuarantorMemberNo());
        guaranteeAcceptance.put("applicationGUID", request.getReferenceNo());

        String requestUri = "";
        switch (request.getAction()) {
            case "1":
                requestUri = env.getProperty("lanster.api.add-guarantor-to-submitted-application");
                break;
            case "2":
                requestUri = env.getProperty("lanster.api.remove-guarantor-to-submitted-application");
                break;
            default:
                return new GuaranteeLoanApplicationResponse()
                        .setMessage("Invalid guarantee status")
                        .setStatus("01");
        }

        // Place request
        JsonNode node = objectMapper.convertValue(guaranteeAcceptance, JsonNode.class);
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest( requestUri, node);

        // Process result
        GuaranteeLoanApplicationResponse result = new GuaranteeLoanApplicationResponse();
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            LansterStandardGuaranteeResponse guaranteeResponse = objectMapper.readValue(
                    responseEntity.getBody(),
                    LansterStandardGuaranteeResponse.class
            );

            if (guaranteeResponse.getStatus() == 0) {
                result.setMessage("Request processed successfully")
                        .setStatus("00");
            } else {
                result.setMessage(guaranteeResponse.getMessage())
                        .setStatus("01");
            }
        } else {
            result.setMessage("CBS returned invalid response [ Add remove guarantors request ]")
                    .setStatus("01");
        }
        return result;
    }

    /**
     * Cancel loan application
     *
     * @param loanReferenceNo
     * @return Object
     * @throws Exception
     */
    @Override
    public Map<String, Object> cancelLoanApplication( String loanReferenceNo ) throws Exception{
        Map<String, Object> response = new HashMap<>();
        String email = SecurityUtils.currentUser();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            response.put("status", "01");
            response.put("message", "Unknown user account.");
            return response;
        }

        Users user = entity.get();
        String memberNo = user.getMemberNo();

        Map<String, Object> apiRequest = new HashMap<>();
        apiRequest.put("memberNo", memberNo );
        apiRequest.put("applicationGUID", loanReferenceNo );
        JsonNode jsonNode = objectMapper.convertValue( apiRequest, JsonNode.class );

        String cancelLoanUri = env.getProperty("lanster.api.loans.cancel");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                cancelLoanUri,
                jsonNode
        );

        JsonNode responseNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
        String message = responseNode.get("message").asText();
        int status = responseNode.get("status").asInt();
        if( 0 == status ){
            response.put("status", "00");
            response.put("message", "Loan cancelled successfully.");
        }
        else{
            response.put("status", "01");
            response.put("message", message );
        }

        return response;
    }

    /**
     * Replace a guarantor in a given loan application
     *
     * @return ResponseModel
     * @throws Exception
     */
    @Override
    public ResponseModel replaceGuarantor( String referenceNo, ReplaceGuarantorRequest request ) throws Exception{
        ResponseModel response;
        String email = SecurityUtils.currentUser();
        Optional<Users> entity = this.userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            return new ResponseModel("01", "Unknown user account.");
        }

        //When the new password matches the current password
        Users user = entity.get();
        String memberNo = user.getMemberNo();

        JsonNode jsonNode = objectMapper.convertValue( request, JsonNode.class);
        ((ObjectNode) jsonNode)
                .put("applicationGUID", referenceNo );

        String requestUri = env.getProperty("lanster.api.loans.replace-guarantor");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        if( responseEntity.getStatusCode().is2xxSuccessful() ){

            jsonNode = objectMapper.readTree( responseEntity.getBody() );
            int status = jsonNode.get("status").asInt();
            String message = jsonNode.get("message").asText();
            if( 0 == status ){
                response = new ResponseModel(
                        "00",
                        "Request processed successfully"
                );
            }
            else{
                response = new ResponseModel(
                        "01",
                        message
                );
            }
        }
        else{
            jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
            String message = jsonNode.get("message").asText();
            response = new ResponseModel(
                    "01",
                    message
            );
        }

        return response;
    }

    /**
     * Re-submit loan application
     *
     * @return ResponseModel
     * @throws Exception
     */
    @Override
    public ResponseModel resubmitApplication( String referenceNo, ResubmitApplicationRequest request ) throws Exception{
        ResponseModel response;
        String email = SecurityUtils.currentUser();
        Optional<Users> entity = this.userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            return new ResponseModel("01", "Unknown user account.");
        }

        //When the new password matches the current password
        Users user = entity.get();
        String memberNo = user.getMemberNo();

        JsonNode jsonNode = objectMapper.convertValue( request, JsonNode.class);
        ((ObjectNode) jsonNode)
                .put("applicationGUID", referenceNo );


        String requestUri = env.getProperty("lanster.api.loans.resubmit-application");
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        if( responseEntity.getStatusCode().is2xxSuccessful() ){

            jsonNode = objectMapper.readTree( responseEntity.getBody() );
            int status = jsonNode.get("status").asInt();
            String message = jsonNode.get("message").asText();
            if( 0 == status ){
                response = new ResponseModel(
                        "00",
                        "Request processed successfully"
                );
            }
            else{
                response = new ResponseModel(
                        "01",
                        message
                );
            }
        }
        else{
            jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
            String message = jsonNode.get("message").asText();
            response = new ResponseModel(
                    "01",
                    message
            );
        }

        return response;
    }


    /**
     * Fetch member guaranteed loans
     *
     * @return  ResponseModel
     * @throws Exception
     */
    @Override
    public ResponseModel guaranteedLoans() throws Exception{
        ResponseModel response = null;
        String email = SecurityUtils.currentUser();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown user account."
            );
        }

        //When the new password matches the current password
        Users user = entity.get();
        String memberNo = user.getMemberNo();

        // Place request
        String uri = env.getProperty("lanster.api.loans.guaranteed-loans");
        JsonNode node = objectMapper.createObjectNode()
                .put("memberNo", memberNo);
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest( uri, node);

        // Process result
        JsonNode jsonNode = objectMapper.readTree( responseEntity.getBody() );
        int responseStatus = jsonNode.get("status").asInt();
        String message = jsonNode.get("message").asText();

        if ( 0 == responseStatus ) {

            CollectionType listType = objectMapper.getTypeFactory()
                    .constructCollectionType(List.class, CommittedLoansResponse.class);

            String jsonData = jsonNode.path("loans").toString();
            List<CommittedLoansResponse> loansData = objectMapper.readValue(
                    jsonData,
                    listType
            );

            response = new ResponseModel(
                    "00",
                    "Request processed successfully",
                    loansData
            );

        } else {
            response =  new ResponseModel(
                    "01",
                    message
            );
        }

        return response;
    }


    /**
     * Retrieve member default account
     *
     * @param memberNo
     * @return String
     * @throws Exception
     */
    private MemberProfileResults.DefaultAccount fetchDefaultAccount(String memberNo) throws Exception{
        MemberProfileResults.DefaultAccount defaultAccount = new MemberProfileResults.DefaultAccount();
        MemberProfileRequest memberProfileRequest = new MemberProfileRequest();
        memberProfileRequest.setMemberNo( memberNo );

        String uri = env.getProperty("lanster.api.get-member-profile");
        JsonNode node = objectMapper.convertValue(memberProfileRequest, JsonNode.class);
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest( uri, node);

        if( responseEntity.getStatusCode().is2xxSuccessful() ){
            MemberProfileResults memberProfile =  objectMapper.readValue(responseEntity.getBody(), MemberProfileResults.class);
            defaultAccount = memberProfile.getDefaultAccount();
        }

        return defaultAccount;
    }


}
