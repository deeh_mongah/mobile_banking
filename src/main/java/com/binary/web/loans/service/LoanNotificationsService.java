package com.binary.web.loans.service;

import com.binary.core.http.LansterHttpService;
import com.binary.core.sms.SmsOptions;
import com.binary.core.sms.SmsService;
import com.binary.web.loans.vm.lanster.LoanProductResponse;
import com.binary.web.loans.vm.request.LoanApplicationRequest;
import com.binary.web.usermanager.entities.Users;
import com.binary.web.usermanager.vm.MemberProfileResults;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class LoanNotificationsService {

    @Autowired private Environment env;
    @Autowired private ObjectMapper objectMapper;
    @Autowired private LansterHttpService lansterHttpService;
    @Autowired private SmsService smsService;


    @Async
    public void notifyGuarantorsOnSubmit(Users user, LoanApplicationRequest request){

        // When there are no guarantors, end here
        if( request.getGuarantors().size() < 1 ) return;

        // Fetch guarantor data
        List<String> mobileNos = new ArrayList<>();
        for( String memberNo: request.getGuarantors() ) {
            Map<String, Object> map = new HashMap<>();
            map.put("nationalID", "");
            map.put("memberNo", memberNo );
            map.put("cellPhone", "");
            map.put("email", "");

            String uri = env.getProperty("lanster.api.get-member-profile");
            JsonNode node = objectMapper.convertValue(map, JsonNode.class);

            try {
                ResponseEntity<String> responseEntity = lansterHttpService.postRequest(uri, node);
                if (responseEntity.getStatusCode().is2xxSuccessful()) {
                    MemberProfileResults profile = objectMapper.readValue(responseEntity.getBody(), MemberProfileResults.class);
                    mobileNos.add(profile.getPhoneNo());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // When there is no data
        if( mobileNos.size() < 1 ) return;

        //Get the loan name
        String loanName = "";
        try {
            JsonNode jsonNode = objectMapper.createObjectNode();
            String requestUri = env.getProperty("lanster.api.loans.products");
            ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                    requestUri,
                    jsonNode
            );

            // Process result
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                LoanProductResponse loanProductResponse = objectMapper.readValue(
                        responseEntity.getBody(),
                        LoanProductResponse.class
                );

                if( 0 == loanProductResponse.getStatus() ){
                    //Fetch loan with the given code
                    List<LoanProductResponse.Product> products = loanProductResponse.getLoanTypes().stream()
                            .filter( e-> e.getLoanTypeCode().equals( request.getLoanTypeCode() ) )
                            .collect(Collectors.toList() )
                            ;

                    if( products.size() > 0 ){
                        LoanProductResponse.Product product = products.get( 0 );
                        loanName = product.getLoanTypeName();
                    }

                }
            }

        }
        catch ( Exception e){
            e.printStackTrace();
        }


        BigDecimal loanAmount = request.getLoanAmount();
        String fmAmount = String.format("%,.0f", loanAmount );
        long noOfMonths = request.getLoanRepaymentPeriod();

        StringBuilder sb = new StringBuilder();
        String fullNames = user.getFirstName() + " " +user.getMiddleName() + " " +user.getSurname();
        sb
                .append( fullNames ).append(", ")
                .append( user.getPhone() )
                .append( " has applied for a Ksh. ").append( fmAmount ).append(" ").append( loanName )
                .append(" loan for ").append( noOfMonths ).append( " month(s) & ")
                .append("has requested you to be a guarantor. Login to Solution Sacco app to approve or decline.");

        // Send SMS to these guarantors
        smsService.sendSMS( new SmsOptions()
                .setMessage( sb.toString() )
                .setMobileNo(  String.join(", ", mobileNos ))
        );

    }


}
