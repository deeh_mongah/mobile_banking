package com.binary.web.reports.services;

import com.binary.web.reports.vm.TransactionsEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ReportService {

    @Autowired
    protected ObjectMapper objectMapper;

    @Async
    public void transactionEvent(TransactionsEvent event, String channel){
        // Allow this call to fail gracefully
        try {
            event.setChannel(channel);
            // Simply log the event here for observations
            System.err.println( objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString( event ));
        }
        catch( Exception e){
            e.printStackTrace();

        }

    }
}
