package com.binary.web.reports.vm;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class TransactionsEvent {

    private String channel = "";
    private String transactionRef ="";
    private String transactionType ="";
    private BigDecimal amount = BigDecimal.ZERO;
    private Commission commission = new Commission();
    private String status;
    private LocalDateTime dateTime = LocalDateTime.now();

    public String getChannel() { return channel; }
    public TransactionsEvent setChannel(String channel) {
        this.channel = channel;
        return this;
    }

    public String getTransactionRef() { return transactionRef; }
    public TransactionsEvent setTransactionRef(String transactionRef) {
        this.transactionRef = transactionRef;
        return this;
    }

    public String getTransactionType() {  return transactionType; }
    public TransactionsEvent setTransactionType(String transactionType) {
        this.transactionType = transactionType;
        return this;
    }

    public BigDecimal getAmount() {  return amount; }
    public TransactionsEvent setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public Commission getCommission() {  return commission; }
    public TransactionsEvent setCommission(Commission commission) {
        this.commission = commission;
        return this;
    }


    public String getStatus() { return status; }
    public TransactionsEvent setStatus(String status) {
        this.status = status;
        return this;
    }

    public LocalDateTime getDateTime() { return dateTime; }
    public TransactionsEvent setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
        return this;
    }

    public static class Commission{
        private BigDecimal binary = BigDecimal.ZERO;
        private BigDecimal sacco = BigDecimal.ZERO;

        public BigDecimal getBinary() { return binary; }
        public Commission setBinary(BigDecimal binary) {
            this.binary = binary;
            return this;
        }

        public BigDecimal getSacco() {  return sacco; }
        public Commission setSacco(BigDecimal sacco) {
            this.sacco = sacco;
            return this;
        }
    }
}
