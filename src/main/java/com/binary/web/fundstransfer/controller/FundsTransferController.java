package com.binary.web.fundstransfer.controller;

import com.binary.web.fundstransfer.FundsTransferServiceInterface;
import com.binary.web.fundstransfer.vm.ConfirmPaymentRequest;
import com.binary.web.fundstransfer.vm.InitiatePaymentRequest;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

//@RestController
//@RequestMapping("/api/v1/funds-transfer")
public class FundsTransferController {

    @Autowired private FundsTransferServiceInterface transferService;

    /**
     * Fetch list of user speciaRequests
     *
     * @param ftRequest
     * @return
     */
    @ApiOperation(value = "Funds transfer transaction", notes = "This method is called to initiate a funds transfer from " +
            "a member’s default account to a pre-agreed payment option. Apart from the response documented below," +
            " this method also triggers an SMS to be sent from the  CBS to the account holder’s cell phone with a " +
            "one-time-PIN. This transaction PIN is to be used when calling the corresponding confirm method.")
    @ApiResponses(value = {

            @ApiResponse(code = 200, message = "Request processed successfully."),
            @ApiResponse(code = 400, message = "Bad request. The request was unacceptable."),
            @ApiResponse(code = 401, message = "Unauthorized. No valid API key provided."),
            @ApiResponse(code = 402, message = "Request Failed. The parameters were valid but the request failed."),
            @ApiResponse(code = 500, message = "Internal server error.")
    })
    @PostMapping("/initiate")
    public ResponseEntity<?> initiateFundsTransferRequest(@RequestBody InitiatePaymentRequest ftRequest)  throws Exception {
        Map<String, Object> map = transferService.initiateFundsTransfer(ftRequest);
        return ResponseEntity.ok().body( map );
    }

    /**
     * Fetch list of user speciaRequests
     *
     * @param ftRequest
     * @return
     */
    @ApiOperation(value = "Funds transfer transaction", notes = "This method is called to initiate a funds transfer from " +
            "a member’s default account to a pre-agreed payment option. Apart from the response documented below," +
            " this method also triggers an SMS to be sent from the  CBS to the account holder’s cell phone with a " +
            "one-time-PIN. This transaction PIN is to be used when calling the corresponding confirm method.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request processed successfully."),
            @ApiResponse(code = 400, message = "Bad request. The request was unacceptable."),
            @ApiResponse(code = 401, message = "Unauthorized. No valid API key provided."),
            @ApiResponse(code = 402, message = "Request Failed. The parameters were valid but the request failed."),
            @ApiResponse(code = 500, message = "Internal server error.")
    })
    @PostMapping("/confirm")
    public ResponseEntity<?> confirmFundsTransfer(@RequestBody ConfirmPaymentRequest ftRequest) throws Exception {
        Map<String, Object> map = transferService.confirmFundsTransfer(ftRequest);
        return ResponseEntity.ok().body( map );
    }
}
