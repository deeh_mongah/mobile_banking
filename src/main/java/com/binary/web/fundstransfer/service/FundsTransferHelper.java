package com.binary.web.fundstransfer.service;

import com.binary.core.http.LansterHttpService;
import com.binary.web.fundstransfer.vm.ConfirmPaymentResponse;
import com.binary.web.transactions.repositories.FailedTnxLogRepository;
import com.binary.web.transactions.repositories.TransactionComponentsRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class FundsTransferHelper {

    @Autowired protected ObjectMapper objectMapper;
    @Autowired protected LansterHttpService lansterHttpService;

    @Autowired protected FailedTnxLogRepository failedTnxLogRepository;
    @Autowired protected TransactionComponentsRepository tnxComponentsRepository;

    @Value("${lanster.api.init-funds-transfer}")
    protected String initTransferUri;

    @Value("${lanster.api.confirm-funds-transfer}")
    protected String completeTransferUri;

    @Value("${lanster.api.transfers.reverse}")
    protected String reverseTrnsUri;

    /**
     * Handle API response
     *
     * @param requestUri
     * @param jsonNode
     * @return Map<String, Object>
     * @throws Exception
     */
    public Map<String, Object> apiResponse(String requestUri, JsonNode jsonNode) throws Exception{
        Map<String, Object> map = new HashMap<>();
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        //This POJO handles both payloads
        ConfirmPaymentResponse responseNode = objectMapper.readValue( responseEntity.getBody(), ConfirmPaymentResponse.class);
        String message = responseNode.getMessage();
        String strStatus = "01";

        jsonNode = objectMapper.readValue(responseEntity.getBody(), JsonNode.class);
        int status = jsonNode.get("status").asInt();
        if( responseEntity.getStatusCode().is2xxSuccessful() ){
            boolean isSuccess = ( 0 == status );
            strStatus = isSuccess ? "00" : "01";

            //Handle confirm payment request
            if( isSuccess && jsonNode.has("newDefaultAvailableBalance") ){
                map.put("ledgerBalance", responseNode.getNewDefaultActualBalance() );
                map.put("availableBalance", responseNode.getNewDefaultActualBalance() );
            }

            //When to log a transaction failure
            if( !isSuccess ){
                //Use the returned status code
                map.put("httpCode", status );
            }

            // When payment category has been specified
            if( jsonNode.has("paymentCategory")){
                map.put("paymentCategory", jsonNode.get("paymentCategory").asText() );
            }

        }
        else map.put("httpCode", status );

        map.put("status", strStatus);
        map.put("message", message);
        return map;
    }

    public Map<String, Object> reverseTransaction(JsonNode jsonNode) throws Exception{
        return apiResponse( reverseTrnsUri, jsonNode );
    }
}
