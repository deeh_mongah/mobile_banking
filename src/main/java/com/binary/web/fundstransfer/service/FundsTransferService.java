package com.binary.web.fundstransfer.service;

import com.binary.core.http.LansterHttpService;
import com.binary.web.fundstransfer.FundsTransferServiceInterface;
import com.binary.web.fundstransfer.vm.ConfirmPaymentRequest;
import com.binary.web.fundstransfer.vm.ConfirmPaymentResponse;
import com.binary.web.fundstransfer.vm.InitiatePaymentRequest;
import com.binary.web.usermanager.auth.SecurityUtils;
import com.binary.web.usermanager.entities.Users;
import com.binary.web.usermanager.repository.UserRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


@Service
@Transactional
public class FundsTransferService implements FundsTransferServiceInterface {

    @Autowired private Environment env;
    @Autowired private ObjectMapper objectMapper;
    @Autowired private LansterHttpService lansterHttpService;

    @Autowired private UserRepository userRepository;

    /**
     * Initiate a funds transfer request
     *
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    @Override
    public Map<String, Object> initiateFundsTransfer(InitiatePaymentRequest request) throws Exception {
        String email = SecurityUtils.currentUser();
        Map<String, Object> map = new HashMap<>();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "Unknown user account.");
            return map;
        }

        //When the new password matches the current password
        Users user = entity.get();
        String memberNo = user.getMemberNo();
        request.setMemberNo( memberNo );

        String requestUri = env.getProperty("lanster.api.init-funds-transfer");
        JsonNode jsonNode = objectMapper.convertValue( request, JsonNode.class );
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        JsonNode responseNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class);
        String message = responseNode.get("message").asText();
        int status = responseNode.get("status").asInt();
        String strStatus = "01";
        if( responseEntity.getStatusCode().is2xxSuccessful() ){
            boolean isSuccess = ( 0 == status );
            strStatus = isSuccess ? "00" : "01";
        }

        map.put("status", strStatus);
        map.put("message", message);

        return map;
    }

    /**
     * Finalise funds transfer request
     *
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    @Override
    public Map<String, Object> confirmFundsTransfer(ConfirmPaymentRequest request) throws Exception{
        String email = SecurityUtils.currentUser();
        Map<String, Object> map = new HashMap<>();
        Optional<Users> entity = userRepository.findByEmail( email );
        if( !entity.isPresent() ){
            map.put("status", "01");
            map.put("message", "Unknown user account.");
            return map;
        }

        //When the new password matches the current password
        Users user = entity.get();
        String memberNo = user.getMemberNo();
        request.setMemberNo( memberNo );

        String requestUri =  env.getProperty("lanster.api.confirm-funds-transfer");
        JsonNode jsonNode = objectMapper.convertValue( request, JsonNode.class );
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
                requestUri,
                jsonNode
        );

        ConfirmPaymentResponse responseNode = objectMapper.readValue( responseEntity.getBody(), ConfirmPaymentResponse.class);
        String message = responseNode.getMessage();
        int status = responseNode.getStatus();
        String strStatus = "01";
        if( responseEntity.getStatusCode().is2xxSuccessful() ){
            boolean isSuccess = ( 0 == status );
            strStatus = isSuccess ? "00" : "01";

            if( isSuccess ){
                map.put("ledgerBalance", responseNode.getNewDefaultActualBalance() );
                map.put("availableBalance", responseNode.getNewDefaultAvailableBalance() );
            }
        }

        map.put("status", strStatus);
        map.put("message", message);

        return map;
    }
}
