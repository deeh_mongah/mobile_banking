package com.binary.web.fundstransfer;

import com.binary.web.fundstransfer.vm.ConfirmPaymentRequest;
import com.binary.web.fundstransfer.vm.InitiatePaymentRequest;

import java.util.Map;


public interface FundsTransferServiceInterface {

    /**
     * Initiate a funds transfer request
     *
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    Map<String, Object> initiateFundsTransfer(InitiatePaymentRequest request)  throws Exception;

    Map<String, Object> confirmFundsTransfer(ConfirmPaymentRequest request)  throws Exception;

}
