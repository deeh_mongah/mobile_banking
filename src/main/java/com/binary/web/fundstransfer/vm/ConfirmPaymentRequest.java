package com.binary.web.fundstransfer.vm;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class ConfirmPaymentRequest {
    @ApiModelProperty(hidden = true)
    private String memberNo;
    private String sourceAccount;
    private BigDecimal amount;
    private String paymentOption;
    private String organisationCode;
    private String destinationAccount;
    private String referenceNo;
    private String transactionPIN;
    private CommissionVm commission;

    public ConfirmPaymentRequest(){}

    public ConfirmPaymentRequest(String memberNo, String sourceAccount, BigDecimal amount, String paymentOption, String organisationCode, String destinationAccount, String referenceNo, String transactionPIN, CommissionVm commission) {
        this.memberNo = memberNo;
        this.sourceAccount = sourceAccount;
        this.amount = amount;
        this.paymentOption = paymentOption;
        this.organisationCode = organisationCode;
        this.destinationAccount = destinationAccount;
        this.referenceNo = referenceNo;
        this.transactionPIN = transactionPIN;
        this.commission = commission;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public ConfirmPaymentRequest setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public ConfirmPaymentRequest setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public String getPaymentOption() {
        return paymentOption;
    }

    public ConfirmPaymentRequest setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
        return this;
    }

    public String getOrganisationCode() {
        return organisationCode;
    }

    public ConfirmPaymentRequest setOrganisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
        return this;
    }

    public String getSourceAccount() {
        return sourceAccount;
    }

    public ConfirmPaymentRequest setSourceAccount(String sourceAccount) {
        this.sourceAccount = sourceAccount;
        return this;
    }

    public String getDestinationAccount() {
        return destinationAccount;
    }

    public ConfirmPaymentRequest setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
        return this;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public ConfirmPaymentRequest setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
        return this;
    }

    public String getTransactionPIN() {
        return transactionPIN;
    }

    public ConfirmPaymentRequest setTransactionPIN(String transactionPIN) {
        this.transactionPIN = transactionPIN;
        return this;
    }

    public CommissionVm getCommission() {
        return commission;
    }

    public void setCommission(CommissionVm commission) {
        this.commission = commission;
    }
}
