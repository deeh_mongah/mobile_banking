package com.binary.web.fundstransfer.vm;

public class InitiatePaymentResponse {
    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public InitiatePaymentResponse setStatus(String status) {
        this.status = status;
        return this;
    }

    public InitiatePaymentResponse setStatus(int status) {
        this.status = status == 0 ? "00" : "01";
        return this;
    }

    public String getMessage() {
        return message;
    }

    public InitiatePaymentResponse setMessage(String message) {
        this.message = message;
        return this;
    }
}
