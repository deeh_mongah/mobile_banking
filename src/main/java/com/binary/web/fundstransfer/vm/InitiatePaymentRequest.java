package com.binary.web.fundstransfer.vm;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class InitiatePaymentRequest {
    @ApiModelProperty(hidden = true)
    protected String memberNo;

    protected String sourceAccount;
    protected BigDecimal amount;
    protected String paymentOption;
    protected String organisationCode;
    protected String destinationAccount;
    protected String referenceNo;
    protected CommissionVm commission;

    public InitiatePaymentRequest(){}

    public InitiatePaymentRequest(String memberNo, String sourceAccount, BigDecimal amount, String paymentOption, String organisationCode, String destinationAccount, String referenceNo, CommissionVm commission) {
        this.memberNo = memberNo;
        this.sourceAccount = sourceAccount;
        this.amount = amount;
        this.paymentOption = paymentOption;
        this.organisationCode = organisationCode;
        this.destinationAccount = destinationAccount;
        this.referenceNo = referenceNo;
        this.commission = commission;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public InitiatePaymentRequest setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public InitiatePaymentRequest setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public String getPaymentOption() {
        return paymentOption;
    }

    public InitiatePaymentRequest setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
        return this;
    }

    public String getOrganisationCode() {
        return organisationCode;
    }

    public InitiatePaymentRequest setOrganisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
        return this;
    }


    public String getSourceAccount() {
        return sourceAccount;
    }

    public InitiatePaymentRequest setSourceAccount(String sourceAccount) {
        this.sourceAccount = sourceAccount;
        return this;
    }

    public String getDestinationAccount() {
        return destinationAccount;
    }

    public InitiatePaymentRequest setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
        return this;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public InitiatePaymentRequest setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
        return this;
    }

    public CommissionVm getCommission() {
        return commission;
    }

    public void setCommission(CommissionVm commission) {
        this.commission = commission;
    }
}
