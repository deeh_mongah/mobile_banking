package com.binary.web.fundstransfer.vm;

public class ConfirmPaymentResponse {
    private int status;
    private String message;
    private double newDefaultAvailableBalance;
    private double newDefaultActualBalance;

    public int getStatus() {
        return status;
    }

    public ConfirmPaymentResponse setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ConfirmPaymentResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public double getNewDefaultAvailableBalance() {
        return newDefaultAvailableBalance;
    }

    public void setNewDefaultAvailableBalance(double newDefaultAvailableBalance) {
        this.newDefaultAvailableBalance = newDefaultAvailableBalance;
    }

    public double getNewDefaultActualBalance() {
        return newDefaultActualBalance;
    }

    public void setNewDefaultActualBalance(double newDefaultActualBalance) {
        this.newDefaultActualBalance = newDefaultActualBalance;
    }
}
