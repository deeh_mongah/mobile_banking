package com.binary.web.fundstransfer.vm;

public enum FundsTransferTypes {

    WITHDRAW_TO_MPESA{
        @Override
        public String toString(){
            return "SEND-TO-MOBILE-CUSTOMER";
        }
    },
    DEPOSIT_FROM_MPESA{
        @Override
        public String toString(){
            return "SEND-FROM-MOBILE-CUSTOMER";
        }
    },
    DEPOSIT_TO_SHARES{
      @Override
      public String toString(){
          return "INTERNAL-SAVE-TO-SHARES";
      }
    },
    FOSA_TRANSFERS_VIA_ACCOUNT_NO{
        @Override
        public String toString(){
            return "INTERNAL-TRANSFER-TO-ACCOUNTNO";
        }
    },
    FOSA_TRANSFERS_VIA_ID_NO{
        @Override
        public String toString(){
            return "INTERNAL-TRANSFER-VIA-IDNO";
        }
    },

    FOSA_TRANSFERS_VIA_PHONE_NO{
        @Override
        public String toString(){
            return "INTERNAL-TRANSFER-VIA-PHONENO";
        }
    },

    FOSA_TRANSFERS_VIA_MEMBER_NO{
        @Override
        public String toString(){
            return "INTERNAL-TRANSFER-VIA-MEMBERNO";
        }
    },

    FOSA_TRANSFERS_VIA_EMAIL{
        @Override
        public String toString(){
            return "INTERNAL-TRANSFER-VIA-EMAIL";
        }
    },

    LOAN_REPAYMENT{
        @Override
        public String toString(){
            return "INTERNAL-PAY-MY-LOAN";
        }
    },

    EXTERNAL_TRANSFERS {
        @Override
        public String toString(){
            return "SEND-TO-BANK";
        }
    },




    // Bill Payments
    KPLC_PREPAID{
        @Override
        public String toString(){
            return "REPORTING-KPLC-PREPAID";
        }
    },
    KPLC_POSTPAID{
        @Override
        public String toString(){
            return "REPORTING-KPLC-POSTPAID";
        }
    },
    NAIROBI_WATER{
        @Override
        public String toString(){
            return "REPORTING-NAIROBI-WATER";
        }
    },
    AIRTIME_PURCHASE{
        @Override
        public String toString(){
            return "REPORTING-AIRTIME-PURCHASE";
        }
    },
}
