package com.binary.web.fundstransfer.vm;

import java.math.BigDecimal;

public class CommissionVm {

    private String action;
    private String feeMode = FeeModes.fixed.toString();
    private Fee fee = new Fee();

    public CommissionVm(){}

    public CommissionVm(String action, String feeMode, Fee fee) {
        this.action = action;
        this.feeMode = feeMode;
        this.fee = fee;
    }

    public String getAction() { return action;}
    public CommissionVm setAction(String action) {
        this.action = action;
        return this;
    }

    public String getFeeMode() { return feeMode; }
    public CommissionVm setFeeMode(String feeMode) {
        this.feeMode = feeMode;
        return this;
    }

    public Fee getFee() {  return fee; }
    public CommissionVm setFee(Fee fee) {
        this.fee = fee;
        return this;
    }

    public static class Fee{
        //what the branches charged the member
        private BigDecimal institution = BigDecimal.ZERO;

        // what SS keeps
        private BigDecimal agent = BigDecimal.ZERO;

        //What is due to Binary
        private BigDecimal thirdParty = BigDecimal.ZERO;

        public BigDecimal getInstitution() { return institution; }
        public Fee setInstitution(BigDecimal institution) {
            this.institution = institution;
            return this;
        }

        public BigDecimal getAgent() { return agent; }
        public Fee setAgent(BigDecimal agent) {
            this.agent = agent;
            return this;
        }

        public BigDecimal getThirdParty() {  return thirdParty; }
        public Fee setThirdParty(BigDecimal thirdParty) {
            this.thirdParty = thirdParty;
            return this;
        }
    }
}
