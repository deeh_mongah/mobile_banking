package com.binary.web.fundstransfer.vm;

public enum FeeModes {

    fixed{
        @Override
        public String toString(){
            return "fixed";
        }
    },
    percentage{
        @Override
        public String toString(){
            return "percentage";
        }
    }
}
