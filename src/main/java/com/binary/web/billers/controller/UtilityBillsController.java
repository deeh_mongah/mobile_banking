package com.binary.web.billers.controller;

import com.binary.core.rpm.ResponseModel;
import com.binary.web.accounts.vm.AccountActions;
import com.binary.web.billers.UtilityBillsServiceInterface;
import com.binary.web.billers.vm.request.AirtimePurchaseRequest;
import com.binary.web.billers.vm.request.KplcPrepaidRequest;
import com.binary.web.billers.vm.request.NairobiWaterPaymentRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/v1/bills/")
@ApiModel(value = "Utility Bill Payments", description = "Bill Payment processing")
public class UtilityBillsController {

    @Autowired private UtilityBillsServiceInterface entityService;

    /**
     * Purchase Airtime
     *
     * @param request Airtime purchase request. Contains amount, phone and phone telco name
     * @return
     */
    @ApiOperation(value = "Airtime purchase", notes = "This resource handles airtime purchase.")
    @PostMapping("/airtime/request")
    public ResponseEntity<?> airtimePurchase(@RequestParam("action") AccountActions action,
                                             @RequestBody @Valid AirtimePurchaseRequest request) throws Exception {
        Map<String, Object> response = entityService.purchaseAirtime(action, request);
        return ResponseEntity.ok().body(response);
    }

    @ApiOperation(value = "Purchase prepaid electricity tokens")
    @PostMapping("/kplc-prepaid/purchase")
    public ResponseEntity<?> kplcPrepaidPurchase(@RequestParam("action")AccountActions action,
                                         @RequestBody @Valid KplcPrepaidRequest request) throws Exception {
        Map<String, Object> response = entityService.kplcPrepaidHandler(action, request);
        return ResponseEntity.ok().body( response );
    }

    @ApiOperation(value = "Fetch meter number details")
    @GetMapping("/kplc-prepaid/details")
    public ResponseEntity<ResponseModel> kplcPrepaidFetchDetails( @RequestBody @Valid KplcPrepaidRequest request ) throws Exception {
        ResponseModel response = entityService.fetchKplcPrepaidMeterDetails( request );
        return ResponseEntity.ok().body( response );
    }

    /**
     * Fetch customer current water bill
     *
     * @param accountNo
     * @return ResponseEntity
     */
    @ApiOperation(value = "Fetch customer bill")
    @PostMapping("/nairobi-water/{accountNo}/fetch")
    public ResponseEntity<?> fetchWaterBillRequest(@RequestParam("accountNo") String accountNo ) throws Exception{
        ResponseModel response = entityService.fetchNwAccountDetails( accountNo );
        return ResponseEntity.ok( response );
    }

    /**
     * Fetch customer current water bill
     *
     * @param request
     * @return ResponseEntity
     */
    @ApiOperation(value = "Pay NW Bill")
    @PostMapping("/nairobi-water/pay-bill")
    public ResponseEntity<?> fetchWaterBillRequest(@RequestParam("action")AccountActions action,
                                                   @RequestBody @Valid NairobiWaterPaymentRequest request ) throws Exception{
        Map<String, Object> response = entityService.payNairobiWaterBill( action, request );
        return ResponseEntity.ok( response );
    }
}
