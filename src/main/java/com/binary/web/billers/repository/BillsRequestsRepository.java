package com.binary.web.billers.repository;

import com.binary.web.billers.entities.BillsRequests;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BillsRequestsRepository extends CrudRepository<BillsRequests, Long> {

    Optional<BillsRequests> findByReferenceNo(String referenceNo);
}
