package com.binary.web.billers.vm.request;

public enum AirtimeProviders {

    airtel{
        @Override
        public String toString(){
            return "airtel";
        }
    },
    safaricom{
        @Override
        public String toString(){
            return "safaricom";
        }
    },
    telkom{
        @Override
        public String toString(){
            return "telkom";
        }
    }
}
