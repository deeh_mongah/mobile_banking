package com.binary.web.billers.vm.request;

import com.binary.web.billers.vm.ComfirmPinRequest;
import io.swagger.annotations.ApiModel;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel(value = "Purchase Airtime Request")
public class AirtimePurchaseRequest extends ComfirmPinRequest {

    private AirtimeProviders provider;

    @NotNull
    @Min( value =  5)
    private BigDecimal amount;

    @NotNull
    private String phone;

    @NotNull
    private String referenceNo;


    public AirtimeProviders getProvider() {
        return provider;
    }

    public void setProvider(AirtimeProviders provider) {
        this.provider = provider;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }
}
