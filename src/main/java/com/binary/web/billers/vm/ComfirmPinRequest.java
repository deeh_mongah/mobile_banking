package com.binary.web.billers.vm;

public class ComfirmPinRequest {

    private String transactionPin;

    public String getTransactionPin() {
        return transactionPin;
    }

    public void setTransactionPin(String transactionPin) {
        this.transactionPin = transactionPin;
    }
}
