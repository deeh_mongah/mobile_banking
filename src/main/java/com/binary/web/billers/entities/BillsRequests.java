package com.binary.web.billers.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table( name = "bill_requests")
public class BillsRequests implements Serializable {

    public static final String AIRTIME_PURCHASE = "airtime";
    public static final String AIRTIME_VOUCHER = "airtime-voucher";
    public static final String PREPAID_ELECTRICITY = "prepaid-electricity";
    public static final String POSTPAID_ELECTRICITY = "prepaid-electricity";
    public static final String NAIROBI_WATER = "nairobi-water";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column( name = "bill_type", length = 100)
    private String billType;

    @Column( name = "json_request")
    private String jsonRequest;

    @Column( name = "json_response")
    private String jsonResponse;

    @Column( name = "member_no", length = 100)
    private String memberNo;

    @Column( name = "reference_no", length = 100)
    private String referenceNo;

    @Column( name = "flag", length = 100)
    private String flag;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date( System.currentTimeMillis() );

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public BillsRequests(){}

    public Long getId() { return id; }
    public void setId(Long id) {
        this.id = id;
    }

    public String getBillType() { return billType; }
    public BillsRequests setBillType(String billType) {
        this.billType = billType;
        return this;
    }

    public String getJsonRequest() { return jsonRequest; }
    public BillsRequests setJsonRequest(String jsonRequest) {
        this.jsonRequest = jsonRequest;
        return this;
    }

    public String getJsonResponse() { return jsonResponse; }
    public BillsRequests setJsonResponse(String jsonResponse) {
        this.jsonResponse = jsonResponse;
        return this;
    }

    public String getMemberNo() { return memberNo; }
    public BillsRequests setMemberNo(String memberNo) {
        this.memberNo = memberNo;
        return this;
    }

    public String getReferenceNo() { return referenceNo; }
    public BillsRequests setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
        return this;
    }

    public String getFlag() { return flag;}
    public BillsRequests setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public Date getCreatedOn() { return createdOn; }
    public BillsRequests setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public Date getUpdatedOn() {return updatedOn; }
    public BillsRequests setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }
}
