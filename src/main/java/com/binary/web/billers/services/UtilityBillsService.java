package com.binary.web.billers.services;

import com.binary.core.http.MfmHttpService;
import com.binary.core.rpm.ResponseModel;
import com.binary.core.sms.SmsOptions;
import com.binary.core.sms.SmsServiceInterface;
import com.binary.web.accounts.vm.AccountActions;
import com.binary.web.accounts.vm.ApiAccountRequest;
import com.binary.web.billers.UtilityBillsServiceInterface;
import com.binary.web.billers.entities.BillsRequests;
import com.binary.web.billers.repository.BillsRequestsRepository;
import com.binary.web.billers.vm.request.AirtimePurchaseRequest;
import com.binary.web.billers.vm.request.KplcPrepaidRequest;
import com.binary.web.billers.vm.request.NairobiWaterPaymentRequest;
import com.binary.web.fees.service.CommissionService;
import com.binary.web.fundstransfer.service.FundsTransferHelper;
import com.binary.web.fundstransfer.vm.CommissionVm;
import com.binary.web.fundstransfer.vm.FundsTransferTypes;
import com.binary.web.transactions.entities.FailedTransactionsLog;
import com.binary.web.transactions.entities.TransactionComponents;
import com.binary.web.transactions.entities.TransactionTypes;
import com.binary.web.transactions.entities.Transactions;
import com.binary.web.transactions.repositories.TransactionTypesRepository;
import com.binary.web.transactions.repositories.TransactionsRepository;
import com.binary.web.usermanager.auth.SecurityUtils;
import com.binary.web.usermanager.entities.Notifications;
import com.binary.web.usermanager.entities.Users;
import com.binary.web.usermanager.repository.NotificationsRepository;
import com.binary.web.usermanager.repository.UserRepository;
import com.binary.web.usermanager.vm.MemberProfileResults;
import com.binary.web.usermanager.vm.lanster.MemberProfileRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class UtilityBillsService extends FundsTransferHelper implements UtilityBillsServiceInterface {

    @Autowired private UserRepository userRepository;
    @Autowired private BillsRequestsRepository billsRequestsRepository;
    @Autowired private CommissionService commissionService;

    @Autowired private Environment env;
    @Autowired private MfmHttpService mfmHttpService;
    @Autowired private SmsServiceInterface smsService;
    @Autowired protected RestTemplate restTemplate;

    @Autowired private TransactionsRepository transactionsRepository;
    @Autowired private NotificationsRepository notificationsRepository;
    @Autowired private TransactionTypesRepository transactionTypesRepository;

    @Value("${mfm.base-url}")
    private String mfmBaseUrl;

    /**
     * Handle electricity payments
     *
     * @param requestAction
     * @param request
     * @return PrepaidClientResponse
     */
    @Override
    public Map<String, Object> kplcPrepaidHandler(AccountActions requestAction, KplcPrepaidRequest request) throws Exception {
        String email = SecurityUtils.currentUser();
        Map<String, Object> map = new HashMap<>();
        Optional<Users> entity = userRepository.findByEmail(email);
        if (!entity.isPresent()) {
            map.put("status", "01");
            map.put("message", "Unknown user account.");
            return map;
        }

        Users user = entity.get();
        Long userNo = user.getId();
        String memberNo = user.getMemberNo();

        MemberProfileResults.DefaultAccount defaultAccount = fetchDefaultAccount(memberNo);
        String accountNo = defaultAccount.getAccountNo();
        BigDecimal availableBalance = defaultAccount.getAvailableBalance();
        BigDecimal transactionAmount = request.getAmount();

        //Check if balance is sufficient to proceed with this request
        if (availableBalance.compareTo(transactionAmount) == -1) {
            map.put("status", "01");
            map.put("message", "You have insufficient balance to complete this request.");
            return map;
        }

        //Parse URI and Fund Transfer Type
        String billType = BillsRequests.PREPAID_ELECTRICITY;
        String paymentOption = FundsTransferTypes.KPLC_PREPAID.toString();
        String transactionType = TransactionTypes.KPLC_PREPAID;

//        CommissionVm commissionVm = new CommissionVm();
//        commissionVm.setAction( "airtime-purchase" );
        BigDecimal amount = request.getAmount();

        //Generate request object
        ApiAccountRequest apiAccountRequest = new ApiAccountRequest(
                memberNo,
                accountNo,
                amount,
                FundsTransferTypes.KPLC_PREPAID.toString(),
                "",
                "",
                request.getReferenceNo(),
                null
        );

        boolean isCompleteRequest = requestAction.equals(AccountActions.complete);
        String requestUri = requestAction.equals(AccountActions.initiate) ?
                initTransferUri :
                requestAction.equals(AccountActions.complete) ? completeTransferUri : null;

        //Populate commission object
        CommissionVm commissionVm = commissionService.fetchCommision(transactionType, request.getAmount());
        apiAccountRequest.setCommission(commissionVm);

        //Generate json holder
        JsonNode jsonNode = objectMapper.convertValue(apiAccountRequest, JsonNode.class);

        //When to append PIN
        if (isCompleteRequest) {
            String transactionPin = request.getTransactionPin();
            ((ObjectNode) jsonNode).put("transactionPIN", transactionPin);
        }

        Map<String, Object> response = apiResponse(requestUri, jsonNode);

        if (isCompleteRequest) {
            String status = (String) response.get("status");
            String apiMessage = (String) response.get("message");
            String flag = ("00".equals(status) ? Transactions.SUCCESS : Transactions.FAILED);

            //Log transaction
            Long transactionTypeNo = null;
            Optional<TransactionTypes> olTransactionType = transactionTypesRepository.findByCode(transactionType);
            if (olTransactionType.isPresent()) {
                transactionTypeNo = olTransactionType.get().getId();
            }

            CommissionVm.Fee fee = commissionVm.getFee();
            Transactions transaction = new Transactions();
            transaction
                    .setUserNo(userNo)
                    .setFlag(flag)
                    .setReferenceNo(request.getReferenceNo())
                    .setTransactionTypeNo(transactionTypeNo)
                    .setBinaryFee(fee.getThirdParty())
                    .setSaccoFee(fee.getInstitution())
                    .setAmount(request.getAmount());

            //Save transaction
            transactionsRepository.save(transaction);

            //When this transaction failed
            if ("01".equals(status)) {
                //Log failed transaction
                int httpCode = (int) response.get("httpCode");

                //Retrieve the component PK
                Long tnxComponentNo = null;
                Optional<TransactionComponents> oComponent = tnxComponentsRepository.findByCode(TransactionComponents.LANSTER_API);
                if (oComponent.isPresent()) {
                    tnxComponentNo = oComponent.get().getId();
                    FailedTransactionsLog log = new FailedTransactionsLog();
                    log
                            .setId(transaction.getId())
                            .setHttpCode(httpCode)
                            .setMessage(apiMessage)
                            .setTransactionComponentNo(tnxComponentNo); //Lanster

                    failedTnxLogRepository.save(log);
                }

                //Notify user
                String smsMessage = "An attempt to purchase KPLC Prepaid Tokens of KES " + amount + " has failed. Contact " +
                        "customer care for further details";
                Notifications notification = new Notifications();
                notification
                        .setClientNo(userNo)
                        .setMessage(smsMessage);

                //Save a record
                notificationsRepository.save(notification);

                //Send SMS
                boolean send = smsService.sendSMS(new SmsOptions()
                        .setMessage(smsMessage)
                        .setMobileNo(user.getPhone())
                );

            }

            //Process bill payment
            else {
                //Process payment
                String jsonRequest = jsonNode.asText();
                BillsRequests billRequest = new BillsRequests();
                billRequest
                        .setReferenceNo(request.getReferenceNo())
                        .setBillType(billType)
                        .setJsonRequest(jsonRequest)
                        .setMemberNo(memberNo)
                        .setFlag(status)
                ;
                billsRequestsRepository.save(billRequest);

                String uri = env.getProperty("mfm.uris.kplc-prepaid.purchase");
                Map<String, Object> mfmRequest = new HashMap<>();
                mfmRequest.put("amount", request.getAmount());
                mfmRequest.put("customer", request.getCustomerName());
                mfmRequest.put("phone", request.getCustomerPhoneNo());
                mfmRequest.put("meter_no", request.getMeterNo());
                ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
                        uri,
                        mfmRequest
                );

                //Parse Response
                jsonNode = objectMapper.readTree(responseEntity.getBody());
                String mfmMessage = jsonNode.get("message").asText();
                map.put("status", jsonNode.get("status").asText());
                map.put("message", mfmMessage);
                map.put("data", jsonNode.get("data"));

                //If this transaction fails, update the transaction
                boolean isMfmSuccess = !responseEntity.getStatusCode().is2xxSuccessful() || "01".equals( status );
                if ( isMfmSuccess ) {
                    transaction.setFlag(Transactions.FAILED);
                    transactionsRepository.save(transaction);

                    //Log failed transaction
                    int httpCode = responseEntity.getStatusCodeValue();

                    //Retrieve the component PK
                    Long tnxComponentNo = null;
                    Optional<TransactionComponents> oComponent = tnxComponentsRepository.findByCode(TransactionComponents.MFM);
                    if (oComponent.isPresent()) {
                        tnxComponentNo = oComponent.get().getId();

                        FailedTransactionsLog log = new FailedTransactionsLog();
                        log
                                .setId(transaction.getId())
                                .setHttpCode(httpCode)
                                .setMessage(mfmMessage)
                                .setTransactionComponentNo(tnxComponentNo); //mfm

                        failedTnxLogRepository.save(log);

                        //TODO: Attempt transaction reversal
                        reverseTransaction(apiAccountRequest, log);
                    }

                    //Notify user
                    String smsMessage = "An attempt to purchase KPLC Prepaid Tokens of KES " + amount + " has failed. Contact " +
                            "customer care for further details";
                    Notifications notification = new Notifications();
                    notification
                            .setClientNo(userNo)
                            .setMessage(smsMessage);

                    //Save a record
                    notificationsRepository.save(notification);

                    //Send SMS
                    boolean send = smsService.sendSMS(new SmsOptions()
                            .setMessage(smsMessage)
                            .setMobileNo(user.getPhone())
                    );

                }

                return map;
            }
        }

        return response;
    }

    /**
     * Fetch KPLC Prepaid meter details
     *
     * @param request
     * @return ResponseModel
     */
    @Override
    public ResponseModel fetchKplcPrepaidMeterDetails(KplcPrepaidRequest request) throws Exception {
        String uri = env.getProperty("mfm.uris.kplc-prepaid.fetch-details");

        Map<String, Object> apiRequest = new HashMap<>();
        apiRequest.put("amount", request.getAmount());
        apiRequest.put("customer", request.getCustomerName());
        apiRequest.put("phone", request.getCustomerPhoneNo());
        apiRequest.put("meter_no", request.getMeterNo());

        ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
                uri,
                apiRequest
        );

        JsonNode jsonNode = objectMapper.readTree(responseEntity.getBody());
        ResponseModel response = new ResponseModel(
                jsonNode.get("status").asText(),
                jsonNode.get("message").asText(),
                jsonNode.get("data")
        );
        return response;
    }

    /**
     * Purchase airtime handler
     *
     * @param requestAction
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    @Override
    public Map<String, Object> purchaseAirtime(AccountActions requestAction, AirtimePurchaseRequest request) throws Exception {
        String email = SecurityUtils.currentUser();
        Map<String, Object> map = new HashMap<>();
        Optional<Users> entity = userRepository.findByEmail(email);
        if (!entity.isPresent()) {
            map.put("status", "01");
            map.put("message", "Unknown user account.");
            return map;
        }

        Users user = entity.get();
        Long userNo = user.getId();
        String userMobileNo = user.getPhone();
        String memberNo = user.getMemberNo();

        MemberProfileResults.DefaultAccount defaultAccount = fetchDefaultAccount(memberNo);
        String accountNo = defaultAccount.getAccountNo();
        BigDecimal availableBalance = defaultAccount.getAvailableBalance();
        BigDecimal transactionAmount = request.getAmount();

        //Check if balance is sufficient to proceed with this request
        if (availableBalance.compareTo(transactionAmount) == -1) {
            map.put("status", "01");
            map.put("message", "You have insufficient balance to complete this request.");
            return map;
        }

        //Parse URI and Fund Transfer Type
        String billType = BillsRequests.AIRTIME_PURCHASE;
        String paymentOption = FundsTransferTypes.AIRTIME_PURCHASE.toString();
        String transactionType = TransactionTypes.AIRTIME_PURCHASE;

//        CommissionVm commissionVm = new CommissionVm();
//        commissionVm.setAction( "airtime-purchase" );
        BigDecimal amount = request.getAmount();

        //Generate request object
        ApiAccountRequest apiAccountRequest = new ApiAccountRequest(
                memberNo,
                accountNo,
                amount,
                FundsTransferTypes.AIRTIME_PURCHASE.toString(),
                "",
                "",
                request.getReferenceNo(),
                null
        );

        boolean isCompleteRequest = requestAction.equals(AccountActions.complete);
        String requestUri = requestAction.equals(AccountActions.initiate) ?
                initTransferUri :
                requestAction.equals(AccountActions.complete) ? completeTransferUri : null;

        //Populate commission object
        CommissionVm commissionVm = commissionService.fetchCommision(transactionType, request.getAmount());
        apiAccountRequest.setCommission(commissionVm);

        //Generate json holder
        JsonNode jsonNode = objectMapper.convertValue(apiAccountRequest, JsonNode.class);

        //When to append PIN
        if (isCompleteRequest) {
            String transactionPin = request.getTransactionPin();
            ((ObjectNode) jsonNode).put("transactionPIN", transactionPin);
        }

        Map<String, Object> response = apiResponse(requestUri, jsonNode);
        response.put("commission", commissionVm);

        if (isCompleteRequest) {
            String status = (String) response.get("status");
            String apiMessage = (String) response.get("message");
            String flag = ("00".equals(status) ? Transactions.SUCCESS : Transactions.FAILED);

            //Log transaction
            Long transactionTypeNo = null;
            Optional<TransactionTypes> olTransactionType = transactionTypesRepository.findByCode(transactionType);
            if (olTransactionType.isPresent()) {
                transactionTypeNo = olTransactionType.get().getId();
            }

            CommissionVm.Fee fee = commissionVm.getFee();
            Transactions transaction = new Transactions();
            transaction
                    .setUserNo(userNo)
                    .setFlag(flag)
                    .setReferenceNo(request.getReferenceNo())
                    .setTransactionTypeNo(transactionTypeNo)
                    .setBinaryFee(fee.getThirdParty())
                    .setSaccoFee(fee.getInstitution())
                    .setAmount(request.getAmount());

            //Save transaction
            transactionsRepository.save(transaction);

            //When this transaction failed
            if ("01".equals(status)) {
                //Log failed transaction
                int httpCode = (int) response.get("httpCode");

                //Retrieve the component PK
                Long tnxComponentNo = null;
                Optional<TransactionComponents> oComponent = tnxComponentsRepository.findByCode(TransactionComponents.LANSTER_API);
                if (oComponent.isPresent()) {
                    tnxComponentNo = oComponent.get().getId();
                    FailedTransactionsLog log = new FailedTransactionsLog();
                    log
                            .setId(transaction.getId())
                            .setHttpCode(httpCode)
                            .setMessage(apiMessage)
                            .setTransactionComponentNo(tnxComponentNo); //Lanster

                    failedTnxLogRepository.save(log);
                }

                //Notify user
                String smsMessage = "An attempt to purchase airtime worth KES " + amount + " has failed. Contact " +
                        "customer care for further details";
                Notifications notification = new Notifications();
                notification
                        .setClientNo(userNo)
                        .setMessage(smsMessage);

                //Save a record
                notificationsRepository.save(notification);

                //Send SMS
                boolean send = smsService.sendSMS(new SmsOptions()
                        .setMessage( smsMessage )
                        .setMobileNo( userMobileNo )
                );

            }

            //Process bill payment
            else {
                //Process payment
                String jsonRequest = jsonNode.asText();
                BillsRequests billRequest = new BillsRequests();
                billRequest
                        .setReferenceNo(request.getReferenceNo())
                        .setBillType(billType)
                        .setJsonRequest(jsonRequest)
                        .setMemberNo(memberNo)
                        .setFlag(status)
                ;
                billsRequestsRepository.save(billRequest);

                String uri = env.getProperty("mfm.uris.airtime");
                Map<String, Object> mfmRequest = new HashMap<>();
                mfmRequest.put("amount", request.getAmount());
                mfmRequest.put("phone", request.getPhone());
                mfmRequest.put("provider", request.getProvider());
                ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
                        uri,
                        mfmRequest
                );

                //Parse Response
                jsonNode = objectMapper.readTree(responseEntity.getBody());
                String mfmMessage = jsonNode.get("message").asText();
                map.put("status", jsonNode.get("status").asText());
                map.put("message", mfmMessage);
                map.put("data", jsonNode.get("data"));

                //If this transaction fails, update the transaction
                boolean isMfmSuccess = !responseEntity.getStatusCode().is2xxSuccessful() ||
                        "01".equals( status );
                if ( isMfmSuccess ) {
                    transaction.setFlag(Transactions.FAILED);
                    transactionsRepository.save(transaction);

                    //Log failed transaction
                    int httpCode = responseEntity.getStatusCodeValue();

                    //Retrieve the component PK
                    Long tnxComponentNo = null;
                    Optional<TransactionComponents> oComponent = tnxComponentsRepository.findByCode(TransactionComponents.MFM);
                    if (oComponent.isPresent()) {
                        tnxComponentNo = oComponent.get().getId();

                        FailedTransactionsLog log = new FailedTransactionsLog();
                        log
                                .setId(transaction.getId())
                                .setHttpCode(httpCode)
                                .setMessage(mfmMessage)
                                .setTransactionComponentNo(tnxComponentNo); //mfm

                        failedTnxLogRepository.save(log);

                        //TODO: Attempt transaction reversal
                        reverseTransaction(apiAccountRequest, log);
                    }

                    //Notify user
                    String smsMessage = "An attempt to purchase airtime worth KES " + amount + " has failed. Contact " +
                            "customer care for further details";
                    Notifications notification = new Notifications();
                    notification
                            .setClientNo(userNo)
                            .setMessage(smsMessage);

                    //Save a record
                    notificationsRepository.save(notification);

                    //Send SMS
                    boolean send = smsService.sendSMS(new SmsOptions()
                            .setMessage(smsMessage)
                            .setMobileNo(user.getPhone())
                    );

                }

                return map;
            }
        }

        return response;
    }

    /**
     * Fetch Nairobi Water account details
     *
     * @param accountNo
     * @return ResponseModel
     */
    @Override
    public ResponseModel fetchNwAccountDetails(String accountNo) throws Exception {
        String uri = env.getProperty("mfm.uris.nairobi-water.fetch-details");

        Map<String, Object> map = new HashMap<>();
        map.put("account_no", accountNo);

        ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
                uri,
                map
        );

        JsonNode jsonNode = objectMapper.readTree(responseEntity.getBody());
        ResponseModel response = new ResponseModel(
                jsonNode.get("status").asText(),
                jsonNode.get("message").asText(),
                jsonNode.get("data")
        );
        return response;
    }


    /**
     * NWC Water bill payments
     *
     * @param requestAction
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    @Override
    public Map<String, Object> payNairobiWaterBill(AccountActions requestAction, NairobiWaterPaymentRequest request) throws Exception {
        String email = SecurityUtils.currentUser();
        Map<String, Object> map = new HashMap<>();
        Optional<Users> entity = userRepository.findByEmail(email);
        if (!entity.isPresent()) {
            map.put("status", "01");
            map.put("message", "Unknown user account.");
            return map;
        }

        Users user = entity.get();
        Long userNo = user.getId();
        String memberNo = user.getMemberNo();

        MemberProfileResults.DefaultAccount defaultAccount = fetchDefaultAccount(memberNo);
        String accountNo = defaultAccount.getAccountNo();
        BigDecimal availableBalance = defaultAccount.getAvailableBalance();
        BigDecimal transactionAmount = request.getAmount();

        //Check if balance is sufficient to proceed with this request
        if (availableBalance.compareTo(transactionAmount) == -1) {
            map.put("status", "01");
            map.put("message", "You have insufficient balance to complete this request.");
            return map;
        }

        //Parse URI and Fund Transfer Type
        String billType = BillsRequests.NAIROBI_WATER;
        String paymentOption = FundsTransferTypes.NAIROBI_WATER.toString();
        String transactionType = TransactionTypes.NAIROBI_WATER;

//        CommissionVm commissionVm = new CommissionVm();
//        commissionVm.setAction( "airtime-purchase" );
        BigDecimal amount = request.getAmount();

        //Generate request object
        ApiAccountRequest apiAccountRequest = new ApiAccountRequest(
                memberNo,
                accountNo,
                amount,
                FundsTransferTypes.NAIROBI_WATER.toString(),
                "",
                "",
                request.getReferenceNo(),
                null
        );

        boolean isCompleteRequest = requestAction.equals(AccountActions.complete);
        String requestUri = requestAction.equals(AccountActions.initiate) ?
                initTransferUri :
                requestAction.equals(AccountActions.complete) ? completeTransferUri : null;

        //Populate commission object
        CommissionVm commissionVm = commissionService.fetchCommision(transactionType, request.getAmount());
        apiAccountRequest.setCommission(commissionVm);

        //Generate json holder
        JsonNode jsonNode = objectMapper.convertValue(apiAccountRequest, JsonNode.class);

        //When to append PIN
        if (isCompleteRequest) {
            String transactionPin = request.getTransactionPin();
            ((ObjectNode) jsonNode).put("transactionPIN", transactionPin);
        }

        Map<String, Object> response = apiResponse(requestUri, jsonNode);

        if (isCompleteRequest) {
            String status = (String) response.get("status");
            String apiMessage = (String) response.get("message");
            String flag = ("00".equals(status) ? Transactions.SUCCESS : Transactions.FAILED);

            //Log transaction
            Long transactionTypeNo = null;
            Optional<TransactionTypes> olTransactionType = transactionTypesRepository.findByCode(transactionType);
            if (olTransactionType.isPresent()) {
                transactionTypeNo = olTransactionType.get().getId();
            }

            CommissionVm.Fee fee = commissionVm.getFee();
            Transactions transaction = new Transactions();
            transaction
                    .setUserNo(userNo)
                    .setFlag(flag)
                    .setReferenceNo(request.getReferenceNo())
                    .setTransactionTypeNo(transactionTypeNo)
                    .setBinaryFee(fee.getThirdParty())
                    .setSaccoFee(fee.getInstitution())
                    .setAmount(request.getAmount());

            //Save transaction
            transactionsRepository.save(transaction);

            //When this transaction failed
            if ("01".equals(status)) {
                //Log failed transaction
                int httpCode = (int) response.get("httpCode");

                //Retrieve the component PK
                Long tnxComponentNo = null;
                Optional<TransactionComponents> oComponent = tnxComponentsRepository.findByCode(TransactionComponents.LANSTER_API);
                if (oComponent.isPresent()) {
                    tnxComponentNo = oComponent.get().getId();
                    FailedTransactionsLog log = new FailedTransactionsLog();
                    log
                            .setId(transaction.getId())
                            .setHttpCode(httpCode)
                            .setMessage(apiMessage)
                            .setTransactionComponentNo(tnxComponentNo); //Lanster

                    failedTnxLogRepository.save(log);
                }

                //Notify user
                String smsMessage = "An attempt to pay Nairobi Water bill of KES " + amount + " has failed. Contact " +
                        "customer care for further details";
                Notifications notification = new Notifications();
                notification
                        .setClientNo(userNo)
                        .setMessage(smsMessage);

                //Save a record
                notificationsRepository.save(notification);

                //Send SMS
                boolean send = smsService.sendSMS(new SmsOptions()
                        .setMessage(smsMessage)
                        .setMobileNo(user.getPhone())
                );

            }

            //Process bill payment
            else {
                //Process payment
                String jsonRequest = jsonNode.asText();
                BillsRequests billRequest = new BillsRequests();
                billRequest
                        .setReferenceNo(request.getReferenceNo())
                        .setBillType(billType)
                        .setJsonRequest(jsonRequest)
                        .setMemberNo(memberNo)
                        .setFlag(status)
                ;
                billsRequestsRepository.save(billRequest);

                String uri = env.getProperty("mfm.uris.nairobi-water.payment");
                Map<String, Object> mfmRequest = new HashMap<>();
                mfmRequest.put("amount", request.getAmount());
                mfmRequest.put("customer", request.getCustomerName());
                mfmRequest.put("phone", request.getCustomerPhoneNo());
                mfmRequest.put("account_no", request.getAccountNo());
                ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
                        uri,
                        mfmRequest
                );

                //Parse Response
                jsonNode = objectMapper.readTree(responseEntity.getBody());
                String mfmMessage = jsonNode.get("message").asText();
                map.put("status", jsonNode.get("status").asText());
                map.put("message", mfmMessage);
                map.put("data", jsonNode.get("data"));

                //If this transaction fails, update the transaction
                if (!responseEntity.getStatusCode().is2xxSuccessful()) {
                    transaction.setFlag(Transactions.FAILED);
                    transactionsRepository.save(transaction);

                    //Log failed transaction
                    int httpCode = responseEntity.getStatusCodeValue();

                    //Retrieve the component PK
                    Long tnxComponentNo = null;
                    Optional<TransactionComponents> oComponent = tnxComponentsRepository.findByCode(TransactionComponents.MFM);
                    if (oComponent.isPresent()) {
                        tnxComponentNo = oComponent.get().getId();

                        FailedTransactionsLog log = new FailedTransactionsLog();
                        log
                                .setId(transaction.getId())
                                .setHttpCode(httpCode)
                                .setMessage(mfmMessage)
                                .setTransactionComponentNo(tnxComponentNo); //mfm

                        failedTnxLogRepository.save(log);

                        //TODO: Attempt transaction reversal
                        reverseTransaction(apiAccountRequest, log);
                    }

                    //Notify user
                    String smsMessage = "An attempt to pay Nairobi Water Bill of KES " + amount + " has failed. Contact " +
                            "customer care for further details";
                    Notifications notification = new Notifications();
                    notification
                            .setClientNo(userNo)
                            .setMessage(smsMessage);

                    //Save a record
                    notificationsRepository.save(notification);

                    //Send SMS
                    boolean send = smsService.sendSMS(new SmsOptions()
                            .setMessage(smsMessage)
                            .setMobileNo(user.getPhone())
                    );

                }

                return map;
            }
        }

        return response;
    }


    /**
     * Retrieve member default account
     *
     * @param memberNo
     * @return String
     * @throws Exception
     */
    private MemberProfileResults.DefaultAccount fetchDefaultAccount(String memberNo) throws Exception {
        MemberProfileResults.DefaultAccount defaultAccount = new MemberProfileResults.DefaultAccount();
        MemberProfileRequest memberProfileRequest = new MemberProfileRequest();
        memberProfileRequest.setMemberNo(memberNo);

        String uri = env.getProperty("lanster.api.get-member-profile");
        JsonNode node = objectMapper.convertValue(memberProfileRequest, JsonNode.class);
        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(uri, node);

        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            MemberProfileResults memberProfile = objectMapper.readValue(responseEntity.getBody(), MemberProfileResults.class);
            defaultAccount = memberProfile.getDefaultAccount();
        }

        return defaultAccount;
    }

    /**
     * GetBranchRequest transactions handler
     *
     * @param request
     * @param transactionType
     * @param action
     * @param transactionPin
     * @return Map<String, Object>
     * @throws Exception
     */
    private Map<String, Object> handleRequest(ApiAccountRequest request,
                                              String transactionType,
                                              AccountActions action, String transactionPin) throws Exception {
        String requestAction = action.toString();
        String requestUri = requestAction.equals(AccountActions.initiate.toString()) ?
                initTransferUri :
                requestAction.equals(AccountActions.complete.toString()) ? completeTransferUri : null;

        //Populate commission object
        CommissionVm commissionVm = commissionService.fetchCommision(transactionType, request.getAmount());
        request.setCommission(commissionVm);

        //Generate json holder
        JsonNode jsonNode = objectMapper.convertValue(request, JsonNode.class);

        //When to append PIN
        if (requestAction.equals(AccountActions.complete.toString())) {
            ((ObjectNode) jsonNode).put("transactionPIN", transactionPin);
        }

        //Parse response
        return apiResponse(requestUri, jsonNode);
    }

    /**
     * Reverse transaction
     *
     * @param apiAccountRequest
     * @param log
     * @throws Exception
     */
    private void reverseTransaction(ApiAccountRequest apiAccountRequest, FailedTransactionsLog log) throws Exception {
        JsonNode jsonNode = objectMapper.convertValue(apiAccountRequest, JsonNode.class);
        Map<String, Object> map = reverseTransaction(jsonNode);

        //Update transaction log
        String status = (String) map.get("status");
        String message = (String) map.get("message");

        String reversalStatus = "00".equals(status) ? "Success" : "Failed";
        log
                .setReversalStatus(reversalStatus)
                .setReversalAttempts(1L);

        failedTnxLogRepository.save(log);
    }


}
