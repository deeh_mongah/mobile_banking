package com.binary.web.billers;

import com.binary.core.rpm.ResponseModel;
import com.binary.web.accounts.vm.AccountActions;
import com.binary.web.billers.vm.request.AirtimePurchaseRequest;
import com.binary.web.billers.vm.request.KplcPrepaidRequest;
import com.binary.web.billers.vm.request.NairobiWaterPaymentRequest;

import java.util.Map;

public interface UtilityBillsServiceInterface {

    /**
     * Handle electricity payments
     *
     * @param requestAction
     * @param request
     * @return PrepaidClientResponse
     */
    public Map<String, Object> kplcPrepaidHandler(AccountActions requestAction, KplcPrepaidRequest request) throws Exception;

    /**
     * Fetch KPLC Prepaid meter details
     *
     * @param request
     * @return ResponseModel
     */
    public ResponseModel fetchKplcPrepaidMeterDetails(KplcPrepaidRequest request ) throws Exception;

    /**
     * Purchase airtime handler
     *
     * @param requestAction
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    public Map<String, Object> purchaseAirtime(AccountActions requestAction, AirtimePurchaseRequest request) throws Exception;

    /**
     * Fetch Nairobi Water account details
     *
     * @param accountNo
     * @return ResponseModel
     */
    public ResponseModel fetchNwAccountDetails(String accountNo) throws Exception;

    /**
     * NWC Water bill payments
     *
     * @param requestAction
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    public Map<String, Object> payNairobiWaterBill(AccountActions requestAction, NairobiWaterPaymentRequest request) throws Exception;


}
