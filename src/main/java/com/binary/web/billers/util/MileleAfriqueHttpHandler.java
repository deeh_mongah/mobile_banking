package com.binary.web.billers.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class MileleAfriqueHttpHandler {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired private ObjectMapper mapper;
    @Autowired private RestTemplate restTemplate;

    @Value("${milele.africa.auth-key}")
    private String MILELE_AFRICA_TOKEN;

    private HttpHeaders createHeaders() {
        return new HttpHeaders() {
            {
                //Authorization
                set("Authorization", "Bearer " + MILELE_AFRICA_TOKEN);
                set("Content-Type", MediaType.APPLICATION_JSON.toString());
                set("Accept", "application/json");
            }
        };
    }

    /**
     * @param url
     * @param request
     * @return ResponseEntity<String>
     * @throws Exception
     */
    public ResponseEntity<String> postRequest(String url, Object request) throws Exception {
        //Fetch Host
        /*Pack the URL for this request*/
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .fromHttpUrl( url )
                .build()
                .encode();

        HttpHeaders httpHeaders = createHeaders();
        String jsonRequest = mapper.writeValueAsString( request );

        HttpEntity<?> httpEntity = new org.springframework.http.HttpEntity<>( jsonRequest, httpHeaders);
        ResponseEntity<String> responseEntity = restTemplate.exchange(uriComponents.toString(), HttpMethod.POST, httpEntity, String.class);
        return responseEntity;
    }

    /**
     * @param url
     * @return ResponseEntity<String>
     * @throws Exception
     */
    public ResponseEntity<String> getRequest(String url) throws Exception {
        //Fetch Host
        /*Pack the URL for this request*/
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .fromHttpUrl( url )
                .build()
                .encode();

        HttpHeaders httpHeaders = createHeaders();
        HttpEntity<?> httpEntity = new org.springframework.http.HttpEntity<>( null, httpHeaders);
        ResponseEntity<String> responseEntity = restTemplate.exchange(uriComponents.toString(), HttpMethod.GET, httpEntity, String.class);
        return responseEntity;
    }


}
