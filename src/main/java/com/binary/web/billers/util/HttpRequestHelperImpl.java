package com.binary.web.billers.util;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;

@Service("httpHelper")
public class HttpRequestHelperImpl implements HttpRequestHelper {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired private ObjectMapper mapper;

    @Value("${milele.africa.auth-key}")
    private String MILELE_AFRICA_TOKEN;

    public String sendTokenAuthenticatedGETReq(String url) {
        String message_response = null;
        try {
//            LOG.info("URL: " + url);
//            Client client = Client.create();
//            client.setConnectTimeout(45 * 1000);
//
//            WebResource webResource = client.resource(url);
//            ClientResponse remoteResponse = webResource
//                    .type("application/json")
//                    .header("Authorization", "Bearer " + MILELE_AFRICA_TOKEN)
//                    .get(ClientResponse.class);
//
//            message_response = remoteResponse.getEntity(String.class);
//
//
//
//
//            UriComponents uriComponents = UriComponentsBuilder.newInstance()
//                    .fromHttpUrl( url )
//                    .build()
//                    .encode();
//            RestTemplate restTemplate = new RestTemplate();
//            HttpHeaders httpHeaders = new HttpHeaders();
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.set("Accept", "application/json");
//            httpHeaders.set("Authorization", "Bearer " + MILELE_AFRICA_TOKEN);
//            HttpEntity<String> httpEntity = new HttpEntity<String>( httpHeaders );
//
//            LOG.info("SERVICE REQUEST : " + uriComponents.toString() + " " + httpEntity.getHeaders() + " " + httpEntity.getBody());
//
//
//            ResponseEntity<String> responseEntity = restTemplate.exchange(
//                    uriComponents.toString(),
//                    HttpMethod.POST,
//                    httpEntity,
//                    String.class
//                    );

//            LOG.info("RESPONSE:  " + message_response);



        } catch (Exception e) {
            LOG.error("Transaction failed processing. " + e.getMessage());

            JsonNode node = mapper.createObjectNode();
            ((ObjectNode) node)
                    .put("status", "01")
                    .put("message", "An exception occurred")
                    .put("error", e.getMessage() );

            message_response = node.asText();
        }
        return message_response;
    }

    /**
     * Forward requests to Safaricom API endpoints
     *
     * @param url     Specific end point
     * @param payload Transaction message
     * @return response from remote server
     */
    @Override
    public String sendTokenAuthenticatedPOSTReq(String url, String payload) {
        String message_response = null;
        System.err.println("Request: " + payload);
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            post.setHeader("Accept", "application/json");
            post.setHeader("Authorization", "Bearer " + MILELE_AFRICA_TOKEN);
            post.setEntity(new StringEntity(payload));

            LOG.info("Request URL: " + url);
            try (CloseableHttpResponse httpResponse = httpclient.execute(post)) {
                HttpEntity entity = httpResponse.getEntity();
                message_response = EntityUtils.toString(entity);
                LOG.info("RESPONSE:  " + message_response);
                EntityUtils.consume(entity);
            }
        } catch (IOException er) {
            LOG.info(er.getMessage());
            JsonNode node = mapper.createObjectNode();
            ((ObjectNode) node)
                    .put("error", "01")
                    .put("message", er.getMessage() );

            message_response = node.asText();
        }

        return message_response;
    }


    /**
     * @param url
     * @param request
     * @return ResponseEntity<String>
     * @throws Exception
     */
    @Override
    public ResponseEntity<String> postRequest(String url, Object request) throws Exception {
        //Fetch Host
        /*Pack the URL for this request*/
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .fromHttpUrl( url )
                .build()
                .encode();

        ObjectMapper mapper = new ObjectMapper();
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.set("Accept", "application/json");
        httpHeaders.set("Authorization", "Bearer " + MILELE_AFRICA_TOKEN);

        String jsonRequest = mapper.writeValueAsString( request );
        org.springframework.http.HttpEntity<?> httpEntity = new org.springframework.http.HttpEntity<>( jsonRequest, httpHeaders);

        System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request));
        LOG.info("SERVICE REQUEST : " + uriComponents.toString() + " " + httpEntity.getHeaders() + " \n" + httpEntity.getBody());
        ResponseEntity<String> responseEntity = restTemplate.exchange(uriComponents.toString(), HttpMethod.POST, httpEntity, String.class);
        LOG.info("SERVICE RESPONSE : " + responseEntity.getHeaders() + " " + responseEntity.getBody());
        System.err.println("SERVICE RESPONSE : " + responseEntity.getHeaders() + " " + responseEntity.getBody());
        return responseEntity;
    }
}
