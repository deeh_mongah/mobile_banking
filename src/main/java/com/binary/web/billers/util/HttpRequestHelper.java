package com.binary.web.billers.util;

import org.springframework.http.ResponseEntity;

public interface HttpRequestHelper {

    String sendTokenAuthenticatedGETReq(String url);

    String sendTokenAuthenticatedPOSTReq(String url, String payload);

    ResponseEntity<String> postRequest(String url, Object request) throws Exception;
}
