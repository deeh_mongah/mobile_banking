//package com.binary;
//
//import com.binary.core.http.LansterHttpService;
//import com.binary.web.fundstransfer.service.FundsTransferHelper;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.core.env.Environment;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.junit4.SpringRunner;
//import springfox.documentation.spring.web.json.Json;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
//public class LansterApiTests{
//
//    @Autowired private ObjectMapper objectMapper;
//    @Autowired private FundsTransferHelper fundsTransferHelper;
////
//    @Autowired private LansterHttpService lansterHttpService;
//    @Autowired private Environment env;
//
//    @Test
//    public void reversalAttempt_ReturnsStatus0() throws Exception{
//        Map<String, Object> map = new HashMap<>();
//        map.put("referenceNo", "00003");
//        map.put("externalPaymentGUID", "");
//
//        JsonNode jsonNode = objectMapper.convertValue( map, JsonNode.class );
//        map = fundsTransferHelper.reverseTransaction( jsonNode );
//        String status = (String)map.get("status");
//
//        Assert.assertTrue( "00".equalsIgnoreCase( status ) );
//    }
//
//    @Test
//    public void getExtraAttributes_ReturnStatus0() throws Exception{
//        JsonNode jsonNode = objectMapper.createObjectNode();
//
//        String requestUri = env.getProperty("lanster.api.users.fetch-attributes");
//        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
//                requestUri,
//                jsonNode
//        );
//
//        jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
//        JsonNode branchesArray = jsonNode.get("branches");
//
//        System.err.println("branches : "+ branchesArray.asText() );
//
//        if( jsonNode.get("branches").isArray() ) {
//
//        }
//
//        branchesArray.forEach( e ->{
//            String name = e.get("name").asText();
//            String code = e.get("code").asText();
//
//            String qr = String.format("INSERT INTO branches (name, code, flag, created_on, updated_on, created_by, updated_by, institution_no) VALUES('%s', '%s', '1', now(), now(), 1, 1, 1);", name, code);
//            System.err.println( qr );
//        });
//    }
//
//    @Test
//    public void test_fetch_loan_products_returns_200() throws Exception{
//        JsonNode jsonNode = objectMapper.createObjectNode();
//        String requestUri = env.getProperty("lanster.api.loans.products");
//        ResponseEntity<String> responseEntity = lansterHttpService.postRequest(
//                requestUri,
//                jsonNode
//        );
//
//        Assert.assertTrue( responseEntity.getStatusCode().is2xxSuccessful() );
//    }
//}
