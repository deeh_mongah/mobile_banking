//package com.binary;
//
//import com.binary.core.http.MfmHttpService;
//import com.binary.core.sms.SmsOptions;
//import com.binary.core.sms.SmsServiceInterface;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.core.env.Environment;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
//public class MfmTests {
//
//    @Autowired private Environment env;
//    @Autowired private MfmHttpService mfmHttpService;
//
//    @Autowired private SmsServiceInterface smsService;
//
//    @Test
//    public void attemptToSendSms_Returns200() throws Exception{
//        StringBuilder message = new StringBuilder();
//        message.append("Your Kluster authorization code is ")
//                .append( "00000");
//        boolean sent = smsService.sendSMS( new SmsOptions()
//                .setMessage( message.toString() )
//                .setMobileNo( "254720727490" )
//        );
//
//        Assert.assertTrue( sent );
//    }
//
//    @Test
//    public void test_plc_purchase_returns_200() throws Exception{
//        String uri = env.getProperty("mfm.uris.kplc-prepaid.purchase");
//        Map<String, Object> mfmRequest = new HashMap<>();
//        mfmRequest.put("amount", 100);
//        mfmRequest.put("customer", "Anthony Mwawughanga");
//        mfmRequest.put("phone", "254720727490");
//        mfmRequest.put("meter_no", "01450291248-234324324");
//        ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
//                uri,
//                mfmRequest
//        );
//
//        Assert.assertTrue("KPLC Purchase failed", responseEntity.getStatusCode().is2xxSuccessful() );
//    }
//
//    @Test
//    public void test_airtime_purchase_returns_200() throws  Exception{
//        String uri = env.getProperty("mfm.uris.airtime");
//        Map<String, Object> mfmRequest = new HashMap<>();
//        mfmRequest.put("amount", 20);
//        mfmRequest.put("phone", "254720727490");
//        mfmRequest.put("provider", "safaricom");
//        ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
//                uri,
//                mfmRequest
//        );
//
//        Assert.assertTrue("Airtime Purchase failed", responseEntity.getStatusCode().is2xxSuccessful() );
//    }
//}
