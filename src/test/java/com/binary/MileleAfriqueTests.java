//package com.binary;
//
//import com.binary.web.billers.util.MileleAfriqueHttpHandler;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.core.env.Environment;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.web.util.UriComponents;
//import org.springframework.web.util.UriComponentsBuilder;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
//public class MileleAfriqueTests {
//
//    @Autowired
//    private MileleAfriqueHttpHandler mileleHandler;
//
//    @Value("${milele.africa.power-prepaid}")
//    private String tokensUrl;
//
//    @Value("${milele.africa.topup}")
//    private String airtimeUrl;
//
//
//    @Autowired private Environment env;
//    @Autowired private ObjectMapper objectMapper;
//
//    @Test
//    public void attemptKplcTokenPurchase_Returns200() throws Exception{
//        Map<String, Object> map = new HashMap<>();
//        map.put("amount", 100);
//        map.put("meter", "01450291248");
//
//        ResponseEntity<String> responseEntity = mileleHandler.postRequest(
//                tokensUrl,
//                map
//        );
//
//        Assert.assertTrue( responseEntity.getStatusCode().is2xxSuccessful() );
//    }
//
//    @Test
//    public void attemptAirtimePurchase_Returns200() throws Exception{
//        Map<String, Object> map = new HashMap<>();
//        map.put("amount", 10);
//        map.put("telco", "safaricom");
//        map.put("phone", "0720727490");
//
//        ResponseEntity<String> responseEntity = mileleHandler.postRequest(
//                airtimeUrl,
//                map
//        );
//
//        Assert.assertTrue( responseEntity.getStatusCode().is2xxSuccessful() );
//    }
//
//    @Test
//    public void attemptNairobiWater_Returns200() throws Exception{
//
//        //1. Bill Request
//        String accountNumber = "1260904";
//        String uri = env.getProperty("milele.africa.nwc-fetch-bill");
//        UriComponents uriComponents = UriComponentsBuilder.newInstance()
//                .fromHttpUrl( uri )
//                .queryParam("account_number", accountNumber)
//                .build();
//        ResponseEntity<String> responseEntity = mileleHandler.getRequest(
//                uriComponents.toString()
//        );
//
//        Assert.assertTrue( responseEntity.getStatusCode().is2xxSuccessful() );
//
//        //2. Initiate transaction
//        uri = env.getProperty("milele.africa.nwc-prepare-tnx-details");
//        Map<String, Object> map = new HashMap<>();
//        map.put("amount", 50);
//        map.put("account_number", accountNumber);
//        map.put("phone_number", "0720727490");
//        map.put("names", "Anthony Mwawughanga");
//
//        responseEntity = mileleHandler.postRequest(
//                uri,
//                map
//        );
//
//        JsonNode jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
//        String transactionId = jsonNode.get("data").get("TransactionID").asText();
//
//        //3. Complete transaction
//        map.clear();
//        uri = env.getProperty("milele.africa.nwc-pay");
//        map.put("transaction_id", transactionId);
//        responseEntity = mileleHandler.postRequest(
//                uri,
//                map
//        );
//
//        Assert.assertTrue( responseEntity.getStatusCode().is2xxSuccessful() );
//    }
//
//    @Test
//    public void attemptNHIF_Returns200() throws Exception{
//
//        String accountNumber = "5517336";
//
//        //1. Initiate transaction
//        String uri = env.getProperty("milele.africa.nhif-individual-contribution-details");
//        Map<String, Object> map = new HashMap<>();
//        map.put("amount", 50);
//        map.put("account_number", accountNumber);
//        map.put("phone_number", "0720727490");
//
//        ResponseEntity<String> responseEntity = mileleHandler.postRequest(
//                uri,
//                map
//        );
//
//        JsonNode jsonNode = objectMapper.readValue( responseEntity.getBody(), JsonNode.class );
//        String transactionId = jsonNode.get("data").get("TransactionID").asText();
//
//        //2. Complete transaction
//        uri = env.getProperty("milele.africa.nhif-pay");
//        map.clear();
//        map.put("amount", 50);
//        map.put("transaction_id", transactionId);
//        map.put("phone_number", "0720727490");
//
//        Assert.assertTrue( responseEntity.getStatusCode().is2xxSuccessful() );
//    }
//}
